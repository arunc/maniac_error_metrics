# CirKit Experimental Addon

## Installation

After installing CirKit, clone this repository inside the `addons` directory of CirKit.

Then perform the following actions in the `build` directory of CirKit.

    ccmake ..

There enable the addon by toggling the flag at `enable_cirkit-addon-experimental`. Press `c` and then `g`. Then type

    make

## Additional dependencies

This addon requires `libffi` and `bison`.

