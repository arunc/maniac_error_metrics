/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE lexicographic

#include <iostream>

#include <boost/test/included/unit_test.hpp>

#include <core/utils/bitset_utils.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/abcsat.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/operations/logic.hpp>
#include <formal/sat/utils/lexicographic.hpp>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  auto solver = make_solver<minisat_solver>();

  auto a = 1;
  auto b = 2;
  auto c = 3;

  /* MAJ operation */
  logic_and( solver, a, b, 4 );
  logic_and( solver, a, c, 5 );
  logic_and( solver, b, c, 6 );
  logic_or( solver, {4,5,6}, 7 );
  add_clause( solver )( {7} );

  std::cout << to_string( lexicographic_smallest_solution( solver, {a,b,c} ) ) << std::endl;
  std::cout << to_string( lexicographic_largest_solution( solver, {a,b,c} ) ) << std::endl;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
