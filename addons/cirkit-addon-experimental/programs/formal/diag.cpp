/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Heinz Riener
 */

#include <classical/io/read_aiger.hpp>
#include <classical/utils/aig_utils.hpp>
#include <core/utils/program_options.hpp>
#include <formal/verification/aig_modelcheck.hpp>
#include <formal/verification/aig_diagnose.hpp>
#include <boost/format.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string circuit_filename, spec_filename;
  unsigned cardinality = 1u;
  program_options opts;
  auto max_cex = 100u;
  opts.add_options()
    ( "circuit",           value( &circuit_filename ),              "Circuit filename" )
    ( "spec",              value( &spec_filename ),                 "Specification filename" )
    ( "max_cex",           value_with_default( &max_cex ),          "Maximum number of counterexamples" )
    ( "cardinality",       value_with_default( &cardinality ),      "Cardinality bound for diagnosis" )
    ( "gia",                                                        "aig --> gia --> cnf" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "circuit" ) || !opts.is_set( "spec" ) )
  {
    std::cout << opts << std::endl;
    return 10;
  }

  /* read aigs */
  aig_graph circuit, specification;
  try
  {
    std::cerr << boost::format( "[i] read net-list of circuit ``%s''" ) % circuit_filename << std::endl;
    read_aiger( circuit, circuit_filename );
    aig_info( circuit ).constant_used = true;

    std::cerr << boost::format( "[i] read net-list of specification ``%s''" ) % spec_filename << std::endl;
    read_aiger( specification, spec_filename );
    aig_info( specification ).constant_used = true;
  }
  catch ( const char *msg )
  {
    std::cerr << "[e] " << msg << std::endl;
    return 10;
  }

  std::set< counterexample_t_ > counterexamples;
  {
    /* *** modelchecking *** */
    std::cout << "[ modelchecking ]" << std::endl;

    auto settings = std::make_shared<properties>();
    auto statistics = std::make_shared<properties>();
    set( settings, "max_counterexamples", max_cex );
    counterexamples = modelchecking( circuit, specification, settings, statistics );
    if ( counterexamples.size() == 0u )
    {
      std::cout << "[i] functionally equivalent (no counterexamples)" << std::endl;
      return 0;
    }
    else
    {
      std::cout << "[i] all counterexamples enumerated" << std::endl;
    }
    std::cout << boost::format( "[i] run-time: %10.2f" )
      % statistics->get<double>( "runtime_user", 0.0 ) << std::endl;

    // for ( const auto& c : counterexamples )
    // {
    //   std::cout << c.assignment << std::endl;
    // }
    std::cout << "[i] #counterexamples: " << counterexamples.size() << std::endl;
  }

  {
    /* *** diagnosis *** */
    std::cout << "[ diagnosis ]" << std::endl;
    auto settings = std::make_shared<properties>();
    auto statistics = std::make_shared<properties>();
    set( settings, "cardinality", cardinality );
    const auto diagnoses = diagnosis( circuit, counterexamples, settings, statistics );

    std::cout << boost::format( "[i] run-time: %10.2f" )
      % statistics->get<double>( "runtime_user", 0.0 ) << std::endl;

    // for ( const auto& d : diagnoses )
    // {
    //   std::cout << "fault candidate: ";
    //   for ( const auto& i : d )
    //   {
    //     std::cout << ( 2u*i ) << ' ';
    //   }
    //   std::cout << std::endl;
    // }
    std::cout << "[i] #diagnosis: " << diagnoses.size() << std::endl;
  }

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
