/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Heinz Riener
 */

#include <formal/verification/modelchecking.hpp>
#include <formal/verification/dnf_learning.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>
#include <classical/functions/aig_miter.hpp>
#include <classical/functions/aig_cone_utils.hpp>
#include <classical/io/read_aiger.hpp>
#include <classical/utils/aig_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/program_options.hpp>
#include <core/utils/timer.hpp>
#include <core/properties.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/assign/std/set.hpp>

using namespace cirkit;

using aig_single_output_function_t = std::pair< std::vector< aig_function >, aig_function >;

std::ostream& operator<<( std::ostream& os, const dnf_t& dnf )
{
  for ( const auto& t : dnf )
  {
    if ( t.size() == 0 ) { os << "true "; continue; }
    os << "( ";
    for ( const auto& l : t )
    {
      const auto var = l > 0 ? ( l-1 ) : -( l-1 );
      os << ( l < 0 ? "~" : "" ) << "x" << var << ' ';
    }
    os << ")";
  }
  return os;
}

std::ostream& operator<<( std::ostream& os, const aig_single_output_function_t& out )
{
  os << out.second << " <-- ";
  for ( const auto& c : out.first )
  {
    os << c << ' ';
  }
  os << '\n';
  return os;
}

boost::optional< boost::dynamic_bitset<> > modelcheck( const aig_graph& miter, const modelchecker& checker )
{
  const auto result = checker.run( miter );
  if ( result.first == modelcheck_result_t::success )
  {
    return boost::optional< boost::dynamic_bitset<> >();
  }
  else
  {
    const auto size = result.second.size();
    boost::dynamic_bitset<> input_assignment( size );
    for ( auto i = 0u; i < size; ++i )
    {
      input_assignment[ i ] = result.second[ i ];
    }
    return boost::optional< boost::dynamic_bitset<> >( input_assignment );
  }
}

void simulate_assignment( const aig_graph& aig, std::map< aig_node, bool >& assignment )
{
  assignment[ 0u ] = 0u;

  std::vector< unsigned > topsort( boost::num_vertices( aig ) );
  boost::topological_sort( aig, topsort.begin() );
  for ( const auto& node : topsort )
  {
    /* constant */
    if ( node == 0u ) { continue; };

    const auto degree = out_degree( node, aig );

    /* latches are not supported */
    assert( degree == 0u || degree == 2u );

    /* input */
    if ( degree == 0u )
    {
      assert( assignment.find( node ) != assignment.end() );
    }

    /* and */
    if ( degree == 2u )
    {
      assert( assignment.find( node ) == assignment.end() );
      const auto children = get_children( aig, node );
      assert( assignment.find( children[ 0u ].node ) != assignment.end() );
      assert( assignment.find( children[ 1u ].node ) != assignment.end() );
      const auto lhs = assignment[ children[ 0u ].node ];
      const auto rhs = assignment[ children[ 1u ].node ];
      assignment[ node ] = ( children[ 0u ].complemented ? !lhs : lhs ) & ( children[ 1u ].complemented ? !rhs : rhs );
    }
  }
}

std::pair< std::string, char > resimulate( const aig_graph& aig, const aig_graph& current_aig,
                                           const aig_single_output_function_t& f,
                                           const boost::dynamic_bitset<>& counterexample )
{
  std::string cone_bits( f.first.size(), '0' );
  char out_bit;

  {
    const auto& info = aig_info( aig );

    std::map< aig_node, bool > assignment;
    for ( const auto& i : index( info.inputs ) )
    {
      assert( i.index < counterexample.size() );
      assignment.insert( { i.value, counterexample[ i.index ] } );
    }

    /* simulate */
    simulate_assignment( aig, assignment );
    assert( assignment.size() == boost::num_vertices( aig ) );

    /* extract the result */
    for ( const auto& c : index( f.first ) )
    {
      cone_bits[ c.index ] = ( assignment[ c.value.node ] != c.value.complemented ) ? '1' : '0';
    }
  }

  {
    const auto& info = aig_info( current_aig );

    std::map< aig_node, bool > assignment;
    for ( const auto& i : index( info.inputs ) )
    {
      assert( i.index < counterexample.size() );
      assignment.insert( { i.value, counterexample[ i.index ] } );
    }

    /* simulate */
    simulate_assignment( current_aig, assignment );
    assert( assignment.size() == boost::num_vertices( current_aig ) );

    /* extract the result */
    out_bit = ( assignment[ f.second.node ] != f.second.complemented ) ? '1' : '0';
  }

  return { cone_bits, out_bit };
}

std::set< std::string > get_annotations( const aig_graph& aig, const aig_node& node, const std::string& name )
{
  std::set< std::string > string_annotations;
  const auto annotation = get( boost::vertex_annotation, aig );
  const auto& it = annotation[ node ].find( name );
  if ( it != annotation[ node ].end() )
  {
    std::istringstream iss( it->second );
    std::copy( std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(),
               std::inserter(string_annotations, string_annotations.begin()) );
  }
  return string_annotations;
}

std::vector< aig_function > extract_cone( const aig_graph& miter, const aig_node& current )
{
  /* compute the complement of the output cone but without zero and current */
  auto nodes = complemented_output_cone_as_vector( miter, current );
  {
    auto it = std::find( nodes.begin(), nodes.end(), 0u );
    if ( it != nodes.end() ) nodes.erase( it );
  }
  {
    auto it = std::find( nodes.begin(), nodes.end(), current );
    if ( it != nodes.end() ) nodes.erase( it );
  }

  std::vector< aig_function > cone;
  for ( const auto& node : nodes )
  {
    cone += aig_function( { node, false } );
  }
  return cone;
}

std::pair< aig_graph, aig_single_output_function_t >
apply_fix( const aig_graph& source_aig, const aig_single_output_function_t& single_output_function, const dnf_t& dnf )
{
  auto current_output_function = single_output_function;

  aig_graph target_aig;
  aig_initialize( target_aig );
  auto& target_info = aig_info( target_aig );

  /* no simplification */
  target_info.enable_strashing = target_info.enable_local_optimization = true;

  auto& source_info = aig_info( source_aig );

  std::vector< aig_function > node_to_function( boost::num_vertices( source_aig ) );
  node_to_function[ 0u ] = { 0u, false };

  if ( source_info.constant_used )
  {
    target_info.constant_used = true;
  }

  /* copy all inputs to ensure that a miter can be constructed */
  for ( const auto& i : source_info.inputs )
  {
    const auto names = source_info.node_names;
    const std::string name = names.find( i ) != names.end() ? names.at( i ) : "";
    node_to_function[ i ] = aig_create_pi( target_aig, name );
  }

  /* visit nodes topologically sorted */
  std::vector<unsigned> topsort( boost::num_vertices( source_aig ) );
  boost::topological_sort( source_aig, topsort.begin() );

  /*
   * build nodes in single_output_function first because the newly
   * synthesized patch is functionally dependent on these nodes
   */
  for ( const auto& node : topsort )
  {
    const auto degree = out_degree( node, source_aig );

    /* constant */
    if ( node == 0u ) { continue; };

    /* and */
    if ( degree == 2u )
    {
      const auto children = get_children( source_aig, node );
      const auto left = node_to_function[ children[0u].node ];
      const auto right = node_to_function[ children[1u].node ];
      const auto and_gate = aig_create_and( target_aig,
                                            { left.node, left.complemented != children[0u].complemented },
                                            { right.node, right.complemented != children[1u].complemented } );
      node_to_function[ node ] = and_gate;
    }
  }

  for ( const auto& node : topsort )
  {
    const auto degree = out_degree( node, source_aig );

    if ( node == single_output_function.second.node )
    {
      const aig_function f = aig_add_dnf( target_aig, node_to_function, dnf, single_output_function.first );
      node_to_function[ node ] = single_output_function.second.complemented ? !f : f;
      current_output_function.second = f;
      continue;
    }

    /* constant */
    if ( node == 0u ) { continue; };

    /* and */
    if ( degree == 2u )
    {
      const auto children = get_children( source_aig, node );
      const auto left = node_to_function[ children[0u].node ];
      const auto right = node_to_function[ children[1u].node ];
      const auto and_gate = aig_create_and( target_aig,
                                            { left.node, left.complemented != children[0u].complemented },
                                            { right.node, right.complemented != children[1u].complemented } );
      node_to_function[ node ] = and_gate;
    }
  }

  for ( const auto& po : source_info.outputs )
  {
    const auto of = node_to_function[ po.first.node ];
    aig_create_po( target_aig, { of.node, po.first.complemented != of.complemented }, po.second );
  }

  return { target_aig, current_output_function };
}

aig_single_output_function_t make_single_output_function( const aig_graph& aig, const aig_function& location )
{
  /* compute the complement of the output cone but without zero and current */
  auto nodes = complemented_output_cone_as_vector( aig, location.node );
  {
    auto it = std::find( nodes.begin(), nodes.end(), 0u );
    if ( it != nodes.end() ) nodes.erase( it );
  }
  {
    auto it = std::find( nodes.begin(), nodes.end(), location.node );
    if ( it != nodes.end() ) nodes.erase( it );
  }

  std::vector< aig_function > cone;
  for ( const auto& node : nodes )
  {
    cone += aig_function( { node, false } );
  }

  return { cone, location };
}

class eco_algorithm
{
public:
  eco_algorithm( const aig_graph& circuit, const aig_graph& specification,
                 const aig_function& location, properties::ptr& statistics )
    : circuit( circuit )
    , specification( specification )
    , current_aig( circuit )
    , single_output_function( make_single_output_function( circuit, location ) )
    , current_output_function( single_output_function )
    , statistics( statistics )
  {}

  void print() const
  {
    std::cerr << "[i] assignments: x0...xn:out" << std::endl;
    for ( const auto& n : negatives )
    {
      std::cerr << n << ":0" << std::endl;
    }
    for ( const auto& p : positives )
    {
      std::cerr << p << ":1" << std::endl;
    }
  }

  std::pair< bool, aig_graph > run( unsigned num_iterations = 32u )
  {
    for ( auto i = 0u; i < num_iterations; ++i )
    {
      if ( !refine() )
      {
        std::cerr << "[i] no new counterexample --- CEGIS completed" << std::endl;
        return { true, current_aig };
      }

      if ( !learn() )
      {
        std::cerr << "[e] learning failed" << std::endl;
        return { false, current_aig };
      }
    }

    std::cerr << "[e] iteration limit exceeded" << std::endl;
    return { false, current_aig };
  }

  bool refine();
  bool learn();

private:
  iimc_modelchecker checker;
  std::set< std::string > positives, negatives;

  const aig_graph& circuit;
  const aig_graph& specification;
  aig_graph current_aig;
  const aig_single_output_function_t single_output_function;
  aig_single_output_function_t current_output_function;

  properties::ptr& statistics;
};

bool eco_algorithm::refine()
{
  const aig_graph miter = aig_miter( current_aig, specification );
  const auto counterexample = modelcheck( miter, checker );
  if ( counterexample )
  {
    // std::cerr << "[i] counterexample: " << *counterexample << std::endl;
    assert( counterexample->size() == aig_info( current_aig ).inputs.size() );
    const auto result = resimulate( circuit, current_aig, current_output_function, *counterexample );
    // std::cout << "[i] cone assignment: " << result.first << ':' << result.second << std::endl;
    assert( negatives.find( result.first ) == negatives.end() );
    assert( positives.find( result.first ) == positives.end() );
    assert( result.second == '1' || result.second == '0' );
    if ( result.second == '1' )
    {
      negatives += result.first;
    }
    else
    {
      positives += result.first;
    }
    return true;
  }
  else
  {
    return false;
  }
}

bool eco_algorithm::learn()
{
  const auto k = 1u;
  const auto dnf = learn_kDNF< minisat_solver >( positives, negatives, k );
  if ( dnf.size() == 0u )
  {
    return false;
  }

  assert( dnf.size() == k );

  const auto lit_count = count_literals( dnf );
  std::cerr << ( boost::format( "[i] %s-DNF[%s]: " ) % dnf.size() % lit_count ) << dnf << std::endl;
  set( statistics, "dnf.num_terms", dnf.size() );
  set( statistics, "dnf.num_literals", lit_count );

  const auto result = apply_fix( circuit, single_output_function, dnf );
  current_aig = result.first;
  current_output_function = result.second;
  // write_aiger( current_aig, std::cout );

  return true;
}

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string circuit_filename, spec_filename;
  aig_node location;
  unsigned iterations = 96u;

  program_options opts;
  opts.add_options()
    ( "circuit",           value( &circuit_filename ),        "Circuit filename" )
    ( "spec",              value( &spec_filename ),           "Specification filename" )
    ( "location,l",        value( &location ),                "Location" )
    ( "iterations,i",  value_with_default( &iterations ),     "Iterations" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "circuit" ) || !opts.is_set( "spec" ) || !opts.is_set( "location" ) )
  {
    std::cout << opts << std::endl;
    return 10;
  }

  auto statistics = std::make_shared<properties>();
  properties_timer *t = new properties_timer( statistics );

  /*** create AIGs. ***/
  aig_graph circuit, specification;
  try
  {
    std::cerr << boost::format( "[i] read net-list of circuit ''%s''" ) % circuit_filename << std::endl;
    read_aiger( circuit, circuit_filename );
    aig_info( circuit ).constant_used = true;
    std::cerr << boost::format( "[i] read net-list of specification ''%s''" ) % spec_filename << std::endl;
    read_aiger( specification, spec_filename );
    aig_info( specification ).constant_used = true;
  }
  catch ( const char *msg )
  {
    std::cerr << "[e] " << msg << std::endl;
    return 10;
  }

  eco_algorithm synthesizer( circuit, specification, {location,false}, statistics );
  const auto result = synthesizer.run( iterations );
  delete t;

  if ( result.first )
  {
    synthesizer.print();
    std::cerr << boost::format( "[i] run-time / size: %10.2f / %4d" ) % statistics->get<double>( "runtime_user", 0.0 ) % statistics->get( "dnf.num_literals", 0u ) << std::endl;
    write_aiger( result.second, std::cout );
    return 20;
  }

  return 10;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
