!!! 5
%html
  %head
    = Haml::Engine.new(File.read('_header.haml')).render(Object.new, :title => 'AIG Simulator')
  %body
    %nav.navbar.navbar-default
      .container-fluid
        .navbar-header
          %a.navbar-brand(href="#") CirKit
        .nav.navbar-nav
          %li
            %a(href="contents.html") Contents
    .container
      .row
        .col-md-12
          %h1 AIG Simulator
          %p
            Mathias Soeken, 2015
          %h2 Introduction
          %p
            The main function to simulate AIGs is
            %code simulate_aig
            which is found in
            %code<> classical/utils/simulate_aig.hpp
            :plain
              .
            It's interface is as follows:
          %pre.hl
            %code.cpp
              = preserve do
                :escaped
                  template<typename T>
                  std::map<aig_function, T> simulate_aig( const aig_graph& aig,
                                                          const aig_simulator<T>& simulator,
                                                          const properties::ptr& settings = properties::ptr(),
                                                          const properties::ptr& statistics = properties::ptr() );
          %p
            Parameter
            %code aig
            is the AIG that should be simulated using
            %code simulator
            that is a simulator annotating each node in the AIG with a value of type
            %code<> T
            :plain
              .
            The values of the output functions are exposed to the outside and can be accessed via
            %code<> results
            :plain
              .
            Internal values can be retrieved if a
            %code statistics
            object is given.  Then a map of type
            %code
              :escaped
                std::map<aig_node, T>
            can be accessed via the key
            %code "node_values"
            in which simulation values for all simulated nodes are stored. All simulators are implementations of the abstract class
            %code aig_simulator
            which signature reads as follows:
          %pre.hl
            %code.cpp
              = preserve do
                :escaped
                  template<typename T>
                  class aig_simulator
                  {
                  public:
                    /* these four methods need to be implemented */
                    virtual T get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const = 0;
                    virtual T get_constant() const = 0;
                    virtual T invert( const T& v ) const = 0;
                    virtual T and_op( const aig_node& node, const T& v1, const T& v2 ) const = 0;

                    /* this method can be overriden if required */
                    virtual bool terminate( const aig_node& node, const aig_graph& aig ) const { return false; }
                  };
          %p
            The first four methods need to be implemented:
          %table.table.table-striped
            %tbody
              %tr
                %td
                  %code get_input
                %td
                  Computes a simulation value based on input
                  %code
                    node
                  which has a
                  %code
                    name
                  and is at position
                  %code
                    pos
                  among all primary inputs.  For convenience the AIG can be accessed in this function. It is usually not accessible from the simulator, however, the implementation can consider this case in the constructor.
              %tr
                %td
                  %code get_constant
                %td
                  Computes a simulation value for the constant node.
              %tr
                %td
                  %code invert
                %td
                  Returns the inverted value for a simulation value.
              %tr
                %td
                  %code and_op
                %td
                  Returns the results of the AND operation on two simulation values.
          %p
            The last method
            %code
              terminate
            can be overriden in order to stop the depth-first traversal in the simulator at a given node.  All children of this node that are not reachable from other nodes will not be assigned any simulation value.
          %h2 Simulator Implementations
          %p
            Several simulators are already implemented in CirKit (hover over the class name to get a full signature of the constructors):
          %table.table.table-striped
            %thead
              %tr
                %th Class name
                %th Simulation value type
                %th Description
            %tbody
              %tr
                %td
                  %code(data-toggle="tooltip" title="simple_assignment_simulator( const aig_name_value_map& assignment )") simple_assignment_simulator
                %td
                  %code bool
                %td
                  Simulates one
                  %code assignment
                  that is given in terms of a Boolean value to each primary input, where the key to the mapping is the primary input's name.
              %tr
                %td
                  %code(data-toggle="tooltip" title="simple_node_assignment_simulator( const aig_node_value_map& assignment )") simple_node_assignment_simulator
                %td
                  %code bool
                %td
                  Simulates one
                  %code assignment
                  that is given in terms of Boolean values to selected nodes.  The simulator stops simulation at each of the nodes and does not necessarily traverse all nodes.
              %tr
                %td
                  %code(data-toggle="tooltip" title="word_assignment_simulator( const aig_name_value_map& assignment )") word_assignment_simulator
                %td
                  %code
                    :escaped
                      boost::dynamic_bitset<>
                %td
                  Same as
                  %code simple_assignment_simulator
                  but uses a bitset as assignment value for each primary input for bitwise parallel simulation.
              %tr
                %td
                  %code(data-toggle="tooltip" title="word_node_assignment_simulator( const aig_node_value_map& assignment )") word_node_assignment_simulator
                %td
                  %code
                    :escaped
                      boost::dynamic_bitset<>
                %td
                  Same as
                  %code simple_node_assignment_simulator
                  but uses a bitset as assignment value for each specified node for bitwise parallel simulation.
              %tr
                %td
                  %code(data-toggle="tooltip" title="tt_simulator()" ) tt_simulator
                %td
                  %code tt
                %td
                  Full simulation in terms of truth table annotated at each node.  The truth tables may be of different size since they each node does not have the same support size.  The truth tables are not compacted and variable order is according to the order of primary inputs.
              %tr
                %td
                  %code(data-toggle="tooltip" title="bdd_simulator(), bdd_simulator( const Cudd& mgr )" ) bdd_simulator
                %td
                  %code BDD
                %td
                  Full simulation in terms of BDDs annotated at each node.  The variable order is according to the order of primary inputs.  Besides the default constructor, there is a second one that can be given a
                  %code Cudd
                  manager object, otherwise a new manager is created for the simulation.  If the manager is created by the simulator, the object cannot be destroyed before using the simulation values.
              %tr
                %td
                  %code(data-toggle="tooltip" title="partial_simulator<T>( const aig_simulator<T>& total_simulator, const std::map<aig_node, T>& assignment, const aig_graph& aig )")
                    :escaped
                      partial_simulator<T>
                %td
                  %code T
                %td
                  This simulator is a wrapper simulator that allows partial simulation based on any of the other simulators.  It mainly makes sense to use it as a wrapper for
                  %code tt_simulator
                  and
                  %code bdd_simulator
                  since it recalculates the number of primary inputs by letting out primary inputs for which values are specified in the
                  %code assignment
                  map that is passed with the constructor.  Using this simulator one can obtain compact truth tables and BDDs even if the AIG is large and has many primary inputs by pre-assigning some of them.  These values do not necessarily need to be don't cares.
          %h2 Example
          %p
            The following example computes the BDD for each AIG output and prints it together with the name.
          %pre.hl
            %code.cpp
              = preserve do
                :escaped
                  #include <classical/utils/aig_utils.hpp>
                  #include <classical/utils/simulate_aig.hpp>

                  /* ... */

                  const auto& info = aig_info( aig );
                  bdd_simulator sim;
                  auto results = simulate_aig( aig, sim );

                  for ( const auto& output : info.outputs )
                  {
                    std::cout << "BDD for output " << output.second << ":" << std::endl;
                    results.at( output.first ).PrintMinterm();
                  }
    %script(src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js")
    %script(src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js")
    :javascript
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      } );

