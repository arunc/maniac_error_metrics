/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stores_experimental.hpp"

#include <sstream>

#include <boost/format.hpp>

#include <core/graph/depth.hpp>
#include <core/utils/range_utils.hpp>

#include <classical/functions/mig_to_aig.hpp>
#include <classical/functions/mig_from_string.hpp>
#include <classical/xmg/xmg_string.hpp>
#include <classical/xmg/xmg_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

template<>
aig_graph store_convert<mig_graph, aig_graph>( const mig_graph& mig )
{
  return mig_to_aig( mig );
}

template<>
std::string store_entry_to_string<mig_graph>( const mig_graph& mig )
{
  const auto& info = mig_info( mig );
  const auto& name = info.model_name;
  return boost::str( boost::format( "%s i/o = %d/%d" ) % ( name.empty() ? "(unnamed)" : name ) % info.inputs.size() % info.outputs.size() );
}

show_store_entry<mig_graph>::show_store_entry( program_options& opts )
{
  boost::program_options::options_description mig_options( "MIG options" );

  mig_options.add_options()
    ( "expr", "Show as string expression" )
    ;

  opts.add( mig_options );
}

bool show_store_entry<mig_graph>::operator()( mig_graph& mig, const std::string& dotname, const program_options& opts, const properties::ptr& settings )
{
  expressions.clear();
  if ( opts.is_set( "expr" ) )
  {
    for ( const auto& output : mig_info( mig ).outputs )
    {
      const auto expr = mig_to_string( mig, output.first );
      expressions.push_back( expr );
      std::cout << boost::format( "[i] %s: %s" ) % output.second % expr << std::endl;
    }
  }

  write_dot( mig, dotname, settings );

  return true;
}

command_log_opt_t show_store_entry<mig_graph>::log() const
{
  if ( !expressions.empty() )
  {
    return command_log_opt_t({
        {"expressions", expressions}
      });
  }
  else
  {
    return boost::none;
  }
}

template<>
void print_store_entry_statistics<mig_graph>( std::ostream& os, const mig_graph& mig )
{
  mig_print_stats( mig, os );
}

template<>
command_log_opt_t log_store_entry_statistics<mig_graph>( const mig_graph& mig )
{
  const auto& info = mig_info( mig );

  std::vector<mig_node> outputs;
  for ( const auto& output : info.outputs )
  {
    outputs += output.first.node;
  }

  std::vector<unsigned> depths;
  const auto depth = compute_depth( mig, outputs, depths );

  return command_log_opt_t({
      {"inputs", static_cast<int>( info.inputs.size() )},
      {"outputs", static_cast<int>( info.outputs.size() )},
      {"size", static_cast<int>( boost::num_vertices( mig ) - info.inputs.size() - 1u )},
      {"depth", depth},
      {"complemented_edges", number_of_complemented_edges( mig )},
      {"inverters", number_of_inverters( mig )}
    });
}

template<>
std::string store_entry_to_string<xmg_graph>( const xmg_graph& xmg )
{
  const auto name = xmg.name();
  return boost::str( boost::format( "%s i/o = %d/%d" ) % ( name.empty() ? "(unnamed)" : name ) % xmg.inputs().size() % xmg.outputs().size() );
}

template<>
void print_store_entry_statistics<xmg_graph>( std::ostream& os, const xmg_graph& xmg )
{
  xmg_print_stats( xmg, os );
}

show_store_entry<xmg_graph>::show_store_entry( program_options& opts )
{
  /* reuse the option provided by MIG */
}

bool show_store_entry<xmg_graph>::operator()( xmg_graph& xmg, const std::string& dotname, const program_options& opts, const properties::ptr& settings )
{
  expressions.clear();
  if ( opts.is_set( "expr" ) )
  {
    for ( const auto& output : xmg.outputs() )
    {
      const auto expr = xmg_to_string( xmg, output.first );
      expressions.push_back( expr );
      std::cout << boost::format( "[i] %s: %s" ) % output.second % expr << std::endl;
    }
  }

  return false; /* don't open dot viewer */
}

command_log_opt_t show_store_entry<xmg_graph>::log() const
{
  if ( !expressions.empty() )
  {
    return command_log_opt_t({
        {"expressions", expressions}
      });
  }
  else
  {
    return boost::none;
  }
}

template<>
command_log_opt_t log_store_entry_statistics<xmg_graph>( const xmg_graph& xmg )
{
  return command_log_opt_t({
      {"inputs", static_cast<unsigned>( xmg.inputs().size() )},
      {"outputs", static_cast<unsigned>( xmg.outputs().size() )},
      {"size", xmg.num_gates()},
      {"maj", xmg.num_maj()},
      {"xor", xmg.num_xor()},
      {"depth", compute_depth( xmg )}
    });
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
