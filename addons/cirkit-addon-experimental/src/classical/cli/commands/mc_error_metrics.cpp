/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2016  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_error_metrics.hpp"

#include <boost/format.hpp>

#include <core/cli/rules.hpp>
#include <core/cli/store.hpp>
#include <core/utils/range_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
namespace { 
char *  curr_time () {
  time_t now = time(0);
  return ctime(&now);
}

float get_elapsed_time ( const clock_t &begin_time ) {
  return float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
}


bdd_function_t cudd_aig_to_bdd( const aig_graph& aig, Cudd& mgr )
{
  bdd_simulator sim( mgr );

  const auto res = simulate_aig( aig, sim );
  std::vector<BDD> fs;

  for ( const auto& output : aig_info( aig ).outputs )
  {
    fs.push_back( res.at( output.first ) );
  }

  return {mgr, fs};
}
}
/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
using namespace boost::program_options;

mc_error_metrics_command::mc_error_metrics_command( const environment::ptr& env )
  : command( env, "MANIAC Error Metrics calculator for Combinational Circuits (standalone, CUDD)" )
{
  opts.add_options()
    ( "golden,g",  value (&golden), "Golden AIG circuit file (.aag)" )
    ( "approx,a",  value (&approx), "Approx AIG circuit file (.aag)" )
    ( "report,r",  value_with_default( &report ), "Output error metrics report" )
    ( "signed_output,s",     value_with_default( &sign_out ), "Not implemented. Write a wrapper" )
    ( "port,p",    value_with_default( &port ),  "Not implemented. Write a wrapper" )
    ( "debug,d",   value_with_default( &debug ), "Debug level" )
    ;
  be_verbose();
}

command::rules_t mc_error_metrics_command::validity_rules() const
{
  return {
    {[&]() { return opts.is_set( "golden" ) && opts.is_set( "approx" ); }, "golden and approx must be set" },
    {[&]() { return sign_out <= 1u; }, "Signed Output not implemented. Write a wrapper instead" },
    {[&]() { return port == "__NA__"; }, "Port cannot be set for now. Write a wrapper instead" }
  };
}

bool mc_error_metrics_command::execute()
{
  using boost::format;
  auto golden_aig = aig_graph ();
  auto approx_aig = aig_graph ();
  std::cout << "[i] MANIAC Error Metrics Calculator for combination circuits. (CUDD based) :: "
	    << curr_time();
  auto begin_time = std::clock();

  // 1. Read the circuits.
  if ( boost::ends_with( golden, "aag" ) ) 
    read_aiger( golden_aig, golden );
  else if ( boost::ends_with( golden, "aig" ) ) 
    read_aiger_binary( golden_aig, golden, opts.is_set( "nostrash" ) );
  else 
    assert ( false && "Only AIG or AAG supported!");

  if ( boost::ends_with( approx, "aag" ) )
    read_aiger( approx_aig, approx );
  else if ( boost::ends_with( approx, "aig" ) )
    read_aiger_binary( approx_aig, approx, opts.is_set( "nostrash" ) );
  else 
    assert ( false && "Only AIG or AAG supported!");
    
  // 2. Convert to bdd_function_t
  //auto cudd_mgr = std::make_shared<Cudd>();
  auto cudd_mgr = new Cudd(); 
  auto f = cudd_aig_to_bdd ( golden_aig, *cudd_mgr ); 
  auto fhat = cudd_aig_to_bdd ( approx_aig, *cudd_mgr ); 

  // 3. Get the error-metrics
  auto err_count = error_rate (f, fhat);
  auto num_inputs = cudd_mgr->ReadSize();
  double er = (double)err_count / (1ull << num_inputs) ;
  auto wc = worst_case (f, fhat);
  if (debug > 0) std::cout << "[w] NO average case supported for now.\n";
  if (debug > 0) std::cout << "[w] for max bit-flips use \'find_max_bit_flips\' program.\n";
  auto total_time = get_elapsed_time (begin_time);

  // 4. Print, Report 
  auto golden_size = f.first.nodeCount( f.second );
  auto approx_size = fhat.first.nodeCount( fhat.second );
  std::cout << "[i] worst-case error   :  " << wc << "\n";
  std::cout << "[i] error-rate         :  " << er << "\n";
  std::cout << "[i] average-case error :  NA \n";  
  std::cout << "[i] max bit-flip error :  NA \n";  
  std::cout << "[i] total run time(s)  :  " << total_time << "\n";  

  std::ofstream rpt;
  rpt.open (report);
  rpt << "MANIAC Error Metrics Calculator for combination circuits. (CUDD based) :: " 
      << curr_time();
  rpt << "golden ckt file    : " << golden << "\n";
  rpt << "approx ckt file    : " << approx << "\n";
  rpt << "worst-case error   : " << wc << "\n";
  rpt << "error-rate         : " << er << "\n";
  rpt << "error-count        : " << err_count << "\n";
  rpt << "total run time(s)  : " << total_time << "\n";
  rpt << "average-case error : NA \n";  
  rpt << "max bit-flip error : NA \n";  
  if (debug > 5) rpt << "golden total nodes : " << golden_size << "\n";
  if (debug > 5) rpt << "approx total nodes : " << approx_size << "\n";
  rpt.close();

  if (debug > 0) std::cout << "[i] finish time :: " << curr_time();
  return true;

}

} // namespace cirkit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
