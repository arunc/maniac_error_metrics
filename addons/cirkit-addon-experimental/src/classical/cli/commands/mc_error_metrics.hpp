/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2016  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_error_metrics.hpp
 *
 * @brief Dump the error metrics. 
 *        Inputs are golden and approximate circuits.
 *        Output is a report for worst-case, avg-case errors and error-rate.
 *
 * IMPORTANT : This is a standalone program. Does NOT read or write to 
 *             cirkit STORE. But invoked as a command inside cirkit.
 *
 * @author Arun
 * @since  2.3
 */

#ifndef CLI_MC_ERROR_METRICS_COMMAND_HPP
#define CLI_MC_ERROR_METRICS_COMMAND_HPP

#include <core/cli/command.hpp>

#include <cassert>
#include <ctime>
#include <fstream>
#include <string>
#include <boost/algorithm/string/predicate.hpp>

#include <core/utils/timer.hpp>

#include <classical/functions/simulate_aig.hpp>
#include <classical/functions/strash.hpp>
#include <classical/aig.hpp>
#include <classical/io/read_aiger.hpp>
#include <classical/utils/aig_utils.hpp>
#include <classical/approximate/cudd.hpp>

namespace cirkit
{

class mc_error_metrics_command : public command
{
public:
  mc_error_metrics_command( const environment::ptr& env );

protected:
  rules_t validity_rules() const;
  bool execute();

private:
  std::string golden, approx;
  std::string port = "__NA__";
  unsigned debug = 1u;
  unsigned sign_out = 0u;
  std::string report = "maniac_error_metrics_report.rpt";
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
