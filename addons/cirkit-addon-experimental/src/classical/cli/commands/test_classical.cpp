/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_classical.hpp"

#include <iostream>

#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/algorithm.hpp>

#include <core/cli/stores.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/aig.hpp>
#include <classical/aig.hpp>
#include <classical/approximate/approximate.hpp>
#include <classical/approximate/cudd.hpp>
#include <classical/approximate/error_metrics.hpp>
#include <classical/cli/stores.hpp>
#include <classical/dd/size.hpp>
#include <classical/functions/aig_from_truth_table.hpp>
#include <classical/functions/compute_levels.hpp>
#include <classical/functions/parallel_compute.hpp>
#include <classical/synthesis/akers_synthesis.hpp>
#include <classical/utils/aig_utils.hpp>
#include <classical/utils/expression_parser.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

test_classical_command::test_classical_command( const environment::ptr& env )
  : command( env, "Test classical functions" )
{
  opts.add_options()
    ( "approximate_cudd", "Test approximate CUDD code" )
    ( "approximate",      "Test approximate" )
    ( "parallel",         "Test parallel code" )
    ( "akers",            "Test Aker's synthesis" )
    ( "inverters",        "Levels and inverters" )
    ( "parser",           "Test expression parser" )
    ;
}

bool test_classical_command::execute()
{
  if ( opts.is_set( "approximate_cudd" ) )
  {
    const auto& aigs = env->store<aig_graph>();
    auto& bdds = env->store<bdd_function_t>();

    auto statistics = std::make_shared<properties>();

    const auto f = approximate2( aigs.current(), {0u, 1u, 2u, 3u}, {}, properties::ptr(), statistics );

    auto settings = std::make_shared<properties>();
    settings->set( "bdd_manager", statistics->get<std::shared_ptr<Cudd>>( "bdd_manager" ) );
    const auto fhat = approximate2( aigs.current(), {1u, 3u, 2u, 0u}, {{4u, 2u}}, settings, statistics );

    std::cout << "[i] error rate: " << error_rate( f, fhat ) << std::endl;

    bdds.extend();
    bdds.current() = f;

    bdds.extend();
    bdds.current() = fhat;
  }

  if ( opts.is_set( "approximate" ) )
  {
    const auto aig = aig_from_truth_table( boost::dynamic_bitset<>( std::string( "10010110" ) ) );

    /* own dd package */
    {
      auto statistics = std::make_shared<properties>();

      const auto f = approximate( aig, {0u, 1u, 2u}, {}, properties::ptr(), statistics );

      auto settings = std::make_shared<properties>();
      settings->set( "bdd_manager", statistics->get<bdd_manager_ptr>( "bdd_manager" ) );

      const auto fhat =
          approximate( aig, {2u, 0u, 1u},
                       {{bdd_level_approximation_mode::round_down, 1u},
                        {bdd_level_approximation_mode::round_up, 0u}},
                       settings, statistics );

      std::cout << "[i] error rate:   " << error_rate( f, fhat ) << std::endl
                << "[i] worst case:   " << worst_case( f, fhat ) << std::endl
                << "[i] average case: " << average_case( f, fhat ) << std::endl
                << "[i] old size:     " << dd_size( f ) << std::endl
                << "[i] new size:     " << dd_size( fhat ) << std::endl
                << boost::format( "[i] run-time:     %.2f secs" ) % statistics->get<double>( "runtime" ) << std::endl;
    }

    /* CUDD */
    {
      auto statistics = std::make_shared<properties>();

      const auto f = approximate2( aig, {0u, 1u, 2u}, {}, properties::ptr(), statistics );

      auto settings = std::make_shared<properties>();
      settings->set( "bdd_manager", statistics->get<std::shared_ptr<Cudd>>( "bdd_manager" ) );

      const auto fhat =
          approximate2( aig, {2u, 0u, 1u},
                        {{0u, 1u},
                         {1u, 0u}},
                        settings, statistics );

      std::cout << "[i] error rate:   " << error_rate( f, fhat ) << std::endl
                << "[i] worst case:   " << worst_case( f, fhat ) << std::endl
                //<< "[i] average case: " << average_case( f, fhat ) << std::endl
                << "[i] old size:     " << f.first.nodeCount( f.second ) << std::endl
                << "[i] new size:     " << fhat.first.nodeCount( fhat.second ) << std::endl
                << boost::format( "[i] run-time:     %.2f secs" ) % statistics->get<double>( "runtime" ) << std::endl;
    }
  }

  if ( opts.is_set( "parallel" ) )
  {
    const auto& aigs = env->store<aig_graph>();
    parallel_simulate( aigs.current(), boost::dynamic_bitset<>( aig_info( aigs.current() ).inputs.size() ) );
  }

  if ( opts.is_set( "akers" ) )
  {
    //    akers_synthesis( boost::dynamic_bitset<>( std::string( "01110010" ) ),
    //                 boost::dynamic_bitset<>( std::string( "11110110" ) ) );
    akers_synthesis( boost::dynamic_bitset<>( std::string( "01001110" ) ),
                     boost::dynamic_bitset<>( std::string( "01101111" ) ) );

    const auto& bdds = env->store<bdd_function_t>();
    if ( !bdds.empty() )
    {
      akers_synthesis( bdds.current() );
    }
  }

  if ( opts.is_set( "inverters" ) )
  {
    const auto& aigs = env->store<aig_graph>();

    if ( aigs.empty() )
    {
      std::cout << "[w] cannot test inverter code, because there is no AIG in the store" << std::endl;
      return true;
    }

    const auto& aig = aigs.current();

    /* returns a vector that maps levels to nodes */
    const auto l = levelize_nodes( aig );

    /* our variables to store the information */
    const auto depth = l.size();
    auto levels_with_ces = 0u;
    std::vector<unsigned> num_ces( depth, 0u ); /* level -> # of complemented edges */
    std::vector<unsigned> num_res( depth, 0u ); /* level -> # of regular edges */
    std::vector<unsigned> level_nodes( depth ); /* level -> # of nodes */

    /* compute the information */
    for ( const auto& nodes : index( l ) )
    {
      auto has_ce = false;
      level_nodes[nodes.index] = nodes.value.size();

      for ( const auto& node : nodes.value )
      {
        if ( out_degree( node, aig ) == 0u ) { continue; }

        const auto children = get_children( aig, node );

        for ( auto i = 0u; i < 2u; ++i )
        {
          if ( children[i].complemented )
          {
            num_ces[nodes.index]++;
            has_ce = true;
          }
          else
          {
            num_res[nodes.index]++;
          }
        }
      }

      if ( has_ce )
      {
        ++levels_with_ces;
      }
    }

    /* print the information */
    std::cout << "[i] depth:             " << depth << std::endl
              << "[i] levels w/ CEs:     " << levels_with_ces << std::endl
              << "[i] levels w/o CES:    " << ( depth - levels_with_ces ) << std::endl
              << "[i] nodes per level:   " << any_join( level_nodes, ", " ) << std::endl
              << "[i] num CEs per level: " << any_join( num_ces, ", " ) << std::endl
              << "[i] num REs per level: " << any_join( num_res, ", " ) << std::endl;
  }

  if ( opts.is_set( "parser" ) )
  {
    std::cout << parse_expression( "!!!<![1!!b]!!!!(a!0)!{a{ac}}>" ) << std::endl;
  }

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
