/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file commands.hpp
 *
 * @brief Experimental classical commands
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef CLI_EXPERIMENTAL_CLASSICAL_COMMANDS_HPP
#define CLI_EXPERIMENTAL_CLASSICAL_COMMANDS_HPP

#include <classical/cli/stores_experimental.hpp>

#include <classical/cli/commands/control_logic.hpp>
#include <classical/cli/commands/controllable.hpp>
#include <classical/cli/commands/cuts.hpp>
#include <classical/cli/commands/dbg_stat.hpp>
#include <classical/cli/commands/depth.hpp>
#include <classical/cli/commands/funchash.hpp>
#include <classical/cli/commands/inject.hpp>
#include <classical/cli/commands/memristor.hpp>
#include <classical/cli/commands/mig.hpp>
#include <classical/cli/commands/mig_decomp.hpp>
#include <classical/cli/commands/mig_rewrite.hpp>
#include <classical/cli/commands/miter.hpp>
#include <classical/cli/commands/plim.hpp>
#include <classical/cli/commands/read_words.hpp>
#include <classical/cli/commands/repair.hpp>
#include <classical/cli/commands/shuffle.hpp>
#include <classical/cli/commands/simulate.hpp>
#include <classical/cli/commands/test_classical.hpp>
#include <classical/cli/commands/mc_error_metrics.hpp>
#include <classical/cli/commands/words.hpp>
#include <classical/cli/commands/write_verilog.hpp>

#undef  STORE_TYPES
#define STORE_TYPES aig_graph, mig_graph, xmg_graph, counterexample_t, simple_fanout_graph_t, std::vector<aig_node>, tt, bdd_function_t

#define EXPERIMENTAL_CLASSICAL_COMMANDS \
    ADD_COMMAND( control_logic );       \
    ADD_COMMAND( controllable );        \
    ADD_COMMAND( cuts );                \
    ADD_COMMAND( dbg_stat );            \
    ADD_COMMAND( depth );               \
    ADD_COMMAND( funchash );            \
    ADD_COMMAND( inject );              \
    ADD_COMMAND( mc_error_metrics );    \
    ADD_COMMAND( memristor );           \
    ADD_COMMAND( mig );                 \
    ADD_COMMAND( mig_decomp );          \
    ADD_COMMAND( mig_rewrite );         \
    ADD_COMMAND( miter );               \
    ADD_COMMAND( plim );                \
    ADD_COMMAND( read_words );          \
    ADD_COMMAND( repair );              \
    ADD_COMMAND( shuffle );             \
    ADD_COMMAND( simulate );            \
    ADD_COMMAND( test_classical );      \
    ADD_COMMAND( words );               \
    ADD_COMMAND( write_verilog );

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
