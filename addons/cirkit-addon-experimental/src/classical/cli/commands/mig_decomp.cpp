/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig_decomp.hpp"

#include <classical/cli/stores_experimental.hpp>
#include <classical/functions/mig_decomposition.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_decomp_command::mig_decomp_command( const environment::ptr& env )
  : aig_base_command( env, "Majority decomposition" )
{
  opts.add_options()
    ( "new,n", "Add new entry to store" )
    ;
  be_verbose();
}

bool mig_decomp_command::execute()
{
  auto& migs = env->store<mig_graph>();

  if ( migs.empty() || opts.is_set( "new" ) )
  {
    migs.extend();
  }

  const auto settings = make_settings();
  migs.current() = mig_decomposition( aig(), settings );
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
