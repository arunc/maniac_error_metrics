/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file stores_experimental.hpp
 *
 * @brief Meta-data for stores
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef CLI_CLASSICAL_STORES_EXPERIMENTAL_HPP
#define CLI_CLASSICAL_STORES_EXPERIMENTAL_HPP

#include <core/cli/store.hpp>

#include <string>
#include <vector>

#include <core/properties.hpp>

#include <classical/aig.hpp>
#include <classical/mig.hpp>
#include <classical/utils/mig_utils.hpp>
#include <classical/xmg/xmg.hpp>

namespace cirkit
{

template<>
inline bool store_can_convert<mig_graph, aig_graph>() { return true; }

template<>
aig_graph store_convert<mig_graph, aig_graph>( const mig_graph& mig );

/******************************************************************************
 * mig_graph                                                                  *
 ******************************************************************************/

template<>
struct store_info<mig_graph>
{
  static constexpr const char* key         = "migs";
  static constexpr const char* option      = "mig";
  static constexpr const char* mnemonic    = "m";
  static constexpr const char* name        = "MIG";
  static constexpr const char* name_plural = "MIGs";
};

template<>
std::string store_entry_to_string<mig_graph>( const mig_graph& mig );

template<>
struct show_store_entry<mig_graph>
{
  show_store_entry( program_options& opts );

  bool operator()( mig_graph& mig, const std::string& dotname, const program_options& opts, const properties::ptr& settings );

  command_log_opt_t log() const;

private:
  std::vector<std::string> expressions;
};

template<>
void print_store_entry_statistics<mig_graph>( std::ostream& os, const mig_graph& mig );

template<>
command_log_opt_t log_store_entry_statistics<mig_graph>( const mig_graph& mig );

/******************************************************************************
 * xmg_graph                                                                  *
 ******************************************************************************/

template<>
struct store_info<xmg_graph>
{
  static constexpr const char* key         = "xmgs";
  static constexpr const char* option      = "xmg";
  static constexpr const char* mnemonic    = "x";
  static constexpr const char* name        = "XMG";
  static constexpr const char* name_plural = "XMGs";
};

template<>
std::string store_entry_to_string<xmg_graph>( const xmg_graph& xmg );

template<>
struct show_store_entry<xmg_graph>
{
  show_store_entry( program_options& opts );

  bool operator()( xmg_graph& xmg, const std::string& dotname, const program_options& opts, const properties::ptr& settings );

  command_log_opt_t log() const;

private:
  std::vector<std::string> expressions;
};

template<>
void print_store_entry_statistics<xmg_graph>( std::ostream& os, const xmg_graph& xmg );

template<>
command_log_opt_t log_store_entry_statistics<xmg_graph>( const xmg_graph& xmg );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
