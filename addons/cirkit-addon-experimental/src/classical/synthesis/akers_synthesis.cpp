/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "akers_synthesis.hpp"

#include <iostream>
#include <string>
#include <vector>

#include <cudd.h>
#include <cuddInt.h>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext/iota.hpp>

#include <core/utils/range_utils.hpp>

extern "C"
{
#include <extra.h>
}

using namespace boost::assign;
using boost::adaptors::reversed;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

class unitized_table
{
public:
  using row_t = boost::dynamic_bitset<>;

  unitized_table( const std::string& columns )
    : columns( columns )
  {
  }

  row_t create_row() const
  {
    return row_t( columns.size() );
  }

  void add_row( const row_t& row )
  {
    rows += row;
  }

  void reduce()
  {
    auto progress = true;

    while ( progress )
    {
      progress = false;

      progress = progress || reduce_columns() || reduce_rows();
    }
  }

  friend std::ostream& operator<<( std::ostream& os, const unitized_table& table );

private:
  bool reduce_rows()
  {
    std::vector<unsigned> to_be_removed;

    for ( auto i = 0u; i < rows.size(); ++i )
    {
      for ( auto j = i + 1u; j < rows.size(); ++j )
      {
        if ( rows[i].is_subset_of( rows[j] ) )
        {
          to_be_removed += j;
        }
        if ( rows[j].is_subset_of( rows[i] ) )
        {
          to_be_removed += i;
        }
      }
    }

    boost::sort( to_be_removed );
    to_be_removed.erase( std::unique( to_be_removed.begin(), to_be_removed.end() ), to_be_removed.end() );
    boost::reverse( to_be_removed );

    for ( auto index : to_be_removed )
    {
      rows.erase( rows.begin() + index );
    }

    /* we had improvement if we removed something */
    return !to_be_removed.empty();
  }

  bool reduce_columns()
  {
    std::vector<unsigned> to_be_removed;

    auto mask = ~create_row();

    for ( auto c = 0u; c < columns.size(); ++c )
    {
      mask.reset( c );
      auto can_be_removed = true;

      /* pretend column c is removed */
      for ( auto i = 0u; i < rows.size(); ++i )
      {
        for ( auto j = i + 1u; j < rows.size(); ++j )
        {
          const auto result = rows[i] & rows[j] & mask;

          if ( result.none() )
          {
            can_be_removed = false;
            break;
          }
        }

        if ( !can_be_removed )
        {
          break;
        }
      }

      if ( can_be_removed )
      {
        to_be_removed += c;
      }
      else
      {
        mask.set( c );
      }
    }

    /* remove columns */
    boost::reverse( to_be_removed );

    for ( auto index : to_be_removed )
    {
      columns.erase( columns.begin() + index );

      for ( auto& row : rows )
      {
        erase_bit( row, index );
      }
    }

    return !to_be_removed.empty();
  }

  void erase_bit( row_t& row, unsigned pos )
  {
    for ( auto i = pos + 1u; i < row.size(); ++i )
    {
      row[i - 1u] = row[i];
    }
    row.resize( row.size() - 1u );
  }

  std::string        columns;

  std::vector<row_t> rows;
};

std::ostream& operator<<( std::ostream& os, const unitized_table& table )
{
  os << table.columns << std::endl;

  for ( const auto& row : table.rows )
  {
    os << ( to_string( row ) | reversed ) << std::endl;
  }

  return os;
}

using zdd_function_t = std::pair<Cudd, std::vector<ZDD>>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

unitized_table create_unitized_table( const tt& func, const tt& care )
{
  const auto num_vars = tt_num_vars( func );

  /* create column names */
  std::string columns;

  for ( auto i = 0u; i < num_vars; ++i )
  {
    columns += 'a' + i;
  }
  for ( auto i = 0u; i < num_vars; ++i )
  {
    columns += 'A' + i;
  }
  columns += '0';
  columns += '1';

  unitized_table table( columns );

  /* add rows */
  foreach_bit( care, [&num_vars, &func, &table]( unsigned pos ) {
      boost::dynamic_bitset<> half( num_vars, pos );
      auto row = table.create_row();

      /* copy the values */
      for ( auto i = 0u; i < num_vars; ++i )
      {
        row[i] = half[i];
        row[i + num_vars] = !half[i];
      }
      row[num_vars << 1] = false;
      row[(num_vars << 1) + 1] = true;

      if ( !func[pos] )
      {
        row.flip();
      }
      table.add_row( row );
    } );

  std::cout << "AFTER CREATION" << std::endl;
  std::cout << table << std::endl;

  table.reduce();

  return table;
}

BDD make_lpsd( const Cudd& mgr, const BDD& f )
{
  const auto n = ( mgr.ReadSize() - 2 ) >> 1;

  auto result = f & !mgr.bddVar( n + n ) & mgr.bddVar( n + n + 1 );
  for ( auto i = 0u; i < n; ++i )
  {
    result &= ( mgr.bddVar( i ) ^ mgr.bddVar( n + i ) );
  }

  return result;
}

DdNode* create_unitized_table( const bdd_function_t& f )
{
  auto& mgr = f.first; /* Cudd */
  const auto& func = f.second[0];
  const auto& care = f.second[1];

  const auto num_vars = mgr.ReadSize();

  /* create auxiliary variables
     x_1' ... x_n' 0 1
   */
  for ( auto i = 0u; i < num_vars + 2u; ++i )
  {
    mgr.bddVar();
  }

  /* lpsd-ify function */
  auto on = make_lpsd( mgr, func & care );
  auto off = make_lpsd( mgr, !func & care );
  for ( auto i = 0u; i < mgr.ReadSize(); ++i )
  {
    off = off.Compose( !mgr.bddVar( i ), i );
  }
  auto lpsd = on | off;

  //std::cout << "[i] lpsd function" << std::endl;
  //lpsd.PrintMinterm();

  /* create ZDD */
  mgr.zddVarsFromBddVars( 1 );

  /* switch to C interface */
  auto _mgr = mgr.getManager();
  auto _t = Cudd_zddPortFromBdd( _mgr, lpsd.getNode() );
  Cudd_Ref( _t );
  DdNode *tmp;

  /* list of columns */
  const auto t_size = num_vars + num_vars + 2u;
  std::vector<unsigned> columns( t_size );
  boost::iota( columns, 0u );

  //std::cout << "[i] initial t:" << std::endl;
  //Cudd_zddPrintMinterm( _mgr, _t );

  auto improvement = true;
  while ( improvement )
  {
    improvement = false;

    // reduce rows
    if ( ( tmp = Extra_zddMinimal( _mgr, _t ) ) != _t )
    {
      Cudd_Ref( tmp );
      Cudd_RecursiveDerefZdd( _mgr, _t );
      improvement = true;
      _t = tmp;
    }

    // reduce columns
    //std::cout << "[i] reduce columns in " << any_join( columns, " " ) << std::endl;
    auto cidx = 0u;
    while ( cidx < columns.size() )
    {
      const auto c = columns[cidx];

      //std::cout << "[i] try to remove " << c << std::endl;

      // build m
      int * var_values = new int[t_size];
      std::fill( var_values, var_values + t_size, 0u );

      for ( auto j = 0u; j < columns.size(); ++j )
      {
        if ( j != cidx )
        {
          var_values[columns[j]] = 1u;
        }
      }

      auto _m = Extra_zddCombination( _mgr, var_values, t_size );
      delete[] var_values;

      //std::cout << "[i] _m:" << std::endl;
      //Cudd_zddPrintMinterm( _mgr, _m );

      auto _s = Extra_zddCrossProduct( _mgr, _t, _m );
      Cudd_Ref( _s );
      Cudd_RecursiveDerefZdd( _mgr, _m );

      //std::cout << "[i] _s:" << std::endl;
      //Cudd_zddPrintMinterm( _mgr, _s );

      tmp = Extra_zddCrossProduct( _mgr, _s, _s );
      Cudd_Ref( tmp );

      auto is_contained = Extra_zddEmptyBelongs( _mgr, tmp );
      Cudd_RecursiveDerefZdd( _mgr, tmp );

      if ( !is_contained )
      {
        Cudd_RecursiveDerefZdd( _mgr, _t );
        _t = _s;
        columns.erase( columns.begin() + cidx );
        improvement = true;
      }
      else
      {
        Cudd_RecursiveDerefZdd( _mgr, _s );
        ++cidx;
      }
    }
  }

  //std::cout << "[i] final t:" << std::endl;
  //Cudd_zddPrintMinterm( _mgr, _t );

  return _t;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_graph akers_synthesis( const tt& func, const tt& care,
                           const properties::ptr& settings,
                           const properties::ptr& statistics )
{
  auto utable = create_unitized_table( func, care );

  std::cout << utable << std::endl;

  // do synthesis

  //assert( false );
  return mig_graph();
}

mig_graph akers_synthesis( const bdd_function_t& f,
                           const properties::ptr& settings,
                           const properties::ptr& statistics )
{
  /* f is incompletely specified */
  assert( f.second.size() >= 2u );

  auto utable = create_unitized_table( f );
  Cudd_RecursiveDerefZdd( f.first.getManager(), utable );

  std::cout << "done" << std::endl;

  // do synthesis

  assert( false );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
