/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file approximate.hpp
 *
 * @brief Helpers for approximate computing
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef APPROXIMATE_HPP
#define APPROXIMATE_HPP

#include <vector>

#include <core/properties.hpp>
#include <core/utils/bdd_utils.hpp>
#include <classical/aig.hpp>
#include <classical/approximate/bdd_level_approximation.hpp>
#include <classical/dd/bdd.hpp>

namespace cirkit
{

std::vector<bdd> approximate(
    const aig_graph& aig, const std::vector<unsigned>& ordering,
    const std::vector<std::pair<bdd_level_approximation_mode, unsigned>>& operations,
    const properties::ptr& settings = properties::ptr(),
    const properties::ptr& statistics = properties::ptr() );

bdd_function_t approximate2(
    const aig_graph& aig, const std::vector<unsigned>& ordering,
    const std::vector<std::pair<unsigned, unsigned>>& operations,
    const properties::ptr& settings = properties::ptr(),
    const properties::ptr& statistics = properties::ptr() );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
