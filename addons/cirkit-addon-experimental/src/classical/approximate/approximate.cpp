/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "approximate.hpp"

#include <core/utils/timer.hpp>
#include <classical/approximate/cudd.hpp>
#include <classical/dd/aig_to_cirkit_bdd.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

aig_graph reordered_aig( const aig_graph& aig, const std::vector<unsigned>& ordering )
{
  /* reorder inputs in AIG */
  std::map<unsigned, unsigned> reorder;
  for ( auto i = 0u; i < ordering.size(); ++i )
  {
    reorder[i] = ordering[i];
  }

  auto s_settings = std::make_shared<properties>();
  s_settings->set( "reorder", reorder );
  return strash( aig, s_settings );
}

bdd_function_t aig_to_bdd( const aig_graph& aig, Cudd& mgr )
{
  bdd_simulator sim( mgr );

  const auto res = simulate_aig( aig, sim );
  std::vector<BDD> fs;

  for ( const auto& output : aig_info( aig ).outputs )
  {
    fs.push_back( res.at( output.first ) );
  }

  return {mgr, fs};
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

std::vector<bdd> approximate(
    const aig_graph& aig, const std::vector<unsigned>& ordering,
    const std::vector<std::pair<bdd_level_approximation_mode, unsigned>>& operations,
    const properties::ptr& settings, const properties::ptr& statistics )
{
  /* settings */
  const auto bdd_verbose  = get( settings, "bdd_verbose", false );
  const auto log_max_objs = get( settings, "log_max_objs", 16u );
  const auto verbose      = get( settings, "verbose", false );
        auto mgr          = get( settings, "bdd_manager", bdd_manager::create( aig_info( aig ).inputs.size(), log_max_objs, bdd_verbose ) );

  /* statistics */
  properties_timer t( statistics );
  set( statistics, "bdd_manager", mgr );

  /* reorder inputs in AIG */
  const auto aig_copy = reordered_aig( aig, ordering );

  /* create BDD from reordered AIG */
  auto f = aig_to_bdd( aig_copy, mgr );

  /* perform operations */
  for ( const auto& op : operations )
  {
    f = bdd_level_approximation( f, op.first, op.second );
  }

  return f;
}

bdd_function_t approximate2(
    const aig_graph& aig, const std::vector<unsigned>& ordering,
    const std::vector<std::pair<unsigned, unsigned>>& operations,
    const properties::ptr& settings,
    const properties::ptr& statistics )
{
  /* settings */
  const auto verbose = get( settings, "verbose", false );
        auto mgr     = get( settings, "bdd_manager", std::make_shared<Cudd>() );

  /* statistics */
  properties_timer t( statistics );
  set( statistics, "bdd_manager", mgr );

  /* reorder inputs in AIG */
  const auto aig_copy = reordered_aig( aig, ordering );

  /* create BDD from reordered AIG */
  auto f = aig_to_bdd( aig_copy, *mgr );

  /* perform operations */
  for ( const auto& op : operations )
  {
    f = bdd_level_approximation( f, op.first, op.second );
  }

  return f;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
