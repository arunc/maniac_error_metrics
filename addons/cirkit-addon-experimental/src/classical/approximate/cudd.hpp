/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file cudd.hpp
 *
 * @brief Cudd-based implementations of BDD approximation
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef APPROXIMATE_CUDD_HPP
#define APPROXIMATE_CUDD_HPP

#include <unordered_map>
#include <vector>

#include <boost/multiprecision/cpp_int.hpp>

#include <cudd.h>

#include <core/properties.hpp>
#include <core/utils/bdd_utils.hpp>
#include <classical/approximate/bdd_level_approximation.hpp>

namespace cirkit
{

using cudd_count_map_t = std::unordered_map<const DdNode*, boost::multiprecision::uint256_t>;

cudd_count_map_t count_solutions( DdManager* dd, const std::vector<DdNode*>& fs );

/* the type of level is DdNode* only to use the Cudd caching functions,
   one can think of them being unsigned */
DdNode* round_down( DdManager* dd, DdNode* f, DdNode* level );
DdNode* round_up( DdManager* dd, DdNode* f, DdNode* level );
DdNode* round( DdManager* dd, DdNode* f, DdNode* level );

/* Arithmetic */

bdd_function_t bdd_subtract( const bdd_function_t& minuend, const bdd_function_t& subtrahend );
bdd_function_t bdd_abs( const bdd_function_t& n );

bdd_function_t zero_extend( const bdd_function_t& n, unsigned to );
bdd_function_t sign_extend( const bdd_function_t& n, unsigned to );

/* Approximation */

bdd_function_t bdd_level_approximation( const bdd_function_t& bdd, unsigned mode, unsigned level,
                                        const properties::ptr& settings = properties::ptr(),
                                        const properties::ptr& statistics = properties::ptr() );

/* Error metrics */

boost::multiprecision::uint256_t error_rate( const bdd_function_t& f, const bdd_function_t& fhat,
                                             const properties::ptr& settings = properties::ptr(),
                                             const properties::ptr& statistics = properties::ptr() );

boost::multiprecision::uint256_t worst_case( const bdd_function_t& f, const bdd_function_t& fhat,
                                             const properties::ptr& settings = properties::ptr(),
                                             const properties::ptr& statistics = properties::ptr() );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
