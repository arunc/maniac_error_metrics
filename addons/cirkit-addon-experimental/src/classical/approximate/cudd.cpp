/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cudd.hpp"

#include <functional>
#include <iostream>
#include <sstream>

#include <boost/format.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>

#include <cuddInt.h>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/approximate/error_metrics.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

boost::multiprecision::uint256_t count_solutions_rec( DdManager* dd, const DdNode* f, cudd_count_map_t& visited )
{
  /* constant case? */
  if cuddIsConstant( Cudd_Regular( f ) )
  {
    return ( f == DD_ONE( dd ) ) ? 1u : 0u;
  }

  /* regular node */
  const auto* fr = Cudd_Regular( f );

  /* visited? */
  const auto it = visited.find( fr );
  if ( it != visited.end() )
  {
    return it->second;
  }

  /* recur */
  const auto *low  = Cudd_Regular( cuddE( fr ) );
  const auto *high = Cudd_Regular( cuddT( fr ) );

  const auto lidx = cuddIsConstant( low )  ? Cudd_ReadSize( dd ) : low->index;
  const auto hidx = cuddIsConstant( high ) ? Cudd_ReadSize( dd ) : high->index;

  // std::cout << boost::format( "lidx: %d, hidx: %d, fr->index: %d" ) % lidx % hidx % fr->index << std::endl;

  assert( fr->index < lidx && fr->index < hidx );

  const auto lv = count_solutions_rec( dd, cuddE( fr ), visited );
  const auto hv = count_solutions_rec( dd, cuddT( fr ), visited );

  auto val = ( lv << ( lidx - fr->index - 1u ) ) + ( hv << ( hidx - fr->index - 1u ) );

  if ( Cudd_IsComplement( f ) )
  {
    val = ( 1u << ( Cudd_ReadSize( dd ) - fr->index ) ) - val;
  }

  // std::cout << boost::format( "[i] value for %d@%d (%d@%d, %d@%d) = %d" ) % fr % fr->index % cuddE( fr ) % lidx % cuddT( fr ) % hidx % val << std::endl;

  return visited[fr] = val;
}

boost::multiprecision::uint256_t get_count( DdManager* dd, DdNode* node, const cudd_count_map_t& counts )
{
  if ( node == DD_ONE( dd ) ) { return 1u; }
  if ( node == Cudd_Not( DD_ONE( dd ) ) ) { return 0u; }
  return counts.at( Cudd_Regular( node ) );
}

DdNode* round_to( DdManager* dd, DdNode* f, DdNode* level, DD_CTFP op, DdNode* to, const cudd_count_map_t& counts )
{
  /* terminating cases */
  if cuddIsConstant( Cudd_Regular( f ) ) { return f; }

  /* check cache */
  DdNode* idx = cuddCacheLookup2( dd, op, f, level );
  if ( idx ) { return idx; }

  const auto* fr = Cudd_Regular( f );
  if ( (DdNode*)fr->index < level )
  {
    auto* rlow  = round_to( dd, cuddE( fr ), level, op, to, counts );
    auto* rhigh = round_to( dd, cuddT( fr ), level, op, to, counts );

    assert( rlow );
    assert( rhigh );

    Cudd_Ref( rlow );
    Cudd_Ref( rhigh );

    idx = cuddUniqueInter( dd, fr->index, rhigh, rlow );
    assert( idx );

    cuddDeref( rlow );
    cuddDeref( rhigh );
  }
  else
  {
    const auto cl = get_count( dd, cuddE( fr ), counts );
    const auto ch = get_count( dd, cuddT( fr ), counts );

    if ( cl < ch )
    {
      auto* rhigh = round_to( dd, cuddT( fr ), level, op, to, counts );

      assert( rhigh );
      Cudd_Ref( rhigh );

      idx = cuddUniqueInter( dd, fr->index, rhigh, to );
      assert( idx );

      cuddDeref( rhigh );
    }
    else
    {
      auto* rlow = round_to( dd, cuddE( fr ), level, op, to, counts );

      assert( rlow );
      Cudd_Ref( rlow );

      idx = cuddUniqueInter( dd, fr->index, to, rlow );
      assert( idx );

      cuddDeref( rlow );
    }
  }

  /* insert into cache */
  cuddCacheInsert2( dd, op, f, level, idx );

  return idx;
}

DdNode* round_rec( DdManager* dd, DdNode* f, DdNode* level, const cudd_count_map_t& counts )
{
  /* terminating case */
  if cuddIsConstant( Cudd_Regular( f ) ) { return f; }

  /* check cache */
  DdNode* idx = cuddCacheLookup2( dd, round, f, level );
  if ( idx ) { return idx; }

  const auto* fr = Cudd_Regular( f );
  if ( (DdNode*)fr->index < level )
  {
    auto* rlow  = round_rec( dd, cuddE( fr ), level, counts );
    auto* rhigh = round_rec( dd, cuddT( fr ), level, counts );

    assert( rlow );
    assert( rhigh );

    Cudd_Ref( rlow );
    Cudd_Ref( rhigh );

    idx = cuddUniqueInter( dd, fr->index, rhigh, rlow );
    assert( idx );

    cuddDeref( rlow );
    cuddDeref( rhigh );
  }
  else
  {
    const auto onset = counts.at( fr );
    const auto all   = boost::multiprecision::uint256_t( 1u ) << ( Cudd_ReadSize( dd ) - fr->index );

    idx = Cudd_NotCond( DD_ONE( dd ), ( onset << 1u ) <= all );
  }

  /* cache */
  cuddCacheInsert2( dd, round, f, level, idx );

  return idx;
}

BDD approximate( const Cudd& mgr, const BDD& f, unsigned mode, unsigned level )
{
  switch ( mode )
  {
  case 0u /* bdd_level_approximation_mode::round_down */:
    return BDD( mgr, round_down( f.manager(), f.getNode(), (DdNode*)level ) );
  case 1u /* bdd_level_approximation_mode::round_up */:
    return BDD( mgr, round_up( f.manager(), f.getNode(), (DdNode*)level ) );
  case 2u /* bdd_level_approximation_mode::round */:
    return BDD( mgr, round( f.manager(), f.getNode(), (DdNode*)level ) );
  case 3u /* bdd_level_approximation_mode::cof0 */:
    return f.Cofactor( !mgr.bddVar( level ) );
  case 4u /* bdd_level_approximation_mode::cof1 */:
    return f.Cofactor( mgr.bddVar( level ) );
  case 5u:
    return f.SubsetHeavyBranch( mgr.ReadSize(), level );
  case 6u:
    return f.SupersetHeavyBranch( mgr.ReadSize(), level );
  case 7u:
    return f.SubsetShortPaths( mgr.ReadSize(), level );
  case 8u:
    return f.SupersetShortPaths( mgr.ReadSize(), level );
  }

  assert( false );
}

std::tuple<BDD, BDD> full_adder( const BDD& x, const BDD& y, const BDD& cin )
{
  const auto sum  = x ^ y ^ cin;
  const auto cout = ( x & y ) | ( x & cin ) | ( y & cin );

  return std::make_tuple( sum, cout );
}

bdd_function_t compute_diff( const bdd_function_t& f, const bdd_function_t& fhat )
{
  const auto to = f.second.size() + 1u;

  const auto zf    = zero_extend( f, to );
  const auto zfhat = zero_extend( fhat, to );
  const auto subtr = bdd_subtract( zf, zfhat );
  const auto diff  = bdd_abs( subtr );

  assert( diff.second.size() == to );
  assert( diff.second.back() == diff.first.bddZero() );

  return diff;
}

boost::multiprecision::uint256_t get_max_value( const bdd_function_t& f )
{
  assert( !f.second.empty() );

  boost::dynamic_bitset<> bs( f.second.size() );
  auto mask = f.first.bddOne();

  for ( int k = f.second.size() - 1; k >= 0; --k )
  {
    const auto r = mask & f.second[k];
    if ( r != f.first.bddZero() )
    {
      bs.set( k );
      mask = r;
    }
  }

  return to_multiprecision<boost::multiprecision::uint256_t>( bs );
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

cudd_count_map_t count_solutions( DdManager* dd, const std::vector<DdNode*>& fs )
{
  cudd_count_map_t map;

  for ( const auto& f : fs )
  {
    count_solutions_rec( dd, f, map );
  }

  return map;
}

DdNode* round_down( DdManager* dd, DdNode* f, DdNode* level )
{
  assert( level < (DdNode*)Cudd_ReadSize( dd ) );

  return round_to( dd, f, level, round_down, Cudd_Not( DD_ONE( dd ) ), count_solutions( dd, {f} ) );
}

DdNode* round_up( DdManager* dd, DdNode* f, DdNode* level )
{
  assert( level < (DdNode*)Cudd_ReadSize( dd ) );

  return round_to( dd, f, level, round_up, DD_ONE( dd ), count_solutions( dd, {f} ) );
}

DdNode* round( DdManager* dd, DdNode* f, DdNode* level )
{
  assert( level < (DdNode*)Cudd_ReadSize( dd ) );

  return round_rec( dd, f, level, count_solutions( dd, {f} ) );
}

bdd_function_t bdd_subtract( const bdd_function_t& minuend, const bdd_function_t& subtrahend )
{
  assert( minuend.second.size() == subtrahend.second.size() );
  assert( !minuend.second.empty() );

  std::vector<BDD> diff( minuend.second.size() );
  auto carry = minuend.first.bddOne();

  for ( const auto& i : boost::counting_range( 0ul, minuend.second.size() ) )
  {
    std::tie( diff[i], carry ) = full_adder( minuend.second[i], !subtrahend.second[i], carry );
  }

  return {minuend.first, diff};
}

bdd_function_t bdd_abs( const bdd_function_t& n )
{
  std::vector<BDD> mask( n.second.size(), n.second.back() );
  std::vector<BDD> result;

  for ( const auto& i : boost::counting_range( 0ul, n.second.size() ) )
  {
    result.push_back( n.second[i] ^ mask[i] );
  }

  const auto diff = bdd_subtract( {n.first, result}, {n.first, mask} );
  std::copy( diff.second.begin(), diff.second.begin() + n.second.size(), result.begin() );

  return {n.first, result};
}

bdd_function_t zero_extend( const bdd_function_t& n, unsigned to )
{
  assert( to >= n.second.size() );
  auto ze = n.second;
  ze.resize( to, n.first.bddZero() );
  return {n.first, ze};
}

bdd_function_t sign_extend( const bdd_function_t& n, unsigned to )
{
  assert( to >= n.second.size() );
  auto se = n.second;
  se.resize( to, n.second.back() );
  return {n.first, se};
}

bdd_function_t bdd_level_approximation( const bdd_function_t& bdd, unsigned mode, unsigned level,
                                        const properties::ptr& settings,
                                        const properties::ptr& statistics )
{
  using boost::adaptors::transformed;
  using namespace std::placeholders;

  /* timing */
  properties_timer t( statistics );

  std::vector<BDD> fshat;
  boost::push_back( fshat, bdd.second | transformed( std::bind( &approximate, std::ref( bdd.first ), _1, mode, level ) ) );

  return {bdd.first, fshat};
}

boost::multiprecision::uint256_t error_rate( const bdd_function_t& f, const bdd_function_t& fhat,
                                             const properties::ptr& settings,
                                             const properties::ptr& statistics )

{
  properties_timer t( statistics );

  auto h = f.first.bddZero();

  for ( auto i = 0u; i < f.second.size(); ++i )
  {
    h |= f.second[i] ^ fhat.second[i];
  }

  std::stringstream s;
  s.precision(0);
  s << std::fixed << h.CountMinterm( f.first.ReadSize() );
  return boost::multiprecision::uint256_t( s.str() );

  //const auto counts = count_solutions( h.manager(), {h.getNode()} );
  //return get_count( h.manager(), h.getNode(), counts ) << h.getNode()->index;
}

boost::multiprecision::uint256_t worst_case( const bdd_function_t& f, const bdd_function_t& fhat,
                                             const properties::ptr& settings,
                                             const properties::ptr& statistics )
{
  const auto maximum_method = get( settings, "maximum_method", worst_case_maximum_method::shift );

  properties_timer t( statistics );

  const auto diff = compute_diff( f, fhat );

  switch ( maximum_method )
  {
  case worst_case_maximum_method::shift:
    return get_max_value( diff );
  case worst_case_maximum_method::chi:
    //return get_max_value_with_chi( diff );
  default:
    assert( false );
  }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
