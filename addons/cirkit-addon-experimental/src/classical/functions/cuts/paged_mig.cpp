/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paged_mig.hpp"

#include <map>
#include <mutex>
#include <stack>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/compute_levels.hpp>
#include <classical/functions/simulate_mig.hpp>
#include <classical/utils/mig_utils.hpp>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

paged_mig_cuts::paged_mig_cuts( const mig_graph& mig, unsigned k, unsigned priority )
  : _mig( mig ),
    _k( k ),
    _priority( priority ),
    data( num_vertices( _mig ) )
{
  unsigned max_level;
  _levels = compute_levels( mig, max_level );

  enumerate();
}

paged_mig_cuts::paged_mig_cuts( const mig_graph& mig, unsigned k, const std::vector<mig_node>& start, const std::vector<mig_node>& boundary, unsigned priority )
  : _mig( mig ),
    _k( k ),
    _priority( priority ),
    data( num_vertices( _mig ) )
{
  unsigned max_level;
  _levels = compute_levels( mig, max_level );

  color_map colors;
  auto _color = boost::make_assoc_property_map( colors );
  for ( const auto& v : boost::make_iterator_range( vertices( _mig ) ) )
  {
    put( _color, v, color_type::white() );
  }

  for ( auto v : start )
  {
    depth_first_visit( _mig, v,
                       boost::make_dfs_visitor( boost::null_visitor() ), _color,
                       [&boundary]( const mig_node& n, const mig_graph& mig )
                       {
                         return boost::find( boundary, n ) != boundary.end();
                       } );
  }

  enumerate_partial( boundary, colors );
}

unsigned paged_mig_cuts::total_cut_count() const
{
  return data.sets_count();
}

double paged_mig_cuts::enumeration_time() const
{
  return _enumeration_time;
}

unsigned paged_mig_cuts::memory() const
{
  return data.memory();
}

unsigned paged_mig_cuts::count( mig_node node ) const
{
  return data.count( node );
}

boost::iterator_range<paged_memory::iterator> paged_mig_cuts::cuts( mig_node node ) const
{
  return data.sets( node );
}

tt paged_mig_cuts::simulate( mig_node node, const paged_mig_cuts::cut& c ) const
{
  std::map<mig_node, tt> inputs;
  auto i = 0u;
  for ( const auto& child : c )
  {
    inputs[child] = tt_nth_var( i++ );
  }

  mig_tt_simulator tt_sim;
  mig_partial_node_assignment_simulator<tt> sim( tt_sim, inputs, tt_const0() );

  return simulate_mig_node( _mig, node, sim );
}

unsigned paged_mig_cuts::depth( mig_node node, const paged_mig_cuts::cut& c ) const
{
  std::map<mig_node, unsigned> inputs;
  for ( const auto& child : c )
  {
    inputs[child] = 0u;
  }

  mig_depth_simulator depth_sim;
  mig_partial_node_assignment_simulator<unsigned> sim( depth_sim, inputs, 0u );

  return simulate_mig_node( _mig, node, sim );
}

unsigned paged_mig_cuts::size( mig_node node, const paged_mig_cuts::cut& c ) const
{
  auto size = 0u;
  std::vector<mig_node> visited;
  std::vector<mig_node> leafs( c.begin(), c.end() );
  std::stack<mig_node> stack;
  stack.push( node );

  while ( !stack.empty() )
  {
    auto n = stack.top();
    stack.pop();

    /* continue, if already visited */
    if ( boost::find( visited, n ) != visited.end() ) continue;

    ++size;
    visited += n;

    /* continue, if leaf */
    if ( out_degree( n, _mig ) == 0u || boost::find( leafs, n ) != leafs.end() ) continue;

    const auto children = get_children( _mig, n );
    stack.push( children[0u].node );
    stack.push( children[1u].node );
    stack.push( children[2u].node );
  }

  return size;
}

std::vector<mig_node> paged_mig_cuts::cone( mig_node node, const paged_mig_cuts::cut& c, bool include_root, bool include_leafs ) const
{
  std::vector<mig_node> cone;
  std::vector<mig_node> visited;
  std::vector<mig_node> leafs( c.begin(), c.end() );
  std::stack<mig_node> stack;
  stack.push( node );

  while ( !stack.empty() )
  {
    auto n = stack.top();
    stack.pop();

    /* continue, if already visited */
    if ( boost::find( visited, n ) != visited.end() ) continue;

    visited += n;
    if ( n == node )
    {
      if ( include_root )
      {
        cone += n;
      }
    }
    else if ( boost::find( leafs, n ) != leafs.end() )
    {
      if ( include_leafs )
      {
        cone += n;
      }
      continue;
    }
    else
    {
      cone += n;
    }

    if ( out_degree( n, _mig ) == 0u ) continue;

    const auto children = get_children( _mig, n );
    stack.push( children[0u].node );
    stack.push( children[1u].node );
    stack.push( children[2u].node );
  }

  return cone;
}

void paged_mig_cuts::enumerate()
{
  reference_timer t( &_enumeration_time );

  /* topsort */
  std::vector<unsigned> topsort( num_vertices( _mig ) );
  boost::topological_sort( _mig, topsort.begin() );

  /* loop */
  _top_index = 0u;
  for ( auto n : topsort )
  {
    if ( out_degree( n, _mig ) == 0u )
    {
      /* constant */
      if ( n == 0u )
      {
        data.assign_empty( 0u );
      }
      /* PI */
      else
      {
        data.assign_singleton( n, n );
      }
    }
    else
    {
      data.append_begin( n );

      /* get children */
      auto it = adjacent_vertices( n, _mig ).first;
      const auto n1 = *it++;
      const auto n2 = *it++;
      const auto n3 = *it++;

      enumerate_node_with_bitsets( n, n1, n2, n3 );

      data.append_singleton( n, n );
    }

    _top_index++;
  }
}

void paged_mig_cuts::enumerate_partial( const std::vector<aig_node>& boundary, const paged_mig_cuts::color_map& colors )
{
  reference_timer t( &_enumeration_time );

  /* topsort */
  std::vector<unsigned> topsort( num_vertices( _mig ) );
  boost::topological_sort( _mig, topsort.begin() );

  /* loop */
  _top_index = 0u;
  for ( auto n : topsort )
  {
    //\if ( colors.at( n ) == color_type::white() ) continue;

    if ( out_degree( n, _mig ) == 0u || boost::find( boundary, n ) != boundary.end() )
    {
      if ( n == 0u )
      {
        data.assign_empty( 0u );
      }
      else
      {
        data.assign_singleton( n, n );
      }
    }
    else
    {
      data.append_begin( n );

      /* get children */
      auto it = adjacent_vertices( n, _mig ).first;
      const auto n1 = *it++;
      const auto n2 = *it++;
      const auto n3 = *it++;

      enumerate_node_with_bitsets( n, n1, n2, n3 );

      data.append_singleton( n, n );
    }

    _top_index++;
  }
}

std::vector<std::pair<boost::dynamic_bitset<>, unsigned>> paged_mig_cuts::enumerate_local_cuts( mig_node n1, mig_node n2, mig_node n3, unsigned max_cut_size ) const
{
  std::vector<std::pair<boost::dynamic_bitset<>, unsigned>> local_cuts;

  for ( const auto& c1 : cuts( n1 ) )
  {
    for ( const auto& c2 : cuts( n2 ) )
    {
      for ( const auto& c3 : cuts( n3 ) )
      {
        auto min_level = std::numeric_limits<unsigned>::max();
        boost::dynamic_bitset<> new_cut( max_cut_size );
        auto f = [&new_cut, &min_level, this]( unsigned pos ) {
          new_cut.set( pos );
          min_level = std::min( min_level, this->_levels.at( pos ) );
        };
        std::for_each( c1.begin(), c1.end(), f );
        std::for_each( c2.begin(), c2.end(), f );
        std::for_each( c3.begin(), c3.end(), f );

        const auto size = new_cut.count();

        if ( size <= _k )
        {
          auto first_subsume = true;
          auto add = true;

          auto l = 0u;
          while ( l < local_cuts.size() )
          {
            auto cut = local_cuts[l].first;

            /* same cut */
            if ( cut == new_cut ) { add = false; break; }

            /* cut subsumes new_cut */
            if ( ( cut & new_cut ) == new_cut ) { add = false; break; }

            /* new_cut subsumes cut */
            if ( ( cut & new_cut ) == cut )
            {
              add = false;
              if ( first_subsume )
              {
                local_cuts[l] = {new_cut, min_level};
                first_subsume = false;
              }
              else
              {
                local_cuts[l] = local_cuts.back();
                local_cuts.pop_back();
              }
            }

            ++l;
          }

          if ( add )
          {
            local_cuts += std::make_pair( new_cut, min_level );
          }
        }
      }
    }
  }

  boost::sort( local_cuts, []( const std::pair<boost::dynamic_bitset<>, unsigned>& e1,
                               const std::pair<boost::dynamic_bitset<>, unsigned>& e2 ) {
                 return ( e1.second > e2.second ) || ( e1.second == e2.second && e1.first.count() < e2.first.count() ); } );

  if ( local_cuts.size() > _priority )
  {
    local_cuts.resize( _priority );
  }

  return local_cuts;
}

void paged_mig_cuts::enumerate_node_with_bitsets( mig_node n, mig_node n1, mig_node n2, mig_node n3 )
{
  for ( const auto& cut : enumerate_local_cuts( n1, n2, n3, _top_index ) )
  {
    data.append_set( n, get_index_vector( cut.first ) );
  }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
