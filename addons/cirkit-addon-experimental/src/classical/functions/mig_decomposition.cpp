/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig_decomposition.hpp"

#include <boost/format.hpp>

#include <core/utils/timer.hpp>
#include <classical/functions/mig_from_string.hpp>
#include <classical/functions/npn_canonization.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/functions/simulate_mig.hpp>
#include <classical/optimization/mig_functional_hashing_constants.hpp>
#include <classical/utils/aig_utils.hpp>
#include <classical/utils/mig_utils.hpp>

#define L(x) if ( verbose ) { std::cout << x << std::endl; }

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

tt make_truth_table( const BDD& f, const std::vector<unsigned>& indexes, unsigned num_vars )
{
  DdGen *gen;
  int * cube = new int[num_vars];
  CUDD_VALUE_TYPE value;

  auto result = tt_const0();

  Cudd_ForeachCube( f.manager(), f.getNode(), gen, cube, value )
  {
    auto cube_tt = tt_const1();

    for ( auto i = 0u; i < indexes.size(); ++i )
    {
      switch ( cube[indexes[i]] )
      {
      case 0u:
        cube_tt &= tt_nth_var( i );
        break;
      case 1u:
        cube_tt &= ~tt_nth_var( i );
        break;
      }
    }

    result |= cube_tt;
  }

  tt_shrink( result, 4u );

  return result;
}

mig_function get_exact_mig( const BDD& f, const Cudd& mgr, mig_graph& mig, const boost::dynamic_bitset<>& mask, bool verbose )
{
  const auto indexes = get_index_vector( mask );
  const auto f_tt = make_truth_table( f, indexes, mask.size() );

  std::vector<unsigned> perm;
  boost::dynamic_bitset<> phase;
  const auto f_npn = exact_npn_canonization( f_tt, phase, perm );

  std::vector<unsigned> invperm( 4u );
  for ( auto i = 0u; i < 4u; ++i ) { invperm[perm[i]] = i; }

  const auto exact = mig_functional_hashing_constants::min_mig_sizes.at( f_npn.to_ulong() );

  L( boost::format( "[i] bdd for %d variables, mask: " ) % mask.count() << mask );
  L( "[i] tt: " << f_tt << ", npn: " << f_npn );
  L( boost::format( "[i] exact MIG: %s" ) % std::get<3>( exact ) );
  //f.PrintMinterm();

  std::map<char, mig_function> variable_map;
  const auto& info = mig_info( mig );
  for ( auto i = 0u; i < 4u; ++i )
  {
    variable_map['a' + invperm[i]] = {info.inputs[indexes[i]], !phase[i]};
    L( "[i] map BDD var " << indexes[i] << " to " << ( "abcd" )[invperm[i]] );
  }

  //std::cout << std::get<3>( exact ) << std::endl;

  assert( phase.size() == 5u );

  const auto settings = std::make_shared<properties>();
  settings->set( "variable_map", variable_map );
  const auto exf = mig_from_string( mig, std::get<3>( exact ), settings ) ^ phase[4u];

  mig_bdd_simulator sim( mgr );
  const auto new_f = simulate_mig_function( mig, exf, sim );
  assert( new_f == f );

  return exf;
}

mig_function mig_decomposition_rec( const BDD& f, const Cudd& mgr, mig_graph& mig, const boost::dynamic_bitset<>& mask, bool verbose )
{
  if ( mask.count() <= 4u )
  {
    return get_exact_mig( f, mgr, mig, mask, verbose );
  }
  else
  {
    const auto v1 = mask.find_first();
    const auto v2 = mask.find_next( v1 );
    const auto v3 = mask.find_next( v2 );

    const auto f1 = f.Compose( mgr.bddVar( v1 ), v2 ); /* e.g. f(x_1, x_1, x_3, ...) */
    const auto f2 = f.Compose( mgr.bddVar( v2 ), v3 ); /* e.g. f(x_1, x_2, x_2, ...) */
    const auto f3 = f.Compose( mgr.bddVar( v3 ), v1 ); /* e.g. f(x_3, x_2, x_3, ...) */

    const auto n = mask.size();

    L( boost::format( "[i] decomp (%d, %d, %d) in " ) % v1 % v2 % v3 << mask );

    return mig_create_maj(
        mig,
        mig_decomposition_rec( f1, mgr, mig, mask & ~onehot_bitset( n, v2 ), verbose ),
        mig_decomposition_rec( f2, mgr, mig, mask & ~onehot_bitset( n, v3 ), verbose ),
        mig_decomposition_rec( f3, mgr, mig, mask & ~onehot_bitset( n, v1 ), verbose ) );
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_graph mig_decomposition( const aig_graph& aig, const properties::ptr& settings, const properties::ptr& statistics )
{
  /* settings */
  const auto verbose = get( settings, "verbose", false );

  /* timing */
  properties_timer t( statistics );

  /* prepare MIG */
  const auto& info = aig_info( aig );
  mig_graph mig;
  mig_initialize( mig, info.model_name );

  for ( const auto& input : info.inputs )
  {
    mig_create_pi( mig, info.node_names.at( input ) );
  }

  /* simulate BDDs */
  Cudd mgr;
  bdd_simulator sim( mgr );
  const auto result = simulate_aig( aig, sim );

  for ( const auto& output : info.outputs )
  {
    mig_create_po( mig, mig_decomposition_rec( result.at( output.first ), mgr, mig, ~boost::dynamic_bitset<>( info.inputs.size() ), verbose ), output.second );
  }

  return mig;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
