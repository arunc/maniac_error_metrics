/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_miter.hpp"

#include <classical/utils/aig_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/zip.hpp>

#include <boost/graph/topological_sort.hpp>
#include <boost/algorithm/string.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using mapping_t = std::map< std::pair< unsigned, aig_node >, aig_node >;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

std::string get_name( const aig_graph& aig, const aig_node& node )
{
  const auto& info = aig_info( aig );
  const auto it = info.node_names.find( node );
  if ( it != info.node_names.end() )
  {
    return it->second;
  }
  else
  {
    return ( boost::format( "n_%s" ) % node ).str();
  }
}

void annotate_node( aig_graph& aig, const aig_node& n, const std::vector< unsigned >& ids )
{
  using iterator_t = std::istream_iterator<std::string>;
  auto annotation = get( boost::vertex_annotation, aig );

  std::set< std::string > string_annotations;
  if ( annotation[ n ][ "miter_id" ] != "" )
  {
    std::istringstream iss( annotation[ n ][ "miter_id" ] );
    std::copy(iterator_t(iss), iterator_t(),
              std::inserter(string_annotations, string_annotations.begin()));
  }

  for ( const auto& id : ids )
  {
    string_annotations.insert( ( boost::format("%s") % id ).str() );
  }

  std::string label;
  for ( const auto& l : string_annotations )
  {
    label += l + " ";
  }
  boost::algorithm::trim_right( label );

  annotation[ n ][ "miter_id" ] = label;
}

void aig_add( aig_graph& result_aig, const aig_graph& aig, const unsigned id, mapping_t& mapping )
{
  auto info = aig_info( aig );
  const auto& complementmap = boost::get( boost::edge_complement, aig );

  /* constant */
  if ( aig_is_constant_used( aig ) )
  {
    const aig_function f = aig_get_constant( result_aig, false );
    annotate_node( result_aig, f.node, { id } );
    mapping.insert( { { id, info.constant }, f.node } );
  }
  else
  {
    /*** beg(debugging) ***/
    const aig_function f = aig_get_constant( result_aig, false );
    annotate_node( result_aig, f.node, { id } );
    mapping.insert( { { id, info.constant }, f.node } );
    /*** end(debugging) ***/
  }

  /* iterate throught nodes in topological order */
  std::vector< unsigned > topsort( boost::num_vertices( aig ) );
  boost::topological_sort( aig, topsort.begin() );
  for ( const auto& node : topsort )
  {
    const auto _out_degree = out_degree( node, aig );

    /* ignore inputs */
    if ( _out_degree == 0u ) { continue; }

    /* latches are not supported */
    if ( _out_degree == 1u ) { assert( false && "latches are not supported" ); }

    /* and gates */
    if ( _out_degree == 2u )
    {
      const auto& edges = boost::make_iterator_range( out_edges( node, aig ) );

      const auto& r_index = target( edges[ 0u ], aig );
      const auto r_compl = complementmap[ edges[ 0u ] ];

      const auto& l_index = target( edges[ 1u ], aig );
      const auto l_compl = complementmap[ edges[ 1u ] ];

      // std::cout << "and " << aig_to_literal( aig, { r_index, r_compl } ) << ' ' << aig_to_literal( aig, { l_index, l_compl } ) << '\n';

      assert( mapping.find( { id, r_index } ) != mapping.end() && "r_index in mapping" );
      assert( mapping.find( { id, l_index } ) != mapping.end() && "l_index in mapping" );

      const auto rhs = mapping.at( {id, r_index} );
      const auto lhs = mapping.at( {id, l_index} );
      const aig_function f = aig_create_and( result_aig, { rhs, r_compl }, { lhs, l_compl } );
      annotate_node( result_aig, f.node, { id } );
      mapping.insert( { { id, node }, f.node } );
    }
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
aig_graph aig_miter( const aig_graph& aig0, const aig_graph& aig1 )
{
  const auto& info0 = aig_info( aig0 );
  const auto& info1 = aig_info( aig1 );

  assert( info0.inputs.size()  == info1.inputs.size()  && "Equal number of PIs" );
  assert( info0.outputs.size() == info1.outputs.size() && "Equal number of POs" );

  const auto aig0_name = info0.model_name == "" ? "unnamed" : info0.model_name;
  const auto aig1_name = info1.model_name == "" ? "unnamed" : info1.model_name;
  const auto model_name = ( boost::format( "miter_of_%s_%s" ) % info0.model_name % info1.model_name ).str();

  aig_graph result_aig;
  aig_initialize( result_aig, model_name );
  auto& result_info = aig_info( result_aig );
  result_info.enable_strashing = result_info.enable_local_optimization = false;

  /* mapping from old graph node ids to new graph node ids*/
  mapping_t mapping;

  /* inputs for all input pairs */
  for ( const auto& i : zip( info0.inputs, info1.inputs ) )
  {
    const auto name0 = get_name( aig0, boost::get<0u>(i) );
    const auto name1 = get_name( aig1, boost::get<01>(i) );
    const auto name = ( name0 == name1 ? name0 : ( boost::format( "in_%s_%s" ) % name0 % name1 ).str() );
    const aig_function f = aig_create_pi( result_aig, name );
    annotate_node( result_aig, f.node, { 0u, 1u } );
    mapping.insert( { { 0u, boost::get<0u>( i ) } , f.node } );
    mapping.insert( { { 1u, boost::get<1u>( i ) } , f.node } );
  }

  aig_add( result_aig, aig0, 0u, mapping );
  aig_add( result_aig, aig1, 1u, mapping );

  /* different outputs */
  std::vector< aig_function > outs;
  for ( auto o : zip( info0.outputs, info1.outputs ) )
  {
    const auto left = mapping.at( { 0u, boost::get<0u>( o ).first.node } );
    const auto right = mapping.at( { 1u, boost::get<1u>( o ).first.node } );
    outs += aig_create_xor( result_aig, { left, boost::get<0u>( o ).first.complemented }, { right, boost::get<1u>( o ).first.complemented } );
  }

  const aig_function o = aig_create_nary_or( result_aig, outs );
  aig_create_po( result_aig, o, "miter" );

  result_info.enable_strashing = result_info.enable_local_optimization = true;

  return result_aig;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
