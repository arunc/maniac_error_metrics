/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig_mighty.hpp"

#ifdef ADDON_EPFL

#include <unordered_map>

#include <boost/graph/topological_sort.hpp>

#include <classical/utils/mig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

MIG* mig_to_mighty( const mig_graph& mig )
{
  auto * _mig = static_cast<MIG*>( malloc( sizeof( MIG ) ) );

  const auto& info  = mig_info( mig );
  std::vector<MAJ3*> node_to_node( num_vertices( mig ) );

  _mig->Nin    = info.inputs.size();
  _mig->Nout   = info.outputs.size();
  _mig->Nnodes = num_vertices( mig ) - _mig->Nin - 1;

  _mig->in       = static_cast<MAJ3**>( malloc( sizeof( MAJ3* ) * _mig->Nin ) );
  _mig->innames  = static_cast<char**>( malloc( sizeof( char* ) * _mig->Nin ) );
  _mig->out      = static_cast<MAJ3**>( malloc( sizeof( MAJ3* ) * _mig->Nout ) );
  _mig->outnames = static_cast<char**>( malloc( sizeof( char* ) * _mig->Nout ) );
  _mig->nodes    = static_cast<MAJ3**>( malloc( sizeof( MAJ3* ) * _mig->Nnodes ) );

  _mig->outcompl = static_cast<unsigned*>( malloc( sizeof( unsigned ) * _mig->Nout ) );

  _mig->one = static_cast<MAJ3*>( malloc( sizeof( MAJ3 ) ) );
  _mig->one->in1 = _mig->one->in2 = _mig->one->in2 = nullptr;
  _mig->one->compl1 = _mig->one->compl2 = _mig->one->compl3 = 0;
  _mig->one->outEdges = nullptr;
  _mig->one->value    = 1;
  _mig->one->fanout   = 0;
  _mig->one->label    = 0;
  _mig->one->aux      = nullptr;
  _mig->one->flag     = 0;
  _mig->one->PI       = 1;
  _mig->one->PO       = 0;

  node_to_node[0u] = _mig->one;

  for ( auto i = 0; i < _mig->Nin; ++i )
  {
    _mig->innames[i] = static_cast<char*>( malloc( sizeof( char ) * MAXCAR ) );
    strcpy( _mig->innames[i], info.node_names.at( info.inputs[i] ).c_str() );

    _mig->in[i] = static_cast<MAJ3*>( malloc( sizeof( MAJ3 ) ) );
    _mig->in[i]->in1 = _mig->in[i]->in2 = _mig->in[i]->in2 = nullptr;
    _mig->in[i]->compl1 = _mig->in[i]->compl2 = _mig->in[i]->compl3 = 0;
    _mig->in[i]->outEdges = nullptr;
    _mig->in[i]->value    = 2;
    _mig->in[i]->fanout   = 0;
    _mig->in[i]->label    = i;
    _mig->in[i]->aux      = nullptr;
    _mig->in[i]->flag     = 0;
    _mig->in[i]->PI       = 1;
    _mig->in[i]->PO       = 0;

    node_to_node[info.inputs[i]] = _mig->in[i];
  }

  std::vector<mig_node> topsort( num_vertices( mig ) );
  boost::topological_sort( mig, topsort.begin() );

  auto idx = 0u;
  for ( const auto& node : topsort )
  {
    if ( out_degree( node, mig ) == 0u ) continue;

    const auto children = get_children( mig, node );

    _mig->nodes[idx] = static_cast<MAJ3*>( malloc( sizeof( MAJ3 ) ) );
    _mig->nodes[idx]->in1      = node_to_node[children[0u].node];
    _mig->nodes[idx]->in2      = node_to_node[children[1u].node];
    _mig->nodes[idx]->in3      = node_to_node[children[2u].node];
    _mig->nodes[idx]->compl1   = children[0u].complemented != ( children[0u].node == 0u );
    _mig->nodes[idx]->compl2   = children[1u].complemented != ( children[1u].node == 0u );
    _mig->nodes[idx]->compl3   = children[2u].complemented != ( children[2u].node == 0u );
    _mig->nodes[idx]->outEdges = nullptr;
    _mig->nodes[idx]->value    = 2;
    _mig->nodes[idx]->fanout   = 0;
    _mig->nodes[idx]->label    = idx;
    _mig->nodes[idx]->aux      = nullptr;
    _mig->nodes[idx]->flag     = 0;
    _mig->nodes[idx]->PI       = 0;
    _mig->nodes[idx]->PO       = 0;

    addfanout( _mig->nodes[idx]->in1, _mig->nodes[idx] );
    addfanout( _mig->nodes[idx]->in2, _mig->nodes[idx] );
    addfanout( _mig->nodes[idx]->in3, _mig->nodes[idx] );

    node_to_node[node] = _mig->nodes[idx];

    ++idx;
  }

  for ( auto i = 0u; i < info.outputs.size(); ++i )
  {
    _mig->outnames[i] = static_cast<char*>( malloc( sizeof( char ) * MAXCAR ) );
    strcpy( _mig->outnames[i], info.outputs[i].second.c_str() );
    _mig->outcompl[i] = info.outputs[i].first.complemented;
    _mig->out[i] = node_to_node[info.outputs[i].first.node];
    _mig->out[i]->PO = 1;
  }

  return _mig;
}

mig_function mig_from_mighty_rec( const MIG* _mig, const MAJ3* maj, mig_graph& mig, std::unordered_map<const MAJ3*, mig_function> node_to_function )
{
  const auto it = node_to_function.find( maj );
  if ( it != node_to_function.end() )
  {
    return it->second;
  }

  const auto c1 = mig_from_mighty_rec( _mig, maj->in1, mig, node_to_function ) ^ (bool)maj->compl1;
  const auto c2 = mig_from_mighty_rec( _mig, maj->in2, mig, node_to_function ) ^ (bool)maj->compl2;
  const auto c3 = mig_from_mighty_rec( _mig, maj->in3, mig, node_to_function ) ^ (bool)maj->compl3;

  return node_to_function[maj] = mig_create_maj( mig, c1, c2, c3 );
}

mig_graph mig_from_mighty( const MIG* _mig )
{
  std::unordered_map<const MAJ3*, mig_function> node_to_function;

  mig_graph mig;
  mig_initialize( mig, "from_mighty" );

  node_to_function[_mig->one] = mig_get_constant( mig, true );
  for ( auto i = 0; i < _mig->Nin; ++i )
  {
    node_to_function[_mig->in[i]] = mig_create_pi( mig, std::string( _mig->innames[i] ) );
  }

  for ( auto i = 0; i < _mig->Nout; ++i )
  {
    const auto f = mig_from_mighty_rec( _mig, _mig->out[i], mig, node_to_function ) ^ (bool)_mig->outcompl[i];
    mig_create_po( mig, f, std::string( _mig->outnames[i] ) );
  }

  return mig;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
