/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_cone_utils.hpp"
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

class aig_input_cone_visitor
{
  using ingoing_edges_t = std::map<vertex_t<aig_graph>, std::vector<edge_t<aig_graph>>>;
  using in_degrees_t = std::vector<unsigned>;

public:
  aig_input_cone_visitor( const aig_graph& aig, const aig_node& node )
    : aig( aig )
    , info( aig_info( aig ) )
    , node( node )
  {}

  void compute_recursive( const aig_node& current )
  {
    /* visit each node only once */
    if ( nodes.find( current ) != nodes.end() )
    {
      return;
    }
    nodes.insert( current );

    for ( const auto& child : get_children( aig, current ) )
    {
      compute_recursive( child.node );
    }
  }

  std::set< aig_node > get()
  {
    for ( const auto& c : get_children( aig, node ) )
    {
      compute_recursive( c.node );
    }
    return nodes;
  }

  const aig_graph& aig;
  const aig_graph_info& info;
  const aig_node& node;

  std::set< aig_node > nodes;
}; /* aig_input_cone_visitor */

class aig_output_cone_visitor
{
  using ingoing_edges_t = std::map<vertex_t<aig_graph>, std::vector<edge_t<aig_graph>>>;
  using in_degrees_t = std::vector<unsigned>;

public:
  aig_output_cone_visitor( const aig_graph& aig, const aig_node& node )
    : aig( aig )
    , info( aig_info( aig ) )
    , in_edges( precompute_ingoing_edges( aig ) )
    , in_degrees( precompute_in_degrees( aig ) )
    , node( node )
  {}

  void compute_recursive( const aig_node& current )
  {
    /* visit each node only once */
    if ( nodes.find( current ) != nodes.end() )
    {
      return;
    }
    nodes.insert( current );

    if ( in_degrees.at( current ) > 0u )
    {
      for ( const auto& edge : in_edges.at(current) )
      {
        compute_recursive( source( edge, aig ) );
      }
    }
  }

  std::set< aig_node > get()
  {
    compute_recursive( node );
    nodes.erase( nodes.find( node ) );
    return nodes;
  }

  const aig_graph& aig;
  const aig_graph_info& info;
  const aig_node& node;
  const ingoing_edges_t in_edges;
  const in_degrees_t in_degrees;

  std::set< aig_node > nodes;
}; /* aig_output_cone_visitor */

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

std::set< aig_node > input_cone_as_set( const aig_graph& aig, const aig_node& node )
{
  return aig_input_cone_visitor( aig, node ).get();
}

std::vector< aig_node > input_cone_as_vector( const aig_graph& aig, const aig_node& node )
{
  const std::set< aig_node > s = aig_input_cone_visitor( aig, node ).get();
  std::vector< aig_node > vs;
  std::copy( s.begin(), s.end(), std::back_inserter( vs ) );
  return vs;
}

std::set< aig_node > output_cone_as_set( const aig_graph& aig, const aig_node& node )
{
  return aig_output_cone_visitor( aig, node ).get();
}

std::vector< aig_node > output_cone_as_vector( const aig_graph& aig, const aig_node& node )
{
  const std::set< aig_node > s = aig_output_cone_visitor( aig, node ).get();
  std::vector< aig_node > vs;
  std::copy( s.begin(), s.end(), std::back_inserter( vs ) );
  return vs;
}

std::set< aig_node > complemented_output_cone_as_set( const aig_graph& aig, const aig_node& node )
{
  auto cone = output_cone_as_set( aig, node );
  auto it = cone.find( node );
  if ( it != cone.end() )
  {
    cone.erase( it );
  }

  std::set< aig_node > nodes;
  for ( auto p = vertices( aig ); p.first != p.second; ++p.first )
  {
    if ( cone.find( *p.first ) == cone.end() )
    {
      nodes.insert( *p.first );
    }
  }

  return nodes;
}

std::vector< aig_node > complemented_output_cone_as_vector( const aig_graph& aig, const aig_node& node )
{
  auto cone = output_cone_as_set( aig, node );
  auto it = cone.find( node );
  if ( it != cone.end() )
  {
    cone.erase( it );
  }

  std::vector< aig_node > nodes;
  for ( auto p = vertices( aig ); p.first != p.second; ++p.first )
  {
    if ( cone.find( *p.first ) == cone.end() )
    {
      nodes.push_back( *p.first );
    }
  }

  return nodes;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
