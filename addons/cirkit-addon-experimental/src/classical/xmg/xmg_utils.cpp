/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmg_utils.hpp"

#include <boost/format.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

int compute_depth_rec( const xmg_graph& xmg, xmg_node node, std::vector<int>& depths )
{
  if ( depths[node] >= 0 ) return depths[node];

  if ( xmg.is_input( node ) )
  {
    return depths[node] = 0;
  }
  else
  {
    int depth = 0;
    for ( const auto& c : xmg.children( node ) )
    {
      depth = std::max( compute_depth_rec( xmg, c.node, depths ), depth );
    }
    return depths[node] = ( depth + 1 );
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

unsigned compute_depth( const xmg_graph& xmg )
{
  std::vector<int> depths( xmg.size(), -1 );

  int depth = -1;

  for ( const auto& output : xmg.outputs() )
  {
    depth = std::max( compute_depth_rec( xmg, output.first.node, depths ), depth );
  }

  return static_cast<unsigned>( depth );
}

void xmg_print_stats( const xmg_graph& xmg, std::ostream& os )
{
  auto name = xmg.name();
  if ( name.empty() )
  {
    name = "(unnamed)";
  }

  os << boost::format( "[i] %20s: i/o = %7d / %7d  maj = %7d  xor = %7d  lev = %4d" ) % name % xmg.inputs().size() % xmg.outputs().size() % xmg.num_maj() % xmg.num_xor() % compute_depth( xmg ) << std::endl;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
