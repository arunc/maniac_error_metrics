/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file xmg.hpp
 *
 * @brief XOR Majority Graph
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef XMG_HPP
#define XMG_HPP

#include <functional>
#include <string>
#include <unordered_map>
#include <vector>

#include <boost/graph/adjacency_list.hpp>

#include <core/utils/graph_utils.hpp>
#include <core/utils/hash_utils.hpp>

namespace cirkit
{

using xmg_graph_t = digraph_t<boost::no_property, boost::property<boost::edge_complement_t, bool>>;
using xmg_node    = vertex_t<xmg_graph_t>;

class xmg_function
{
public:
  xmg_function( xmg_node node = 0, bool complemented = false );

  bool operator==( const xmg_function& other ) const;
  bool operator!=( const xmg_function& other ) const;
  bool operator<( const xmg_function& other ) const;
  bool operator>( const xmg_function& other ) const;

  xmg_function operator!() const;
  xmg_function operator^( bool value ) const;

public:
  xmg_node node;
  bool     complemented;
};

}

namespace std
{

template<>
struct hash<cirkit::xmg_function>
{
  inline std::size_t operator()( const cirkit::xmg_function& f ) const
  {
    return ( f.node << 1u ) + static_cast<int>( f.complemented );
  }
};

}

namespace cirkit
{

class xmg_graph
{
public:
  using graph_t = xmg_graph_t;
  using node_t  = vertex_t<graph_t>;

  using input_vec_t  = std::vector<std::pair<node_t, std::string>>;
  using output_vec_t = std::vector<std::pair<xmg_function, std::string>>;

public:
  xmg_graph( const std::string& name = std::string() );

  void compute_fanout();

  xmg_function get_constant( bool value ) const;
  xmg_function create_pi( const std::string& name );
  void create_po( const xmg_function& f, const std::string& name );
  xmg_function create_maj( const xmg_function& a, const xmg_function& b, const xmg_function& c );
  xmg_function create_xor( const xmg_function& a, const xmg_function& b );
  xmg_function create_or( const xmg_function& a, const xmg_function& b );

  unsigned fanin_count( node_t n ) const;
  unsigned fanout_count( node_t n ) const;

  bool is_input( node_t n ) const;
  bool is_maj( node_t n ) const;
  bool is_xor( node_t n ) const;

  const std::string& name() const;
  std::size_t size() const;
  unsigned num_gates() const;
  unsigned num_maj() const;
  unsigned num_xor() const;
  graph_t graph() const;
  const input_vec_t& inputs() const;
  const output_vec_t& outputs() const;
  std::vector<xmg_function> children( xmg_node n ) const;
  std::vector<node_t> topological_nodes() const;

private:
  graph_t g;
  node_t  constant;

  std::string  _name;
  input_vec_t  _inputs;
  output_vec_t _outputs;

  using maj_strash_key_t = std::tuple<xmg_function, xmg_function, xmg_function>;
  using xor_strash_key_t = std::pair<xmg_function, xmg_function>;
  std::unordered_map<maj_strash_key_t, node_t, hash<maj_strash_key_t>> maj_strash;
  std::unordered_map<xor_strash_key_t, node_t, hash<xor_strash_key_t>> xor_strash;

  boost::property_map<graph_t, boost::edge_complement_t>::type         complement;

  std::vector<unsigned>                                                fanout;
  unsigned                                                             _num_maj = 0u;
  unsigned                                                             _num_xor = 0u;
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
