/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmg.hpp"

#include <boost/range/iterator_range.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

/******************************************************************************
 * header                                                                     *
 ******************************************************************************/

xmg_graph::xmg_graph( const std::string& name )
  : constant( add_vertex( g ) ),
    _name( name )
{
  assert( constant == 0 );
}

void xmg_graph::compute_fanout()
{
  fanout = precompute_in_degrees( g );
}

xmg_function xmg_graph::get_constant( bool value ) const
{
  return xmg_function( constant, value );
}

xmg_function xmg_graph::create_pi( const std::string& name )
{
  const auto node = add_vertex( g );
  _inputs.push_back( {node, name} );
  return xmg_function( node );
}

void xmg_graph::create_po( const xmg_function& f, const std::string& name )
{
  _outputs.push_back( {f, name} );
}

xmg_function xmg_graph::create_maj( const xmg_function& a, const xmg_function& b, const xmg_function& c )
{
  /* Special cases */
  if ( a == b )  { return a; }
  if ( a == c )  { return a; }
  if ( b == c )  { return b; }
  if ( a == !b ) { return c; }
  if ( a == !c ) { return b; }
  if ( b == !c ) { return a; }

  /* structural hashing */
  xmg_function children[] = {a, b, c};
  std::sort( children, children + 3 );

  const auto key = std::make_tuple( children[0], children[1], children[2] );

  const auto it = maj_strash.find( key );
  if ( it != maj_strash.end() )
  {
    return it->second;
  }

  /* insert node */
  const auto node = add_vertex( g );
  ++_num_maj;

  const auto ea = add_edge( node, children[0].node, g ).first;
  const auto eb = add_edge( node, children[1].node, g ).first;
  const auto ec = add_edge( node, children[2].node, g ).first;

  complement[ea] = children[0].complemented;
  complement[eb] = children[1].complemented;
  complement[ec] = children[2].complemented;

  return maj_strash[key] = node;
}

xmg_function xmg_graph::create_xor( const xmg_function& a, const xmg_function& b )
{
  /* special cases */
  if ( a == b ) { return get_constant( false ); }
  if ( a == !b) { return get_constant( false ); }
  if ( a.node == constant ) { return xmg_function( b.node, b.complemented ^ a.complemented ); }
  if ( b.node == constant ) { return xmg_function( a.node, a.complemented ^ b.complemented ); }

  /* structural hashing */
  const auto key = a.node < b.node ? std::make_pair( a, b ) : std::make_pair( b, a );

  const auto it = xor_strash.find( key );
  if ( it != xor_strash.end() )
  {
    return it->second;
  }

  /* insert node */
  const auto node = add_vertex( g );
  ++_num_xor;

  const auto ea = add_edge( node, key.first.node, g ).first;
  const auto eb = add_edge( node, key.second.node, g ).first;

  complement[ea] = key.first.complemented;
  complement[eb] = key.second.complemented;

  return xor_strash[key] = node;
}

xmg_function xmg_graph::create_or( const xmg_function& a, const xmg_function& b )
{
  return create_maj( get_constant( true ), a, b );
}

unsigned xmg_graph::fanin_count( node_t n ) const
{
  return boost::out_degree( n, g );
}

unsigned xmg_graph::fanout_count( node_t n ) const
{
  return fanout[n];
}

bool xmg_graph::is_input( node_t n ) const
{
  return fanin_count( n ) == 0u;
}

bool xmg_graph::is_maj( node_t n ) const
{
  return fanin_count( n ) == 3u;
}

bool xmg_graph::is_xor( node_t n ) const
{
  return fanin_count( n ) == 2u;
}

const std::string& xmg_graph::name() const
{
  return _name;
}

std::size_t xmg_graph::size() const
{
  return num_vertices( g );
}

unsigned xmg_graph::num_gates() const
{
  return _num_maj + _num_xor;
}

unsigned xmg_graph::num_maj() const
{
  return _num_maj;
}

unsigned xmg_graph::num_xor() const
{
  return _num_xor;
}

xmg_graph::graph_t xmg_graph::graph() const
{
  return g;
}

const xmg_graph::input_vec_t& xmg_graph::inputs() const
{
  return _inputs;
}

const xmg_graph::output_vec_t& xmg_graph::outputs() const
{
  return _outputs;
}

std::vector<xmg_function> xmg_graph::children( xmg_node n ) const
{
  std::vector<xmg_function> c;
  for ( const auto& e : boost::make_iterator_range( boost::out_edges( n, g ) ) )
  {
    c.push_back( xmg_function( boost::target( e, g ), complement[e] ) );
  }
  return c;
}

std::vector<xmg_graph::node_t> xmg_graph::topological_nodes() const
{
  std::vector<node_t> top( num_vertices( g ) );
  boost::topological_sort( g, top.begin() );
  return top;
}

/******************************************************************************
 * xmg_fuction                                                            *
 ******************************************************************************/

xmg_function::xmg_function( xmg_node node, bool complemented )
  : node( node ),
    complemented( complemented )
{
}

bool xmg_function::operator==( const xmg_function& other ) const
{
  return node == other.node && complemented == other.complemented;
}

bool xmg_function::operator!=( const xmg_function& other ) const
{
  return !operator==( other );
}

bool xmg_function::operator<( const xmg_function& other ) const
{
  if ( node < other.node )
  {
    return true;
  }
  else if ( node == other.node )
  {
    return !complemented && other.complemented;
  }
  else
  {
    return false;
  }
}

bool xmg_function::operator>( const xmg_function& other ) const
{
  return ( other < *this );
}

xmg_function xmg_function::operator!() const
{
  return xmg_function( node, !complemented );
}

xmg_function xmg_function::operator^( bool value ) const
{
  return xmg_function( node, complemented != value );
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
