/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exact_mig.hpp"

#include <cmath>
#include <memory>
#include <type_traits>
#include <vector>

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/utils/mig_utils.hpp>
#include <classical/utils/spec_representation.hpp>
#include <formal/utils/z3_utils.hpp>

#include <z3++.h>

using namespace boost::assign;

using boost::format;
using boost::str;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

struct exact_mig_instance
{
  struct wire
  {
    wire( exact_mig_instance* inst, unsigned gate_id, unsigned input_id )
      : inst( inst ),
        sel( create_sel( inst, gate_id, input_id ) ),
        neg( create_neg( inst, gate_id, input_id ) )
    {
    }

    int sel_value( const z3::model& m ) const
    {
      int v;
      if ( inst->enc_bv )
      {
        v = to_bitset( m.eval( sel ) ).to_ulong();
      }
      else
      {
        Z3_get_numeral_int( inst->ctx, m.eval( sel ), &v );
      }
      return v;
    }

    bool neg_value( const z3::model& m ) const
    {
      return expr_to_bool( m.eval( neg ) );
    }

    exact_mig_instance* inst;
    z3::expr sel;
    z3::expr neg;

  private:
    z3::expr create_sel( exact_mig_instance* inst, unsigned gate_id, unsigned input_id ) const
    {
      const auto name = str( format( "sel%d_%d" ) % input_id % gate_id );
      if ( inst->enc_bv )
      {
        auto e = inst->ctx.bv_const( name.c_str(), inst->bw );

        /* assertion for sel[level] <= n + k */
        inst->solver.add( z3::ule( e, inst->ctx.bv_val( inst->num_vars + gate_id, inst->bw ) ) );

        return e;
      }
      else
      {
        auto e = inst->ctx.int_const( name.c_str() );

        /* assertion for sel[level] <= n + k */
        inst->solver.add( e >= 0 );
        inst->solver.add( e <= static_cast<int>( inst->num_vars + gate_id ) );

        return e;
      }
    }

    z3::expr create_neg( exact_mig_instance* inst, unsigned gate_id, unsigned input_id ) const
    {
      return inst->ctx.bool_const( str( format( "neg%d_%d" ) % input_id % gate_id ).c_str() );
    }
  };

  struct gate
  {
    gate( exact_mig_instance* inst, unsigned gate_id, bool with_xor )
      : inst( inst),
        inputs({wire( inst, gate_id, 0u ), wire( inst, gate_id, 1u ), wire( inst, gate_id, 2u )}),
        with_xor( with_xor )
    {
      if ( with_xor )
      {
        /* type = 0 : MAJ, type = 1 : XOR */
        type_expr = std::make_shared<z3::expr>( inst->ctx.bool_const( str( format( "type%d" ) % gate_id ).c_str() ) );
      }
    }

    void symmetry_breaking_commutativity() const
    {
      if ( inst->enc_bv )
      {
        inst->solver.add( z3::ult( inputs[0].sel, inputs[1].sel ) );

        if ( with_xor )
        {
          inst->solver.add( implies( !type(), z3::ult( inputs[1].sel, inputs[2].sel ) ) );
        }
        else
        {
          inst->solver.add( z3::ult( inputs[1].sel, inputs[2].sel ) );
        }
      }
      else
      {
        inst->solver.add( inputs[0].sel < inputs[1].sel );

        if ( with_xor )
        {
          inst->solver.add( implies( !type(), z3::ult( inputs[1].sel, inputs[2].sel ) ) );
        }
        else
        {
          inst->solver.add( inputs[1].sel < inputs[2].sel );
        }
      }
    }

    void symmetry_breaking_inverters() const
    {
      if ( with_xor )
      {
        inst->solver.add( implies( !type(), !inputs[0].neg || !inputs[1].neg || !inputs[2].neg ) );
        inst->solver.add( implies( type(), !inputs[1].neg ) );
      }
      else
      {
        inst->solver.add( !inputs[0].neg || !inputs[1].neg || !inputs[2].neg );
      }
    }

    void symmetry_breaking_different( const gate& other )
    {
      if ( with_xor )
      {
        inst->solver.add( implies( !type() && !other.type(),
                                   inputs[0].sel != other.inputs[0].sel ||
                                   inputs[1].sel != other.inputs[1].sel ||
                                   inputs[2].sel != other.inputs[2].sel ||
                                   inputs[0].neg != other.inputs[0].neg ||
                                   inputs[1].neg != other.inputs[1].neg ||
                                   inputs[2].neg != other.inputs[2].neg ) );
        inst->solver.add( implies( type() && other.type(),
                                   inputs[0].sel != other.inputs[0].sel ||
                                   inputs[1].sel != other.inputs[1].sel ||
                                   inputs[0].neg != other.inputs[0].neg ||
                                   inputs[1].neg != other.inputs[1].neg ) );
      }
      else
      {
        inst->solver.add( inputs[0].sel != other.inputs[0].sel ||
                          inputs[1].sel != other.inputs[1].sel ||
                          inputs[2].sel != other.inputs[2].sel ||
                          inputs[0].neg != other.inputs[0].neg ||
                          inputs[1].neg != other.inputs[1].neg ||
                          inputs[2].neg != other.inputs[2].neg );
      }
    }

    void symmetry_breaking_constant_input() const
    {
      inst->solver.add( !( type() && inst->equals( inputs[0].sel, 0u ) ) );
    }

    const wire& operator[]( unsigned index ) const
    {
      return inputs[index];
    }

    inline const z3::expr& type() const
    {
      return *type_expr;
    }

    bool type_value( const z3::model& m ) const
    {
      return expr_to_bool( m.eval( type() ) );
    }

  private:
    exact_mig_instance* inst;

  public:
    std::vector<wire> inputs;
    bool with_xor;
    std::shared_ptr<z3::expr> type_expr;
  };

  exact_mig_instance( unsigned num_vars, bool output_inverter, bool with_xor, bool enc_bv, bool expl ) :
    num_vars( num_vars ),
    output_inverter( output_inverter ),
    with_xor( with_xor ),
    enc_bv( enc_bv ),
    solver( make_solver( expl ) ),
    out_neg( ctx.bool_const( "out_neg" ) )
  {
    auto upper_bound = 7u;
    if ( num_vars > 4u )
    {
      upper_bound += 10 * ( ( 1u << ( num_vars - 4u ) ) - 1 );
    }
    bw = (unsigned)ceil( log( 2u + num_vars + upper_bound ) / log( 2u ) );
  }

  z3::solver make_solver( bool expl )
  {
    if ( expl )
    {
      return z3::solver( ctx, "QF_BV" );
      //return ( z3::tactic( ctx, "aig" ) & z3::tactic( ctx, "bit-blast" ) &  z3::tactic( ctx, "solve-eqs" ) & z3::tactic( ctx, "sat" ) ).mk_solver();
    }
    else
    {
      return ( z3::tactic( ctx, "qe" ) & z3::tactic( ctx, "smt" ) ).mk_solver();
    }
  }

  void add_level()
  {
    unsigned level = gates.size();

    gates += gate( this, level, with_xor );

    /* symmetry breaking */
    gates[level].symmetry_breaking_commutativity();

    if ( output_inverter )
    {
      gates[level].symmetry_breaking_inverters();
    }
    else if ( level > 0u )
    {
      gates[level - 1].symmetry_breaking_inverters();
    }

    for ( auto i = 0u; i < level; ++i )
    {
      gates[level].symmetry_breaking_different( gates[i] );
    }

    if ( with_xor )
    {
      gates[level].symmetry_breaking_constant_input();
    }
  }

  void constrain( const mig_graph& mig )
  {
    z3::params p( ctx );
    p.set( "mbqi", true );
    solver.set( p );

    const auto& info = mig_info( mig );
    assert( info.inputs.size() == num_vars );
    assert( info.outputs.size() == 1u );

    /* for later */
    z3::expr_vector all_vars( ctx );

    /* value constraints */
    std::vector<z3::expr> out;
    std::vector<std::vector<z3::expr>> in( 3u );

    std::vector<z3::expr> input;

    for ( auto i = 0u; i < num_vars; ++i )
    {
      input += ctx.bool_const( str( format( "x%d" ) % i ).c_str() );
      all_vars.push_back( input.back() );
    }

    //auto constraint = ctx.bool_val( true );

    for ( auto level = 0u; level < gates.size(); ++level )
    {
      /* assertions for in[x][j][level] = neg[level] ^ ite( sel[level], ... ) */
      for ( auto x = 0u; x < 3u; ++x )
      {
        /* constant */
        auto expr = implies( equals( gates[level][x].sel, 0 ), gates[level][x].neg );

        /* inputs */
        for ( auto l = 0u; l < num_vars; ++l )
        {
          expr = expr && implies( equals( gates[level][x].sel, l + 1 ), logic_xor( gates[level][x].neg, input[l] ) );
        }

        /* gates */
        for ( auto l = 0u; l < level; ++l )
        {
          expr = expr && implies( equals( gates[level][x].sel, l + 1 + num_vars ), logic_xor( gates[level][x].neg, out[l] ) );
        }

        in[x] += expr;
      }

      if ( with_xor )
      {
        out += implies( !gates[level].type(), ( in[0u][level] && in[1u][level] ) || ( in[0u][level] && in[2u][level] ) || ( in[1u][level] && in[2u][level] ) )
          && implies( gates[level].type(), logic_xor( in[0u][level], in[1u][level] ) );
      }
      else
      {
        out += ( in[0u][level] && in[1u][level] ) || ( in[0u][level] && in[2u][level] ) || ( in[1u][level] && in[2u][level] );
      }
    }

    /* constrain mig */
    std::vector<boost::optional<z3::expr>> node_to_expr( num_vertices( mig ) );
    node_to_expr[0u] = ctx.bool_val( false );
    for ( auto i : index( info.inputs ) )
    {
      node_to_expr[i.value] = input[i.index];
    }

    std::vector<mig_node> topsort( num_vertices( mig ) );
    boost::topological_sort( mig, topsort.begin() );

    for ( auto node : topsort )
    {
      if ( out_degree( node, mig ) == 0u ) continue;

      const auto children = get_children( mig, node );

      const z3::expr c1 = logic_xor( *node_to_expr[children[0].node], ctx.bool_val( children[0].complemented ) );
      const z3::expr c2 = logic_xor( *node_to_expr[children[1].node], ctx.bool_val( children[1].complemented ) );
      const z3::expr c3 = logic_xor( *node_to_expr[children[2].node], ctx.bool_val( children[2].complemented ) );

      node_to_expr[node] = ( c1 && c2 ) || ( c1 && c3 ) || ( c2 && c3 );
    }

    /* join constraints */
    const auto o = info.outputs[0u].first;
    const auto out1 = output_inverter ? logic_xor( out.back(), out_neg ) : out.back();
    const auto out2 = logic_xor( *node_to_expr[o.node], ctx.bool_val( o.complemented ) );

    /* build everything */
    const auto forall_expr = forall( all_vars, out1 == out2 );
    solver.add( forall_expr );
  }

  void constrain( const tt& spec )
  {
    auto N = 1u << num_vars;

    std::vector<std::vector<z3::expr>> out( N );
    std::vector<std::vector<std::vector<z3::expr>>> in( 3u, std::vector<std::vector<z3::expr>>( N ) );

    /* value constraints */
    for ( auto j = 0u; j < N; ++j )
    {
      for ( auto level = 0u; level < gates.size(); ++level )
      {
        out[j] += ctx.bool_const( str( format( "out_%d_%d" ) % j % level ).c_str() );
        in[0u][j] += ctx.bool_const( str( format( "in1_%d_%d" ) % j % level ).c_str() );
        in[1u][j] += ctx.bool_const( str( format( "in2_%d_%d" ) % j % level ).c_str() );
        in[2u][j] += ctx.bool_const( str( format( "in3_%d_%d" ) % j % level ).c_str() );

        /* assertion for out[j][level] = M(in1[j][level],in2[j][level],in3[j][level] */
        if ( with_xor )
        {
          solver.add( out[j][level] == ( implies( !gates[level].type(), ( in[0u][j][level] && in[1u][j][level] ) || ( in[0u][j][level] && in[2u][j][level] ) || ( in[1u][j][level] && in[2u][j][level] ) )
                                         && implies( gates[level].type(), logic_xor( in[0u][j][level], in[1u][j][level] ) ) ) );
        }
        else
        {
          solver.add( out[j][level] == ( ( in[0u][j][level] && in[1u][j][level] ) || ( in[0u][j][level] && in[2u][j][level] ) || ( in[1u][j][level] && in[2u][j][level] ) ) );
        }

        /* assertions for in[x][j][level] = neg[level] ^ ite( sel[level], ... ) */
        boost::dynamic_bitset<> val( num_vars, j );
        for ( auto x = 0u; x < 3u; ++x )
        {
          solver.add( implies( equals( gates[level][x].sel, 0 ),
                               in[x][j][level] == logic_xor( gates[level][x].neg, ctx.bool_val( false ) ) ) );

          for ( auto l = 0u; l < num_vars; ++l )
          {
            solver.add( implies( equals( gates[level][x].sel, l + 1 ),
                                 in[x][j][level] == logic_xor( gates[level][x].neg, ctx.bool_val( val[l] ) ) ) );
          }
          for ( auto l = 0u; l < level; ++l )
          {
            solver.add( implies( equals( gates[level][x].sel, l + 1 + num_vars ),
                                 in[x][j][level] == logic_xor( gates[level][x].neg, out[j][l] ) ) );
          }
        }
      }

      if ( output_inverter )
      {
        solver.add( logic_xor( out[j].back(), out_neg ) == ctx.bool_val( spec[j] ) );
      }
      else
      {
        solver.add( out[j].back() == ctx.bool_val( spec[j] ) );
      }
    }
  }

  mig_graph extract_mig( const std::string& model_name, const std::string& output_name, bool verbose )
  {
    mig_graph mig;
    mig_initialize( mig, model_name );

    std::vector<mig_function> inputs, nodes;
    for ( auto i = 0u; i < num_vars; ++i )
    {
      inputs += mig_create_pi( mig, str( format( "x%d" ) % i ) );
    }

    const auto m = solver.get_model();

    for ( auto i = 0u; i < gates.size(); ++i )
    {
      mig_function children[3];

      for ( auto x = 0u; x < 3u; ++x )
      {
        auto sel_val = gates[i][x].sel_value( m );

        if ( sel_val == 0 )
        {
          children[x] = mig_get_constant( mig, false );
        }
        else if ( sel_val > 0 && sel_val <= num_vars )
        {
          children[x] = inputs[sel_val - 1u];
        }
        else
        {
          children[x] = nodes[sel_val - num_vars - 1u];
        }

        if ( gates[i][x].neg_value( m ) )
        {
          children[x] = !children[x];
        }
      }

      nodes += mig_create_maj( mig, children[0], children[1], children[2] );

      if ( verbose )
      {
        std::cout << format( "added node (%d,%d) for i = %d" ) % nodes.back().node % nodes.back().complemented % i << std::endl;
        for ( auto x = 0u; x < 3u; ++x )
        {
          std::cout << format( "  - child %d = (%d,%d)" ) % x % children[x].node % children[x].complemented << std::endl;
        }
      }
    }

    auto f = nodes.back();

    if ( output_inverter && expr_to_bool( m.eval( out_neg ) ) )
    {
      f = !f;
    }

    mig_create_po( mig, f, output_name );

    return mig;
  }

  xmg_graph extract_xmg( const std::string& model_name, const std::string& output_name, bool verbose )
  {
    xmg_graph xmg( model_name );

    std::vector<xmg_function> inputs, nodes;
    for ( auto i = 0u; i < num_vars; ++i )
    {
      inputs += xmg.create_pi( str( format( "x%d" ) % i ) );
    }

    const auto m = solver.get_model();

    for ( auto i = 0u; i < gates.size(); ++i )
    {
      const auto type = gates[i].type_value( m );

      xmg_function children[3];

      for ( auto x = 0u; x < ( type ? 2u : 3u ); ++x )
      {
        std::cout << "i: " << i << ", x: " << x << std::endl;
        auto sel_val = gates[i][x].sel_value( m );

        if ( sel_val == 0 )
        {
          children[x] = xmg.get_constant( false );
        }
        else if ( sel_val > 0 && sel_val <= num_vars )
        {
          children[x] = inputs[sel_val - 1u];
        }
        else
        {
          children[x] = nodes[sel_val - num_vars - 1u];
        }

        if ( gates[i][x].neg_value( m ) )
        {
          children[x] = !children[x];
        }
      }

      if ( !type )
      {
        nodes += xmg.create_maj( children[0], children[1], children[2] );
      }
      else
      {
        nodes += xmg.create_xor( children[0], children[1] );
      }

      if ( verbose )
      {
        std::cout << format( "added %s node (%d,%d) for i = %d" ) % ( type ? "XOR" : "MAJ" ) % nodes.back().node % nodes.back().complemented % i << std::endl;
        for ( auto x = 0u; x < ( type ? 2u : 3u ); ++x )
        {
          std::cout << format( "  - child %d = (%d,%d)" ) % x % children[x].node % children[x].complemented << std::endl;
        }
      }
    }

    auto f = nodes.back();

    if ( output_inverter && expr_to_bool( m.eval( out_neg ) ) )
    {
      f = !f;
    }

    xmg.create_po( f, output_name );

    return xmg;
  }

  void block_solution()
  {
    const auto m = solver.get_model();
    auto clause = ctx.bool_val( false );

    for ( auto i = 0u; i < gates.size(); ++i )
    {
      for ( auto x = 0u; x < 3u; ++x )
      {
        clause = clause || ( gates[i][x].sel != m.eval( gates[i][x].sel ) ) || ( gates[i][x].neg != m.eval( gates[i][x].neg ) );
      }

      if ( with_xor )
      {
        clause = clause || ( gates[i].type() != m.eval( gates[i].type() ) );
      }
    }

    solver.add( clause );
  }

  void add_depth_constraints( int max_depth )
  {
    const auto num_gates = gates.size();

    /* depth variables d[x][i], x = 0,1,2,3, i = 0,1,2,...,num_gates - 1 */
    std::vector<std::vector<z3::expr>> dvars( 4u );

    for ( auto i = 0u; i < num_gates; ++i )
    {
      /* create variables, it's safe because of topological order */
      for ( auto x = 0u; x < 4u; ++x )
      {
        if ( enc_bv )
        {
          dvars[x] += ctx.bv_const( str( format( "dvars%d_%d" ) % x % i ).c_str(), bw );
        }
        else
        {
          dvars[x] += ctx.int_const( str( format( "dvars%d_%d" ) % x % i ).c_str() );
        }
      }

      /* depth of output is maximum of input depths + 1 */
      const auto& a = dvars[0u][i];
      const auto& b = dvars[1u][i];
      const auto& c = dvars[2u][i];

      solver.add( dvars[3u][i] == ite( a > b, ite( a > c, a, c ), ite( b > c, b, c ) ) + 1 );

      /* depth of inputs */
      for ( auto x = 0u; x < 3u; ++x )
      {
        solver.add( implies( less_equals( gates[i][x].sel, num_vars ),
                             equals( dvars[x][i], 0 ) ) );

        for ( auto l = 0u; l < i; ++l )
        {
          solver.add( implies( equals( gates[i][x].sel, l + num_vars + 1 ),
                               dvars[x][i] == dvars[3u][l] ) );
        }
      }
    }

    solver.add( less_equals( dvars[3u][num_gates - 1u], max_depth ) );
  }

  /* some helper methods */
  z3::expr equals( const z3::expr& expr, unsigned value )
  {
    if ( enc_bv )
    {
      return ( expr == ctx.bv_val( value, bw ) );
    }
    else
    {
      return ( expr == static_cast<int>( value ) );
    }
  }

  z3::expr less_equals( const z3::expr& expr, unsigned value )
  {
    if ( enc_bv )
    {
      return ( z3::ule( expr, ctx.bv_val( value, bw ) ) );
    }
    else
    {
      return ( expr <= static_cast<int>( value ) );
    }
  }

  unsigned num_vars;
  bool output_inverter;
  bool with_xor;
  bool enc_bv;
  z3::context ctx;
  z3::solver solver;

  std::vector<gate> gates;

  z3::expr out_neg;

  unsigned bw;
};

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

template<typename T>
class exact_mig_manager
{
public:
  exact_mig_manager( const spec_representation& spec, const properties::ptr& settings )
      : spec( spec )
  {
    /* meta data */
    model_name          = get( settings, "model_name",          std::string( "exact" ) );
    output_name         = get( settings, "output_name",         std::string( "f" ) );

    /* control algorithm */
    objective           = get( settings, "objective",           0u );   /* 0: size, 1: size/depth, 2: depth/size */
    start               = get( settings, "start",               1u );
    start_depth         = get( settings, "start_depth",         1u );
    incremental         = get( settings, "incremental",         false );
    all_solutions       = get( settings, "all_solutions",       false );

    /* encoding */
    output_inverter     = get( settings, "output_inverter",     false );
    enc_with_bitvectors = get( settings, "enc_with_bitvectors", false );

    verbose             = get( settings, "verbose",             false );
  }

  std::vector<T> run()
  {
    /* check trivial case */
    const auto triv = spec.is_trivial();
    if ( (bool)triv )
    {
      return {create_trivial<T>( triv->first, triv->second )};
    }

    if ( incremental )
    {
      return exact_mig_size_incremental();
    }
    else
    {
      switch ( objective )
      {
      case 0u:
        return exact_mig_size_explicit();
      case 1u:
        return exact_mig_size_depth_explicit();
      case 2u:
        return exact_mig_depth_size_explicit();
      default:
        assert( false );
      }
    }
  }

  std::vector<T> exact_mig_size_explicit()
  {
    auto k = start;
    while ( true )
    {
      if ( verbose )
      {
        std::cout << boost::format( "[i] check for realization with %d gates" ) % k << std::endl;
      }

      const auto inst = create_instance();

      for ( auto i = 0u; i < k; ++i )
      {
        inst->add_level();
      }

      constrain( inst );

      if ( inst->solver.check() == z3::sat )
      {
        return extract_solutions( inst );
      }

      ++k;
    }
  }

  std::vector<T> exact_mig_size_depth_explicit()
  {
    auto k = start;
    auto d = 0; /* d = 0 means still looking for best size (depth-0 migs are found before this function is called) */

    while ( true )
    {
      if ( verbose )
      {
        if ( d )
        {
          std::cout << boost::format( "[i] check for realization with %d gates and depth %d" ) % k % d << std::endl;
        }
        else
        {
          std::cout << boost::format( "[i] check for realization with %d gates" ) % k << std::endl;
        }
      }

      const auto inst = create_instance();

      for ( auto i = 0u; i < k; ++i )
      {
        inst->add_level();
      }

      constrain( inst );

      if ( d ) /* find best depth */
      {
        inst->add_depth_constraints( d );
        if ( inst->solver.check() == z3::sat )
        {
          return extract_solutions( inst );
        }
        else
        {
          ++d;
        }
      }
      else
      {
        if ( inst->solver.check() == z3::sat )
        {
          d = start_depth;
        }
        else
        {
          ++k;
        }
      }
    }
  }

  std::vector<T> exact_mig_depth_size_explicit()
  {
    auto d = start_depth;

    while ( true )
    {
      const auto max_gates = ( static_cast<int>( pow( 3, d ) ) - 1 ) / 2;

      for ( auto k = 1u; k <= max_gates; ++k )
      {
        if ( verbose )
        {
          std::cout << boost::format( "[i] check for realization with depth %d and %d gates" ) % d % k << std::endl;
        }

        const auto inst = create_instance();

        for ( auto i = 0u; i < k; ++i )
        {
          inst->add_level();
        }

        constrain( inst );
        inst->add_depth_constraints( d );

        if ( inst->solver.check() == z3::sat )
        {
          return extract_solutions( inst );
        }
      }

      ++d;
    }
  }

  std::vector<T> exact_mig_size_incremental()
  {
    const auto inst = create_instance();

    for ( unsigned i = 1u; i < start; ++i )
    {
      inst->add_level();
    }

    while ( true )
    {
      inst->add_level();

      if ( verbose )
      {
        std::cout << boost::format( "[i] check for realization with %d gates" ) % inst->gates.size() << std::endl;
      }

      /* solve */
      inst->solver.push();
      constrain( inst );
      if ( inst->solver.check() == z3::sat )
      {
        return extract_solutions( inst );
      }
      inst->solver.pop();
    }
  }

private:
  template<typename C, typename std::enable_if<std::is_same<mig_graph, C>::value>::type* = nullptr>
  bool with_xor() const
  {
    return false;
  }

  template<typename C, typename std::enable_if<std::is_same<xmg_graph, C>::value>::type* = nullptr>
  bool with_xor() const
  {
    return true;
  }

  template<typename C, typename std::enable_if<std::is_same<mig_graph, C>::value>::type* = nullptr>
  mig_graph create_trivial( unsigned id, bool complement )
  {
    mig_graph mig;
    mig_initialize( mig, model_name );
    mig_create_po( mig, ( id == 0u ? mig_get_constant( mig, false ) : mig_create_pi( mig, str( format( "x%d" ) % id ) ) ) ^ complement, output_name );
    return mig;
  }

  template<typename C, typename std::enable_if<std::is_same<xmg_graph, C>::value>::type* = nullptr>
  xmg_graph create_trivial( unsigned id, bool complement )
  {
    xmg_graph xmg( model_name );
    xmg.create_po( ( id == 0u ? xmg.get_constant( false ) : xmg.create_pi( str( format( "x%d" ) % id ) ) ) ^ complement, output_name );
    return xmg;
  }

  template<typename C, typename std::enable_if<std::is_same<mig_graph, C>::value>::type* = nullptr>
  mig_graph extract_solution( const std::shared_ptr<exact_mig_instance>& inst ) const
  {
    return inst->extract_mig( model_name, output_name, verbose );
  }

  template<typename C, typename std::enable_if<std::is_same<xmg_graph, C>::value>::type* = nullptr>
  xmg_graph extract_solution( const std::shared_ptr<exact_mig_instance>& inst ) const
  {
    return inst->extract_xmg( model_name, output_name, verbose );
  }

  inline std::shared_ptr<exact_mig_instance> create_instance() const
  {
    return std::make_shared<exact_mig_instance>( spec.num_vars(), output_inverter, with_xor<T>(), enc_with_bitvectors, spec.is_explicit() );
  }

  std::vector<T> extract_solutions( const std::shared_ptr<exact_mig_instance>& inst ) const
  {
    if ( all_solutions )
    {
      std::vector<T> migs;

      do
      {
        migs.push_back( extract_solution<T>( inst ) );
        inst->block_solution();
      } while ( inst->solver.check() == z3::sat );

      return migs;
    }
    else
    {
      return {extract_solution<T>( inst )};
    }
  }

  struct constrain_visitor : public boost::static_visitor<void>
  {
    constrain_visitor( const std::shared_ptr<exact_mig_instance>& inst ) : inst( inst ) {}

    void operator()( const tt& spec ) const
    {
      inst->constrain( spec );
    }

    void operator()( const mig_graph& spec ) const
    {
      inst->constrain( spec );
    }

  private:
    const std::shared_ptr<exact_mig_instance>& inst;
  };

  void constrain( const std::shared_ptr<exact_mig_instance>& inst ) const
  {
    spec.apply_visitor( constrain_visitor( inst ) );
  }

private:
  spec_representation spec;
  unsigned start;
  unsigned start_depth;
  std::string model_name;
  std::string output_name;
  unsigned objective;
  bool incremental;
  bool all_solutions;
  bool output_inverter;
  bool enc_with_bitvectors;
  bool verbose;
};

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_graph exact_mig_with_sat( const tt& spec,
                              const properties::ptr& settings,
                              const properties::ptr& statistics )
{
  /* settings */
  const auto all_solutions = get( settings, "all_solutions", false );

  /* timing */
  properties_timer t( statistics );

  auto n = tt_num_vars( spec );
  assert( !spec.empty() && n <= 6u );

  exact_mig_manager<mig_graph> mgr( spec_representation( spec ), settings );

  const auto migs = mgr.run();
  if ( all_solutions )
  {
    set( statistics, "all_solutions", migs );
  }
  return migs.front();
}

mig_graph exact_mig_with_sat( const mig_graph& spec,
                              const properties::ptr& settings,
                              const properties::ptr& statistics )
{
  /* settings */
  const auto all_solutions = get( settings, "all_solutions", false );

  /* timing */
  properties_timer t( statistics );

  exact_mig_manager<mig_graph> mgr( spec_representation( spec ), settings );

  const auto migs = mgr.run();
  if ( all_solutions )
  {
    set( statistics, "all_solutions", migs );
  }
  return migs.front();
}

xmg_graph exact_xmg_with_sat( const tt& spec,
                              const properties::ptr& settings,
                              const properties::ptr& statistics )
{
  /* settings */
  const auto all_solutions = get( settings, "all_solutions", false );

  /* timing */
  properties_timer t( statistics );

  auto n = tt_num_vars( spec );
  assert( !spec.empty() && n <= 6u );

  exact_mig_manager<xmg_graph> mgr( spec_representation( spec ), settings );

  const auto migs = mgr.run();
  if ( all_solutions )
  {
    set( statistics, "all_solutions", migs );
  }
  return migs.front();
}

xmg_graph exact_xmg_with_sat( const mig_graph& spec,
                              const properties::ptr& settings,
                              const properties::ptr& statistics )
{
  /* settings */
  const auto all_solutions = get( settings, "all_solutions", false );

  /* timing */
  properties_timer t( statistics );

  exact_mig_manager<xmg_graph> mgr( spec_representation( spec ), settings );

  const auto xmgs = mgr.run();
  if ( all_solutions )
  {
    set( statistics, "all_solutions", xmgs );
  }
  return xmgs.front();
}

mig_graph exact_mig_with_bdds( const tt& spec,
                               const properties::ptr& settings,
                               const properties::ptr& statistics )
{
  properties_timer t( statistics );

  assert( false && "not yet implemented" );

  return mig_graph();
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
