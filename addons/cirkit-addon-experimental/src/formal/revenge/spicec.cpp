/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spicec.hpp"

#include <vector>

#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include <cuddObj.hh>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/functions/simulation_graph.hpp>
#include <classical/utils/aig_utils.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/add_aig_with_gia.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

class constant_propagation_simulator : public aig_simulator<aig_function>
{
public:
  constant_propagation_simulator( aig_graph& dest, const std::map<aig_node, bool>& data, const std::map<aig_node, aig_function>& controls )
    : dest( dest ),
      data( data ),
      controls ( controls )
  {
  }

  aig_function get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const
  {
    const auto itData = data.find( node );
    if ( itData != data.end() )
    {
      return aig_get_constant( dest, itData->second );
    }

    return controls.at( node );
  }

  aig_function get_constant() const
  {
    return aig_get_constant( dest, false );
  }

  aig_function invert( const aig_function& v ) const
  {
    return !v;
  }

  aig_function and_op( const aig_node& node, const aig_function& v1, const aig_function& v2 ) const
  {
    return aig_create_and( dest, v1, v2 );
  }

private:
  aig_graph& dest;
  const std::map<aig_node, bool>& data;
  const std::map<aig_node, aig_function>& controls;
};

template<typename Solver>
class control_assignment
{
public:
  control_assignment( const aig_graph& target, const std::vector<aig_node>& controls, const std::vector<unsigned>& types )
    : target( target ),
      info( aig_info( target ) ),
      controls( controls ),
      data_inputs( info.inputs.size() - controls.size() )
  {
    /* create initial SAT formula */
    solver = make_solver<Solver>();
    compute_data_inputs();

    aig_initialize( control_aig );
    for ( const auto& c : controls )
    {
      control_map.insert( {c, aig_create_pi( control_aig, info.node_names.at( c ) )} );
    }

    std::vector<aig_function> all_fs;
    for ( const auto& vector : create_simulation_vectors( data_inputs.size(), types ) )
    {
      boost::push_back( all_fs, simulate( vector ) );
    }

    sid = add_aig_with_gia( solver, control_aig, 1, control_piids, control_poids );
    assert( control_piids.size() == controls.size() );
    assert( control_poids.size() == all_fs.size() );
    create_sat_formula( all_fs );

    foreach_control_assignment( []() {} );
  }

  template<typename Fn>
  void foreach_control_assignment( Fn&& f )
  {
    /* solve */
    solver_result_t result;

    while ( true )
    {
      solver_execution_statistics statistics;
      result = solve( solver, statistics, {} );
      if ( result != boost::none )
      {
        std::vector<int> blocking;

        std::cout << "[i] assignment:";
        for ( auto id : control_piids )
        {
          blocking.push_back( result->first[id - 1u] ? -id : id );
          std::cout << " " << result->first[id - 1u];
        }
        std::cout << std::endl;

        add_clause( solver )( blocking );
      }
      else
      {
        break;
      }
    }

    // TODO

    /* if satisfiable, call f and block solution */
    // TODO
  }

private:
  void compute_data_inputs()
  {
    std::set<aig_node> set_inputs( info.inputs.begin(), info.inputs.end() );
    std::set<aig_node> set_controls( controls.begin(), controls.end() );

    boost::set_difference( set_inputs, set_controls, data_inputs.begin() );
  }

  std::vector<aig_function> simulate( const boost::dynamic_bitset<>& vector )
  {
    std::map<aig_node, bool> assignment;

    for ( const auto& pi : index( data_inputs ) )
    {
      assignment.insert( {pi.value, vector[pi.index]} );
    }

    constant_propagation_simulator sim( control_aig, assignment, control_map );

    const auto result = simulate_aig( target, sim );

    std::vector<aig_function> fs;
    for ( const auto& output : info.outputs )
    {
      aig_create_po( control_aig, result.at( output.first ), output.second + "_" + to_string( vector ) );
      fs.push_back( result.at( output.first ) );
    }
    return fs;
  }

  void create_sat_formula( const std::vector<aig_function>& all_fs )
  {
    for ( const auto& f : index( all_fs ) )
    {
      const auto polarity = f.value.complemented ? -1 : 1;
      equals( solver, sid++, control_poids[f.index] * polarity );
    }
  }

private:
  const aig_graph&             target;
  const aig_graph_info&        info;
  const std::vector<aig_node>& controls;
  std::vector<aig_node>        data_inputs;

  Solver             solver;
  int                sid;

  aig_graph                        control_aig;
  std::map<aig_node, aig_function> control_map;
  std::vector<int>                 control_piids;
  std::vector<int>                 control_poids;
};

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

bool spicec_with_lad( const aig_graph& pattern, const aig_graph& target,
                      const properties::ptr& settings,
                      const properties::ptr& statistics )
{
  const auto& info = aig_info( target );
  control_assignment<minisat_solver> assignment( target, {info.inputs[16u], info.inputs[17u]}, {0u,3u} );

  return false;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
