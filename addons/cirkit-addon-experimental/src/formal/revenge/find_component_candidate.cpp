/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "find_component_candidate.hpp"

#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/string_utils.hpp>
#include <classical/functions/lad2.hpp>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

namespace detail
{
void blocking_nodes_from_partitions( const std::vector<unsigned>& partition_target,
                                     const std::vector<unsigned>& partition_pattern,
                                     const std::function<void(unsigned, unsigned)>& f )
{
  auto offset_target = 0u;
  for ( auto i = 0u; i < partition_target.size(); ++i )
  {
    auto offset_pattern = 0u;
    for ( auto j = 0u; j < partition_pattern.size(); ++j )
    {
      if ( i != j )
      {
        for ( auto k = 0u; k < partition_target[i]; ++k )
        {
          for ( auto l = 0u; l < partition_pattern[j]; ++l )
          {
            f( offset_target + k, offset_pattern + l );
          }
        }
      }
      offset_pattern += partition_pattern[j];
    }
    offset_target += partition_target[i];
  }
}

}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

struct pruning_hook
{
  pruning_hook( const aig_graph_info& info, const std::vector<std::string>& input_names )
    : input_names( input_names )
  {
    for ( const auto& name : input_names )
    {
      indexes += aig_input_index( info, aig_node_by_name( info, name ) );
    }
  }

  bool operator()( lad2_domain& d )
  {
    for ( const auto& i : index( indexes ) )
    {
      /* show the domain of the first pattern element */
      std::cout << boost::format( "D[%s] =" ) % input_names[i.index];
      for ( const auto& v : d.get( i.value ) )
      {
        std::cout << " " << v;
      }
      std::cout << std::endl;
    }

    return true;
  }

private:
  const std::vector<std::string>& input_names;
  std::vector<unsigned>           indexes;
};

bool find_component_candidate_directed_lad4( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                                             const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                                             properties::ptr settings, properties::ptr statistics )
{
  /* Settings */
  const auto circuit_dotname       = get( settings, "circuit_dotname",       std::string() );
  const auto component_dotname     = get( settings, "component_dotname",     std::string() );
  const auto circuit_graphname     = get( settings, "circuit_graphname",     std::string( "/tmp/target.graph" ) );
  const auto component_graphname   = get( settings, "component_graphname",   std::string( "/tmp/pattern.graph" ) );
  const auto support_edges         = get( settings, "support_edges",         false );
  const auto simulation_signatures = get( settings, "simulation_signatures", boost::optional<unsigned>() );
  const auto pruning_inputs        = get( settings, "pruning_inputs",        std::vector<std::string>() );
  const auto texlogname            = get( settings, "texlogname",            std::string( "/tmp/log.tex" ) );
  const auto verbose               = get( settings, "verbose",               false );

  /* Timer */
  properties_timer t( statistics );

  /* AIG info */
  const auto& target_info  = aig_info( circ );
  const auto& pattern_info = aig_info( component );

  properties::ptr dl_settings = std::make_shared<properties>();
  dl_settings->set( "support_edges",         support_edges );
  dl_settings->set( "simulation_signatures", simulation_signatures );
  dl_settings->set( "texlogname",            texlogname );
  dl_settings->set( "verbose",               verbose );
  dl_settings->set( "on_filter",             domain_hook_t( domain_func_t( pruning_hook( pattern_info, pruning_inputs ) ) ) );
  properties::ptr dl_statistics = std::make_shared<properties>();
  std::vector<unsigned> mapping;
  auto result = directed_lad2_from_aig( mapping, circ, component, types, dl_settings, dl_statistics );
  set( statistics, "num_branches", dl_statistics->get<unsigned>( "num_branches" ) );
  if ( result )
  {
    unsigned num_vec_target = dl_statistics->get<unsigned>( "target_vertices" ) - target_info.inputs.size() - target_info.outputs.size();
    unsigned num_vec_pattern = dl_statistics->get<unsigned>( "pattern_vertices" ) - pattern_info.inputs.size() - pattern_info.outputs.size();
    detail::extract_mapping( input_mapping, output_mapping, mapping,
                             target_info, num_vec_target, pattern_info, num_vec_pattern,
                             verbose );
    return true;
  }
  else
  {
    return false;
  }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
