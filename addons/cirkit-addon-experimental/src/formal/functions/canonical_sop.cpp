/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "canonical_sop.hpp"

#include <core/utils/bitset_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/add_aig_with_gia.hpp>
#include <formal/sat/utils/lexicographic.hpp>

#define L(x) if ( verbose ) { std::cout << x << std::endl; }

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

template<typename Solver>
cube_vec_t canonical_sop( Solver& solver, const std::vector<int>& vars, int output, int sid, bool verbose )
{
  const auto n = vars.size();
  cube_vec_t cubes;
  solver_execution_statistics stats;

  boost::dynamic_bitset<> sol;

  std::vector<int> blocking_vars = { output };

  while ( true )
  {
    /* get the largest solution */
    try
    {
      sol = lexicographic_largest_solution( solver, vars, blocking_vars );
    }
    catch ( unsat_exception& e )
    {
      break;
    }

    /* expand cube */
    boost::dynamic_bitset<> care( n );
    std::deque<int> assumptions;

    for ( auto i = 0u; i < n; ++i )
    {
      assumptions.push_back( sol[i] ? vars[i] : -vars[i] );
    }
    assumptions.push_back( -output );

    solver_gen_model( solver, false );
    for ( auto i = 0u; i < n; ++i )
    {
      assumptions.front() *= -1;
      if ( solve( solver, stats, std::vector<int>( assumptions.begin(), assumptions.end() ) ) != boost::none )
      {
        care.set( i );
        const auto lit = assumptions.front();
        assumptions.pop_front();
        assumptions.push_back( lit );
      }
      else
      {
        sol.reset( i );
        assumptions.pop_front();
      }
    }
    solver_gen_model( solver, true );

    cubes.push_back( cube( sol, care ) );

    /* assumptions now contains a blocking clause for the obtained cube and -output */
    assert( assumptions.front() == -output );
    assumptions.front() = sid;
    blocking_vars.push_back( -sid );
    sid++;
    add_clause( solver )( assumptions );
  }

  return cubes;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

cube_vec_t canonical_sop( const aig_graph& aig,
                          const properties::ptr& settings,
                          const properties::ptr& statistics )
{
  /* settings */
  const auto verbose = get( settings, "verbose", false );

  /* runtime */
  properties_timer t( statistics );

  /* solver and SAT instance */
  auto solver = make_solver<minisat_solver>();
  std::vector<int> piids, poids;
  auto sid = add_aig_with_gia( solver, aig, 1, piids, poids );

  /* AIG should have a single output */
  assert( poids.size() == 1u );

  return canonical_sop( solver, piids, poids[0], sid, verbose );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
