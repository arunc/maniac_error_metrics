/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error_metrics_sat.hpp"

#include <boost/format.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/aig_word.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/add_aig_with_gia.hpp>
#include <formal/sat/utils/lexicographic.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

aig_graph difference_miter( const aig_graph& f, const aig_graph& fhat )
{
  /* merge both AIGs */
  aig_graph diff = strash( f );

  const auto s_settings = std::make_shared<properties>();
  s_settings->set( "reuse_inputs", true );
  strash( fhat, diff, s_settings );

  auto& info = aig_info( diff );

  /* collect output words */
  aig_word op1, op2, *current = &op1;

  for ( auto i = 0u; i < info.outputs.size(); ++i )
  {
    if ( ( i << 1u ) == info.outputs.size() )
    {
      current = &op2;
    }

    current->push_back( info.outputs[i].first );
  }

  /* remove old output pointers */
  info.outputs.clear();

  const auto sub = aig_create_bvsub( diff, op1, op2 );

  assert( false );

  // TODO
  const aig_word abs/* = aig_create_abs( aig, diff )*/;

  for ( const auto& f : index( abs ) )
  {
    aig_create_po( diff, f.value, boost::str( boost::format( "abs_%d" ) % f.index ) );
  }

  return diff;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

boost::multiprecision::uint256_t worst_case( const aig_graph& f,
                                             const aig_graph& fhat,
                                             const properties::ptr& settings,
                                             const properties::ptr& statistics )
{
  const auto diff = difference_miter( f, fhat );

  auto solver = make_solver<minisat_solver>();
  std::vector<int> piids, poids;
  add_aig_with_gia( solver, diff, 1, piids, poids );

  const auto max = lexicographic_largest_solution( solver, poids );

  return to_multiprecision<boost::multiprecision::uint256_t>( max );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
