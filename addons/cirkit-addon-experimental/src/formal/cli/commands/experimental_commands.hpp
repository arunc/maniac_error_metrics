/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file experimental_commands.hpp
 *
 * @brief Experimental formal commands
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef CLI_EXPERIMENTAL_FORMAL_COMMANDS_HPP
#define CLI_EXPERIMENTAL_FORMAL_COMMANDS_HPP

//--boost bug in any_iterator -- #include <formal/cli/commands/csop.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/diagnose.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/exact_mig.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/exact_xmg.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/modelcheck.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/satnpn.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/spicec.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/spiec.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/symm.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/unate.hpp>
//--boost bug in any_iterator -- #include <formal/cli/commands/xmmiter.hpp>
//--boost bug in any_iterator -- 
//--boost bug in any_iterator -- #define EXPERIMENTAL_FORMAL_COMMANDS \
//--boost bug in any_iterator --     ADD_COMMAND( csop );             \
//--boost bug in any_iterator --     ADD_COMMAND( diagnose );         \
//--boost bug in any_iterator --     ADD_COMMAND( exact_mig );        \
//--boost bug in any_iterator --     ADD_COMMAND( exact_xmg );        \
//--boost bug in any_iterator --     ADD_COMMAND( modelcheck );       \
//--boost bug in any_iterator --     ADD_COMMAND( satnpn );           \
//--boost bug in any_iterator --     ADD_COMMAND( spicec );           \
//--boost bug in any_iterator --     ADD_COMMAND( spiec );            \
//--boost bug in any_iterator --     ADD_COMMAND( symm );             \
//--boost bug in any_iterator --     ADD_COMMAND( unate );            \
//--boost bug in any_iterator --     ADD_COMMAND( xmmiter );

#define EXPERIMENTAL_FORMAL_COMMANDS 

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
