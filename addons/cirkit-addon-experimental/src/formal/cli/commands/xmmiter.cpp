/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmmiter.hpp"

#include <boost/program_options.hpp>

#include <classical/aig.hpp>
#include <classical/mig.hpp>
#include <classical/cli/stores.hpp>
#include <classical/cli/stores_experimental.hpp>
#include <formal/optimization/maj_xor_rewriting.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

xmmiter_command::xmmiter_command( const environment::ptr& env )
  : command( env, "Miter based on XOR/MAJ rewriting" )
{
  opts.add_options()
    ( "id1",                value_with_default( &id1 ),  "Id of first circuit" )
    ( "id2",                value_with_default( &id2 ),  "Id of second circuit" )
    ( "propagate,p",                                     "Perform XOR propagation" )
    ( "max_growth_factor",  value( &max_growth_factor ), "Max growth factor" )
    ( "max_rounds",         value( &max_rounds ),        "Max rounds" )
    ( "functional_hashing",                              "Optimize with functional hashing" )
    ( "depth_rewriting",                                 "Optimize with depth rewriting" )
#ifdef ADDON_EPFL
    ( "mighty_rewriting",                                "Optimize with MIGhty rewriting" )
#endif
    ( "new,n",                                           "Create new store entry for result" )
    ;
  be_verbose();
}

command::rules_t xmmiter_command::validity_rules() const
{
  return {
    { [&]() { return id1 < env->store<mig_graph>().size(); }, "no MIG with id1 in store" },
    { [&]() { return id2 < env->store<mig_graph>().size(); }, "no MIG with id2 in store" }
  };
}

bool xmmiter_command::execute()
{
  std::cout << "[i] read from " << id1 << " and " << id2 << std::endl;

  auto& aigs = env->store<aig_graph>();
  const auto& migs = env->store<mig_graph>();
  const auto& mig1 = migs[id1];
  const auto& mig2 = migs[id2];

  if ( aigs.empty() || opts.is_set( "new" ) )
  {
    aigs.extend();
  }

  const auto settings = make_settings();
  settings->set( "propagate", opts.is_set( "propagate" ) );
  settings->set( "functional_hashing", opts.is_set( "functional_hashing" ) );
  settings->set( "depth_rewriting", opts.is_set( "depth_rewriting" ) );
#ifdef ADDON_EPFL
  settings->set( "mighty_rewriting", opts.is_set( "mighty_rewriting" ) );
#endif
  if ( opts.is_set( "max_growth_factor" ) )
  {
    settings->set( "max_growth_factor", boost::optional<unsigned>( max_growth_factor ) );
  }
  if ( opts.is_set( "max_rounds" ) )
  {
    settings->set( "max_rounds", boost::optional<unsigned>( max_rounds ) );
  }
  aigs.current() = create_miter( mig1, mig2, settings, statistics );

  return true;
}

command::log_opt_t xmmiter_command::log() const
{
  log_map_t log;
  log["propagate"] = opts.is_set( "propagate" );
  if ( opts.is_set( "max_growth_factor" ) )
  {
    log["max_growth_factor"] = static_cast<int>( max_growth_factor );
  }
  if ( opts.is_set( "max_rounds" ) )
  {
    log["max_rounds"] = static_cast<int>( max_rounds );
  }
  log["runtime"] = statistics->get<double>( "runtime" );
  log["num_majs"] = statistics->get<std::vector<int>>( "num_majs" );
  log["num_xors"] = statistics->get<std::vector<int>>( "num_xors" );

  return log_opt_t( log );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
