/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dnf_learning.hpp"

#include <classical/utils/aig_utils.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/format.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

unsigned count_literals( const dnf_t& dnf )
{
  auto num_literals = 0u;
  for ( const auto& term : dnf )
  {
    num_literals += term.size();
  }
  return num_literals;
}

unsigned get_input_size_from_samples( const std::set< std::string >& a, const std::set< std::string >& b )
{
  if ( a.size() > 0u )
  {
    return a.begin()->size();
  }
  else if ( b.size() > 0u )
  {
    return b.begin()->size();
  }
  return 0u;
}

aig_function aig_add_dnf( aig_graph& target_aig, std::vector< aig_function >& node_to_function, const dnf_t& dnf, const std::vector< aig_function >& mapping )
{
  /* false case */
  if ( dnf.size() == 0u )
  {
    return aig_get_constant( target_aig, false );
  }

  /* true case */
  if ( dnf.size() == 1u && dnf[ 0u ].size() == 0u )
  {
    return aig_get_constant( target_aig, true );
  }

  /* dnf */
  std::vector< aig_function > d;
  for ( unsigned i = 0u; i < dnf.size(); ++i )
  {
    std::vector< aig_function > c;
    for ( const auto literal : dnf[ i ] )
    {
      const unsigned var = ( literal < 0 ? -literal : literal );
      const auto function = mapping[ var-1u ];
      const auto f = ( ( literal < 0 ) != function.complemented ) ? !node_to_function[ function.node ] : node_to_function[ function.node ];
      c.push_back( f );
    }
    d.push_back( aig_create_nary_and( target_aig, c ) );
  }
  return aig_create_nary_or( target_aig, d );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
