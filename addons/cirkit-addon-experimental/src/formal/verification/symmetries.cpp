/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "symmetries.hpp"

#include <boost/assign/std/vector.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/phoenix.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>

#include <classical/functions/aig_cone.hpp>
#include <classical/functions/strash.hpp>
#include <classical/io/write_aiger.hpp>
#include <classical/utils/aig_utils.hpp>

#include <formal/sat/sat_solver.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/operations/logic.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/add_aig_with_gia.hpp>

#include <abc/abc_api.hpp>
#include <abc/abc_manager.hpp>
#include <abc/functions/cirkit_to_gia.hpp>

#include <misc/extra/extra.h>
#include <opt/sim/sim.h>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

void compute_symmetry_pairs_for_single_output( const aig_graph& aig, unsigned output )
{
  using boost::adaptors::filtered;
  using boost::adaptors::transformed;
  using namespace boost::assign;
  using namespace boost::phoenix::arg_names;

  auto cone = aig_cone( aig, std::vector<unsigned>{ output } );
  strash( cone, cone );

  write_aiger( cone, "/tmp/cone.aag" );

  const auto& info = aig_info( aig );

  auto solver = make_solver<minisat_solver>();
  solver_execution_statistics statistics;
  solver_result_t result;
  solver_gen_model( solver, false );

  std::vector<int> piids, poids;
  auto sid = add_aig_with_gia( solver, cone, 1, piids, poids );

  //std::cout << "[i] piids: " << any_join( piids, " " ) << std::endl
  //          << "[i] poids: " << any_join( poids, " " ) << std::endl;

  assert( poids.size() == 2u );
  not_equals( solver, poids[0u], poids[1u] );

  /* input constraints */
  const auto n = info.inputs.size();
  std::vector<int> equals;
  for ( auto i = 0u; i < n; ++i )
  {
    logic_xnor( solver, piids[i], piids[n + i], sid );
    equals.push_back( sid++ );
  }
  //std::cout << "[i] equals: " << any_join( equals, " " ) << std::endl;

  /* for all pairs */
  for ( auto i1 = 0u; i1 < ( n - 1 ); ++i1 )
  {
    for ( auto i2 = i1 + 1u; i2 < n; ++i2 )
    {
      //std::cout << "[i] check for " << i1 << " and " << i2 << std::endl;

      std::vector<int> assumptions( n - 2u );
      std::function<unsigned(unsigned)> f = [&equals]( unsigned i ) { return equals[i]; };
      boost::copy( boost::counting_range( 0u, (unsigned)n ) |
                       filtered( arg1 != i1 && arg1 != i2 ) | transformed( f ),
                   assumptions.begin() );

      assumptions += piids[i1],-piids[n + i1],-piids[i2],piids[n + i2];

      //std::cout << "[i] assumptions: " << any_join( assumptions, " " ) << std::endl;

      result = solve( solver, statistics, assumptions );

      if ( !(bool)result )
      {
        std::cout << "[i] symmetric pair: " << i1 << " and " << i2 << std::endl;
      }
    }
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

void compute_symmetry_pairs( const aig_graph& aig,
                             const properties::ptr& settings,
                             const properties::ptr& statistics )
{
  /* settings */
  const auto verbose = get( settings, "verbose", false );

  /* timing */
  properties_timer t( statistics );

  const auto& info = aig_info( aig );

  for ( auto j = 0u; j < info.outputs.size(); ++j )
  {
    compute_symmetry_pairs_for_single_output( aig, j );
  }
}

void compute_symmetry_pairs_with_abc( const aig_graph& aig,
                                      const properties::ptr& settings,
                                      const properties::ptr& statistics )
{
  /* settings */
  const auto verbose = get( settings, "verbose", false );

  /* timing */
  properties_timer t( statistics );

  /* convert aig_graph into Abc_Ntk_t */
  abc_manager::get();
  abc::Abc_FrameGetGlobalFrame();

  auto * gia = cirkit_to_gia( aig );
  auto * man = abc::Gia_ManToAig( gia, 0 );
  auto * ntk = abc::Abc_NtkFromAigPhase( man );
  abc::Aig_ManStop( man );

  /* start simulation manager */
  srand( 0xABC );
  auto * p = Sym_ManStart( ntk, verbose ? 1 : 0 );
  p->nPairsTotal = p->nPairsRem =
      abc::Sim_UtilCountAllPairs( p->vSuppFun, p->nSimWords, p->vPairsTotal );

  /* detect symmetries based on circuit structure */
  abc::Sim_SymmsStructCompute( ntk, p->vMatrSymms, p->vSuppFun );
  abc::Sim_UtilCountPairsAll( p );
  p->nPairsSymmStr = p->nPairsSymm;

  /* detect symmetries using simulation */
  for ( auto i = 1u; i <= 1000u; ++i )
  {
    abc::Sim_UtilSetRandom( p->uPatRand, p->nSimWords );
    abc::Sim_SymmsSimulate( p, p->uPatRand, p->vMatrNonSymms );
    if ( i % 50u != 0u ) { continue; }

    assert( abc::Sim_UtilMatrsAreDisjoint( p ) );
    abc::Sim_UtilCountPairsAll( p );
  }

  /* detect symmetries using SAT */
  for ( auto i = 1u; abc::Sim_SymmsGetPatternUsingSat( p, p->uPatRand ); ++i )
  {
    abc::Sim_SymmsSimulate( p, p->uPatRand, p->vMatrNonSymms );
    Sim_XorBit( p->uPatRand, p->iVar1 );
    abc::Sim_SymmsSimulate( p, p->uPatRand, p->vMatrNonSymms );
    Sim_XorBit( p->uPatRand, p->iVar2 );
    abc::Sim_SymmsSimulate( p, p->uPatRand, p->vMatrNonSymms );
    Sim_XorBit( p->uPatRand, p->iVar1 );
    abc::Sim_SymmsSimulate( p, p->uPatRand, p->vMatrNonSymms );
    Sim_XorBit( p->uPatRand, p->iVar2 );

    if ( i % 10u != 0u ) { continue; }

    assert( abc::Sim_UtilMatrsAreDisjoint( p ) );
    abc::Sim_UtilCountPairsAll( p );
  }

  abc::Sim_UtilCountPairsAll( p );

  std::cout << "[i] " << p->vMatrSymms->nSize << std::endl;
  auto * bitmask = static_cast<abc::Extra_BitMat_t*>( Vec_PtrEntry( p->vMatrSymms, 0u ) );
  for ( auto i = 0; i < abc::Extra_BitMatrixReadSize( bitmask ); ++i )
  {
    for ( auto j = 0; j < abc::Extra_BitMatrixReadSize( bitmask ); ++j )
    {
      if ( abc::Extra_BitMatrixLookup1( bitmask, i, j ) && abc::Extra_BitMatrixLookup2( bitmask, i, j ) )
      {
        std::cout << "[i] variables " << i << " and " << j << " are symmetric" << std::endl;
      }
    }
  }

  std::cout << "[i] " << p->vMatrNonSymms->nSize << std::endl;
  bitmask = static_cast<abc::Extra_BitMat_t*>( Vec_PtrEntry( p->vMatrNonSymms, 0u ) );
  for ( auto i = 0; i < abc::Extra_BitMatrixReadSize( bitmask ); ++i )
  {
    for ( auto j = 0; j < abc::Extra_BitMatrixReadSize( bitmask ); ++j )
    {
      if ( abc::Extra_BitMatrixLookup1( bitmask, i, j ) && abc::Extra_BitMatrixLookup2( bitmask, i, j ) )
      {
        std::cout << "[i] variables " << i << " and " << j << " are not symmetric" << std::endl;
      }
    }
  }

  abc::Sym_ManStop( p );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
