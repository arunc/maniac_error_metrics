/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file dnf_learning.hpp
 *
 * @brief kDNF Learner
 *
 * @author Heinz Riener
 * @since  2.3
 */

#ifndef DNF_LEARNING_HPP
#define DNF_LEARNING_HPP

#include <formal/sat/sat_solver.hpp>
#include <classical/aig.hpp>

#include <boost/assign/std/set.hpp>

#include <vector>
#include <set>
#include <string>

namespace cirkit
{

using term_t = std::set< int >;
using dnf_t = std::vector< term_t >;

unsigned get_input_size_from_samples( const std::set< std::string >& a, const std::set< std::string >& b );

unsigned count_literals( const dnf_t& dnf );

namespace detail
{

template < typename Solver >
class dnf_learner
{
public:
  dnf_learner( Solver& solver, unsigned n, unsigned k );

  void add_negative( const std::string& negative );
  void add_positive( const std::string& positive );

  dnf_t learn();

private:
  unsigned p( unsigned i, unsigned j ) const;
  unsigned q( unsigned i, unsigned j ) const;
  unsigned z( unsigned i, unsigned j ) const;

private:
  Solver& solver;
  unsigned n;
  unsigned k;
  int var = 1;
  unsigned num_positives = 0u;
  unsigned num_negatives = 0u;
}; /* dnf_learner */

template < typename Solver >
dnf_learner<Solver>::dnf_learner( Solver& solver, unsigned n, unsigned k )
  : solver( solver )
  , n( n )
  , k( k )
{
  /* reserve 2*n*k variables */
  var += 2*n*k;
}

template < typename Solver >
unsigned dnf_learner<Solver>::p( unsigned i, unsigned j ) const
{
  return 1u + i*n + j;
}

template < typename Solver >
unsigned dnf_learner<Solver>::q( unsigned i, unsigned j ) const
{
  return 1u + (k + i)*n + j;
}

template < typename Solver >
unsigned dnf_learner<Solver>::z( unsigned i, unsigned j ) const
{
  return 1u + 2u*k*n + k*j + i;
}

template < typename Solver >
void dnf_learner<Solver>::add_negative( const std::string& negative )
{
  // std::cout << "[i] add negative " << negative << '\n';
  assert( negative.size() == n );

  for ( auto i = 0u; i < k; ++i )
  {
    clause_t cl;
    for ( auto j = 0u; j < n; ++j )
    {
      if ( negative[ j ] == '0' ) { cl += p( i, j ); }
      if ( negative[ j ] == '1' ) { cl += q( i, j ); }
    }
    add_clause( solver )( cl );
  }
}

template < typename Solver >
void dnf_learner<Solver>::add_positive( const std::string& positive )
{
  // std::cout << "[i] add positive " << positive << '\n';
  assert( positive.size() == n );

  /* reserve new z variables */
  var += k;

  clause_t cl;
  for ( auto i = 0u; i < k; ++i )
  {
    cl += z( i,num_positives );
  }
  add_clause( solver )( cl );

  for ( auto i = 0u; i < k; ++i )
  {
    for ( auto j = 0u; j < n; ++j )
    {
      clause_t cl;
      cl += -z( i, num_positives );
      if ( positive[ j ] == '0' ) { cl += -p( i, j ); }
      if ( positive[ j ] == '1' ) { cl += -q( i, j ); }
      add_clause( solver )( cl );
    }
  }

  ++num_positives;
}

template < typename Solver >
dnf_t dnf_learner<Solver>::learn()
{
  /* make sure that the counterexample is big enough */
  add_clause( solver )( { var-1, -var+1 } );

  solver_execution_statistics stats;
  const solver_result_t result = solve( solver, stats );

  dnf_t dnf;
  if ( result )
  {
    for ( auto i = 0u; i < k; ++i )
    {
      term_t term;
      for ( auto j = 0u; j < n; ++j )
      {
        if ( result->first[ p(i,j)-1u ] )
        {
          term += ( j+1 );
        }

        if ( result->first[ q(i,j)-1u ] )
        {
          term += -( j+1 );
        }
      }
      dnf += term;
    }
  }

  return dnf;
}

} /* detail */

template < typename Solver >
dnf_t learn_kDNF( const std::set< std::string >& positives, const std::set< std::string >& negatives, const unsigned max_k )
{
  const unsigned n = get_input_size_from_samples( positives, negatives );
  auto solver = make_solver< Solver >();
  detail::dnf_learner< Solver > learn_algorithm( solver, n, max_k );

  for( const auto& sample : negatives )
  {
    learn_algorithm.add_negative( sample );
  }

  for( const auto& sample : positives )
  {
    learn_algorithm.add_positive( sample );
  }

  return learn_algorithm.learn();
}

/*** dnf to aig ***/

aig_function aig_add_dnf( aig_graph& target_aig, std::vector< aig_function >& node_to_function, const dnf_t& dnf, const std::vector< aig_function >& mapping );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
