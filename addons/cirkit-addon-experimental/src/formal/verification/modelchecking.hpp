/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file modelchecking.hpp
 *
 * @brief Modelchecking
 *
 * @author Heinz Riener
 * @since  2.3
 */

#ifndef MODELCHECKING_HPP
#define MODELCHECKING_HPP

#include <classical/aig.hpp>
#include <classical/io/write_aiger.hpp>
#include <core/utils/system_utils.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <algorithm>
#include <fstream>
#include <vector>

namespace cirkit
{

/******************************************************************************
 * Abstract class for modelcheckers                                           *
 ******************************************************************************/

enum class modelcheck_result_t { success = 20, fail = 10, error = -1 };

class modelchecker
{
public:
  virtual std::pair< modelcheck_result_t, boost::dynamic_bitset<> >
  run( const aig_graph& miter ) const = 0;
}; /* modelchecker */

/******************************************************************************
 * Several modelchecker implementations                                       *
 ******************************************************************************/

class blimc_modelchecker : public modelchecker
{
public:
  blimc_modelchecker()
  {
    const auto result = execute_and_return( "blimc -h" );
    assert( result.first == 0 && "blimc is not in the $PATH" );
  }

  std::pair< modelcheck_result_t, boost::dynamic_bitset<> >
  run( const aig_graph& miter ) const
  {
    const constexpr auto filename = "/tmp/miter.aag";
    std::ofstream out( filename );
    write_aiger( miter, out );
    out.close();

    const auto res = execute_and_return( ( boost::format( "blimc %s" ) % filename ).str() );
    if ( res.first == 10 )
    {
      std::string cex = res.second[ 3u ];
      std::reverse( cex.begin(), cex.end() );
      const auto counterexample = boost::dynamic_bitset<>( cex );
      return { modelcheck_result_t::fail, counterexample };
    }
    else if ( res.first == 20 )
    {
      return { modelcheck_result_t::success, boost::dynamic_bitset<>() };
    }
    else
    {
      return { modelcheck_result_t::error, boost::dynamic_bitset<>() };
    }
  }
}; /* blimc_modelchecker */

class iimc_modelchecker : public modelchecker
{
public:
  iimc_modelchecker()
  {
    const auto result = execute_and_return( "iimc -h" );
    assert( result.first == 1 && "iimc is not in the $PATH" );
  }

  std::pair< modelcheck_result_t, boost::dynamic_bitset<> >
  run( const aig_graph& miter ) const
  {
    const constexpr auto filename = "/tmp/miter.aag";
    std::ofstream out( filename );
    write_aiger( miter, out );
    out.close();

    const auto res = execute_and_return( ( boost::format( "iimc %s" ) % filename ).str() );
    if ( res.second.size() > 1u )
    {
      assert( res.second.size() > 4u );
      std::string cex = res.second[ 4u ];
      boost::replace_all( cex, "x", "0" );
      std::reverse( cex.begin(), cex.end() );
      const auto counterexample = boost::dynamic_bitset<>( cex );
      return { modelcheck_result_t::fail, counterexample };
    }
    else if ( res.second[ 0u ] == "0" )
    {
      return { modelcheck_result_t::success, boost::dynamic_bitset<>() };
    }
    else
    {
      return { modelcheck_result_t::error, boost::dynamic_bitset<>() };
    }
  }
}; /* iimc_modelchecker */

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
