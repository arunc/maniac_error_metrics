/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "maj_xor_rewriting.hpp"

#include <functional>
#include <map>
#include <string>
#include <thread>
#include <unordered_map>

#include <boost/format.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/iterator_range.hpp>

#include <abc/functions/cirkit_to_gia.hpp>
#include <core/utils/graph_utils.hpp>
#include <core/utils/hash_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/fanout_free_regions.hpp>
#include <classical/functions/cuts/stack.hpp>
#include <classical/functions/cuts/traits.hpp>
#include <classical/optimization/mig_functional_hashing.hpp>
#include <classical/optimization/mig_rewriting.hpp>
#include <classical/utils/mig_utils.hpp>
#include <classical/utils/truth_table_utils.hpp>
#include <classical/xmg/xmg.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/add_aig_with_gia.hpp>

#ifdef ADDON_EPFL
#include <classical/functions/mig_mighty.hpp>
#endif

#define LN(x) if ( verbose ) { std::cout << x << std::endl; }
#define L(x) if ( verbose ) { std::cout << x; }

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

template<typename Iterator, typename Fn>
typename Iterator::value_type balanced_accumulate( Iterator begin, Iterator end, Fn&& f )
{
  const auto distance = std::distance( begin, end );
  assert( distance >= 1u );

  if ( distance == 1u )
  {
    return *begin;
  }
  else if ( distance == 2u )
  {
    return f( *begin, *( begin + 1 ) );
  }
  else
  {
    const auto mid = begin + ( distance >> 1u );
    return f( balanced_accumulate( begin, mid, f ),
              balanced_accumulate( mid, end, f ) );
  }
}

aig_graph create_aig_topological( const xmg_graph& circ )
{
  aig_graph aig;
  aig_initialize( aig );

  std::vector<aig_function> node_to_function( circ.size() );
  node_to_function[0] = aig_get_constant( aig, false );

  for ( const auto& i : circ.inputs() )
  {
    node_to_function[i.first] = aig_create_pi( aig, i.second );
  }

  foreach_topological( circ.graph(), [&]( const xmg_node& node ) {
      switch ( circ.fanin_count( node ) )
      {
      case 2:
        {
          const auto children = circ.children( node );
          node_to_function[node] = aig_create_xor( aig,
                                                   node_to_function[children[0u].node] ^ children[0u].complemented,
                                                   node_to_function[children[1u].node] ^ children[1u].complemented );
        }
        break;

      case 3:
        {
          const auto children = circ.children( node );
          node_to_function[node] = aig_create_maj( aig,
                                                   node_to_function[children[0u].node] ^ children[0u].complemented,
                                                   node_to_function[children[1u].node] ^ children[1u].complemented,
                                                   node_to_function[children[2u].node] ^ children[2u].complemented );
        }
        break;
      }

      return true;
    } );

  for ( const auto& o : circ.outputs() )
  {
    aig_create_po( aig, node_to_function[o.first.node] ^ o.first.complemented, o.second );
  }

  return aig;
}

aig_function create_aig_top_down_rec( const xmg_graph& circ, const xmg_node& node, aig_graph& aig, std::unordered_map<xmg_node, aig_function>& node_to_function )
{
  const auto it = node_to_function.find( node );

  if ( it != node_to_function.end() )
  {
    return it->second;
  }

  aig_function f;
  const auto children = circ.children( node );

  if ( circ.is_maj( node ) )
  {
    f = aig_create_maj( aig,
                        create_aig_top_down_rec( circ, children[0u].node, aig, node_to_function ) ^ children[0u].complemented,
                        create_aig_top_down_rec( circ, children[1u].node, aig, node_to_function ) ^ children[1u].complemented,
                        create_aig_top_down_rec( circ, children[2u].node, aig, node_to_function ) ^ children[2u].complemented );
  }
  else if ( circ.is_xor( node ) )
  {
    f = aig_create_xor( aig,
                        create_aig_top_down_rec( circ, children[0u].node, aig, node_to_function ) ^ children[0u].complemented,
                        create_aig_top_down_rec( circ, children[1u].node, aig, node_to_function ) ^ children[1u].complemented );
  }
  else
  {
    assert( false );
  }

  node_to_function.insert( {node, f} );

  return f;
}

aig_graph create_aig_top_down( const xmg_graph& circ, const xmg_function& f )
{
  aig_graph aig;
  aig_initialize( aig );

  std::unordered_map<xmg_node, aig_function> node_to_function;

  node_to_function.insert( {0, aig_get_constant( aig, false )} );

  for ( const auto& i : circ.inputs() )
  {
    node_to_function.insert( {i.first, aig_create_pi( aig, i.second )} );
  }

  const auto aig_f = create_aig_top_down_rec( circ, f.node, aig, node_to_function ) ^ f.complemented;

  aig_create_po( aig, aig_f, "f" );

  return aig;
}

/******************************************************************************
 * extract_submig_manager                                                     *
 ******************************************************************************/

class extract_submig_manager
{
public:
  extract_submig_manager( xmg_graph& circ, bool verbose );

  template<typename Fn>
  void update_submigs( Fn&& f )
  {
    for ( auto& submig : submigs )
    {
      submig.second = f( submig.second );
    }
  }

  xmg_graph rebuild() const;

private:
  std::vector<xmg_node> compute_ffr_inputs( xmg_node ffr_output );
  void compute_ffr_inputs( xmg_node v, xmg_node ffr_output, std::vector<xmg_node>& ffr_inputs );

  mig_function create_mig_rec( mig_graph& mig, xmg_node v, const std::map<xmg_node, mig_function>& pis ) const;
  mig_graph create_mig( xmg_node output, const std::vector<xmg_node>& inputs ) const;

  xmg_function rebuild_rec( const mig_graph& mig, xmg_graph& new_circ, mig_node v, const std::map<mig_node, xmg_function>& node_to_function ) const;

private:
  xmg_graph& circ;
  topsort_compare_t<xmg_graph::graph_t> comp;
  ffr_output_queue_t<xmg_graph::graph_t> queue;
  std::map<xmg_node, std::vector<xmg_node>> ffrs;
  std::map<xmg_node, mig_graph> submigs;
  bool verbose;
};

extract_submig_manager::extract_submig_manager( xmg_graph& circ, bool verbose )
  : circ( circ ),
    comp( circ.graph() ),
    queue( comp ),
    verbose( verbose )
{
  circ.compute_fanout();

  /* create initial output queue */
  for ( const auto& output : circ.outputs() )
  {
    queue.push( output.first.node );
  }

  /* compute FFRs */
  while ( !queue.empty() )
  {
    auto ffr_output = queue.top();

    /* already computed? */
    if ( ffrs.find( ffr_output ) == ffrs.end() )
    {
      /* compute inputs */
      ffrs.insert( {ffr_output, compute_ffr_inputs( ffr_output )} );
    }

    queue.pop();
  }

  L( boost::format( " (%5d FFRs)" ) % ffrs.size() );

  /* compute sub migs */
  for ( const auto& ffr : ffrs )
  {
    submigs.insert( {ffr.first, create_mig( ffr.first, ffr.second )} );
  }
}

std::vector<xmg_node> extract_submig_manager::compute_ffr_inputs( xmg_node ffr_output )
{
  std::vector<xmg_node> ffr_inputs;
  compute_ffr_inputs( ffr_output, ffr_output, ffr_inputs );
  return ffr_inputs;
}

void extract_submig_manager::compute_ffr_inputs( xmg_node v, xmg_node ffr_output, std::vector<xmg_node>& ffr_inputs )
{
  /* if primary input or constant */
  if ( circ.is_input( v ) )
  {
    if ( v != 0 && boost::find( ffr_inputs, v ) == ffr_inputs.end() )
    {
      ffr_inputs.push_back( v );
    }
  }
  /* if XOR gate */
  else if ( circ.is_xor( v ) )
  {
    ffr_inputs.push_back( v );
    const auto children = circ.children( v );
    queue.push( children[0u].node );
    queue.push( children[1u].node );
  }
  /* if has multiple fanout */
  else if ( v != ffr_output && circ.fanout_count( v ) > 1u )
  {
    ffr_inputs.push_back( v );
    queue.push( v );
  }
  /* else recur */
  else
  {
    for ( const auto& child : circ.children( v ) )
    {
      compute_ffr_inputs( child.node, ffr_output, ffr_inputs );
    }
  }
}

xmg_function extract_submig_manager::rebuild_rec( const mig_graph& mig, xmg_graph& new_circ, mig_node v, const std::map<mig_node, xmg_function>& node_to_function ) const
{
  const auto it = node_to_function.find( v );
  if ( it != node_to_function.end() )
  {
    return it->second;
  }

  const auto children = get_children( mig, v );

  return new_circ.create_maj( rebuild_rec( mig, new_circ, children[0u].node, node_to_function ) ^ children[0u].complemented,
                              rebuild_rec( mig, new_circ, children[1u].node, node_to_function ) ^ children[1u].complemented,
                              rebuild_rec( mig, new_circ, children[2u].node, node_to_function ) ^ children[2u].complemented );
}

xmg_graph extract_submig_manager::rebuild() const
{
  xmg_graph new_circ;

  std::map<xmg_node, xmg_function> old_to_new;
  old_to_new[0u] = new_circ.get_constant( false );

  for ( const auto& pi : circ.inputs() )
  {
    old_to_new[pi.first] = new_circ.create_pi( pi.second );
  }

  for ( const auto& node : circ.topological_nodes() )
  {
    if ( circ.is_xor( node ) )
    {
      const auto children = circ.children( node );
      old_to_new[node] = new_circ.create_xor(
          old_to_new[children[0u].node] ^ children[0u].complemented,
          old_to_new[children[1u].node] ^ children[1u].complemented );
      continue;
    }

    const auto it = ffrs.find( node );
    if ( it == ffrs.end() ) { continue; }

    auto ffr_output = it->first;
    const auto& ffr_inputs = it->second;

    std::map<mig_node, xmg_function> mig_node_to_function;
    mig_node_to_function[0u] = old_to_new[0u];

    const auto& submig = submigs.at( ffr_output );
    const auto& submig_info = mig_info( submig );
    for ( const auto& pi : index( submig_info.inputs ) )
    {
      const auto it2 = old_to_new.find( ffr_inputs[pi.index] );
      assert( it2 != old_to_new.end() );
      mig_node_to_function[pi.value] = it2->second;
    }

    const auto& output = submig_info.outputs.front().first;
    old_to_new[ffr_output] = rebuild_rec( submig, new_circ, output.node, mig_node_to_function );
  }

  for ( const auto& output : circ.outputs() )
  {
    new_circ.create_po( old_to_new[output.first.node] ^ output.first.complemented, output.second );
  }

  return new_circ;
}

mig_function extract_submig_manager::create_mig_rec( mig_graph& mig, xmg_node v, const std::map<xmg_node, mig_function>& pis ) const
{
  const auto it = pis.find( v );
  if ( it != pis.end() )
  {
    return it->second;
  }

  if ( !circ.is_maj( v ) )
  {
    std::cout << boost::format( "[e] expected %d to be a MAJ gate, but it has fanin_count = %d" ) % v % circ.fanin_count( v ) << std::endl;
    assert( false );
  }

  const auto children = circ.children( v );
  return mig_create_maj(
      mig,
      create_mig_rec( mig, children[0u].node, pis ) ^ children[0u].complemented,
      create_mig_rec( mig, children[1u].node, pis ) ^ children[1u].complemented,
      create_mig_rec( mig, children[2u].node, pis ) ^
          children[2u].complemented );
}

mig_graph extract_submig_manager::create_mig( xmg_node output, const std::vector<xmg_node>& inputs ) const
{
  mig_graph mig;
  mig_initialize( mig );

  std::map<xmg_node, mig_function> pis;
  pis[0u] = mig_get_constant( mig, false );
  for ( const auto input : inputs )
  {
    pis[input] = mig_create_pi( mig, boost::str( boost::format( "pi_%d" ) % input ) );
  }
  mig_create_po( mig, create_mig_rec( mig, output, pis ), "output" );

  return mig;
}

/******************************************************************************
 * xor_propagation_manager                                                    *
 ******************************************************************************/

class xor_propagation_manager
{
public:
  xor_propagation_manager( xmg_graph& old_circ ) : old_circ( old_circ )
  {
    old_circ.compute_fanout();
  }

  xmg_graph run();

  inline bool is_modified() const { return modified; }
  inline double runtime() const { return _runtime; }

private:
  std::pair<xmg_function, xmg_function> normalized_xor_children( xmg_node node ) const;
  xmg_function xor_distributivity( const xmg_function& a, const xmg_function& b );
  xmg_function propagate_xors( xmg_node node );

private:
  xmg_graph& old_circ;
  xmg_graph new_circ;
  std::map<xmg_node, xmg_function> old_to_new;
  bool modified = false;
  double _runtime = 0.0;
};

std::pair<xmg_function, xmg_function> xor_propagation_manager::normalized_xor_children( xmg_node node ) const
{
  auto children = old_circ.children( node );

  /* normalize polarities */
  // if ( children[0].complemented == children[1].complemented )
  // {
  //   children[0].complemented = children[1].complemented = false;
  // }
  // else
  // {
  //   children[0].complemented = false;
  //   children[1].complemented = true;
  // }

  return {children[0], children[1]};
}

/*
 * a ⊕ b = <(a ⊕ c)(a ⊕ d)(a ⊕ e)> where b = <cde>
 */
xmg_function xor_propagation_manager::xor_distributivity( const xmg_function& a, const xmg_function& b )
{
  modified = true;

  const auto grand_children = old_circ.children( b.node );

  const auto af = propagate_xors( a.node ) ^ a.complemented;
  const auto cf = propagate_xors( grand_children[0].node ) ^ grand_children[0].complemented ^ b.complemented;
  const auto df = propagate_xors( grand_children[1].node ) ^ grand_children[1].complemented ^ b.complemented;
  const auto ef = propagate_xors( grand_children[2].node ) ^ grand_children[2].complemented ^ b.complemented;

  return new_circ.create_maj( new_circ.create_xor( af, cf ), new_circ.create_xor( af, df ), new_circ.create_xor( af, ef ) );
}

xmg_function xor_propagation_manager::propagate_xors( xmg_node node )
{
  /* already visited? (will also catch PIs and constant) */
  const auto it = old_to_new.find( node );
  if ( it != old_to_new.end() )
  {
    return it->second;
  }

  xmg_function new_f;

  /* if gate is majority gate */
  if ( old_circ.is_maj( node ) )
  {
    const auto children = old_circ.children( node );
    new_f = new_circ.create_maj(
        propagate_xors( children[0].node ) ^ children[0].complemented,
        propagate_xors( children[1].node ) ^ children[1].complemented,
        propagate_xors( children[2].node ) ^ children[2].complemented );
  }
  /* if gate is xor gate */
  else
  {
    xmg_function a, b;
    std::tie( a, b ) = normalized_xor_children( node );

    /* if one child is XOR or both children are PIs */
    if ( old_circ.is_xor( a.node ) || old_circ.is_xor( b.node ) ||
         ( old_circ.is_input( a.node ) && old_circ.is_input( b.node ) ) )
    {
      new_f = new_circ.create_xor( propagate_xors( a.node ) ^ a.complemented,
                                   propagate_xors( b.node ) ^ b.complemented );
    }
    /* if left child is PI */
    else if ( old_circ.is_input( a.node ) )
    {
      assert( a.node != 0u ); /* this shouldn't be possible */
      new_f = xor_distributivity( a, b );
    }
    /* if right child is PI */
    else if ( old_circ.is_input( b.node ) )
    {
      assert( b.node != 0u ); /* this shouldn't be possible */
      new_f = xor_distributivity( b, a );
    }
    else
    {
      if ( old_circ.fanout_count( b.node ) > old_circ.fanout_count( a.node ) )
      {
        new_f = xor_distributivity( a, b );
      }
      else
      {
        new_f = xor_distributivity( b, a );
      }
    }
  }

  old_to_new.insert( {node, new_f} );
  return new_f;
}

xmg_graph xor_propagation_manager::run()
{
  reference_timer t( &_runtime );

  /* map constant and PIs */
  old_to_new.insert( {0, new_circ.get_constant( false )} );
  for ( const auto& input : old_circ.inputs() )
  {
    old_to_new.insert( {input.first, new_circ.create_pi( input.second )} );
  }

  /* transform outputs */
  for ( const auto& output : old_circ.outputs() )
  {
    const auto new_f = propagate_xors( output.first.node ) ^ output.first.complemented;
    new_circ.create_po( new_f, output.second );
  }

  return new_circ;
}

/******************************************************************************
 * find_xor_manager                                                           *
 ******************************************************************************/

class find_xor_manager
{
public:
  bool realizes_xor( mig_node node, const structural_cut& cuts, const mig_graph& mig )
  {
    for ( const auto& cut : cuts )
    {
      auto tt = simulate_cut_tt( node, cut, mig );
      tt_to_minbase( tt );

      switch ( tt.size() )
      {
      case 2u:
        {
          const auto uv = tt.to_ulong();
          if ( uv == 0x9 || uv == 0x6 ) /* 1001 0110 */
          {
            return true;
          }
        }
        break;
      case 3u:
        {
          const auto uv = tt.to_ulong();
          if ( uv == 0x96 || uv == 0x69 ) /* 10010110 01101001 */
          {
            return true;
          }
        }
      }
    }

    return false;
  }
};

/******************************************************************************
 * Optimizations                                                              *
 ******************************************************************************/

xmg_graph functional_hashing_optimization( xmg_graph& circ, bool verbose )
{
  extract_submig_manager mgr( circ, verbose );

  mgr.update_submigs( [&]( mig_graph& mig ) { return mig_functional_hashing( mig ); } );

  return mgr.rebuild();
}

xmg_graph depth_rewriting_optimization( xmg_graph& circ, bool verbose )
{
  extract_submig_manager mgr( circ, verbose );

  mgr.update_submigs( [&]( mig_graph& mig ) { return mig_depth_rewriting( mig ); } );

  return mgr.rebuild();
}

#ifdef ADDON_EPFL
xmg_graph mighty_rewriting_optimization( xmg_graph& circ, bool verbose )
{
  extract_submig_manager mgr( circ, verbose );

  mgr.update_submigs( [&]( mig_graph& mig ) {
      auto * _mig = mig_to_mighty( mig );
      computedepth( _mig );
      selective_depth( _mig );
      aggressive_depth( _mig, 1.2 );
      selective_depth( _mig );
      const auto new_mig = mig_from_mighty( _mig );
      freemig( _mig );
      return new_mig;
    } );

  return mgr.rebuild();
}
#endif

/******************************************************************************
 * Cut points                                                                 *
 ******************************************************************************/

void evaluate_cut_points( const xmg_graph& circ )
{
  std::cout << "[i] evaluate cut points";

  foreach_topological( circ.graph(), [&]( const xmg_node& node ) {
      if ( circ.is_xor( node ) )
      {
        const auto aig = create_aig_top_down( circ, node );
        const auto gia = cirkit_to_gia( aig );

        auto solver = make_solver<minisat_solver>();

        std::vector<int> piids, poids;
        add_aig_with_gia( solver, aig, 1, piids, poids );

        solver_execution_statistics ignore;
        auto result = solve( solver, ignore, {-poids[0]} );

        if ( !(bool)result )
        {
          std::cout << "1";
        }
        else
        {
          std::cout << ".";
        }

        result = solve( solver, ignore, {poids[0]} );

        if ( !(bool)result )
        {
          std::cout << "0";
        }
        else
        {
          std::cout << ".";
        }
      }

      std::cout.flush();

      return true;
    } );

  std::cout << std::endl;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

xmg_graph create_miter_as_circuit( const mig_graph& mig1, const mig_graph& mig2 )
{
  xmg_graph circ;

  /* copy inputs */
  const auto& info1 = mig_info( mig1 );
  const auto& info2 = mig_info( mig2 );

  assert( info1.inputs.size() == info2.inputs.size() );

  std::vector<xmg_function> node_to_function1( num_vertices( mig1 ) );
  std::vector<xmg_function> node_to_function2( num_vertices( mig2 ) );

  node_to_function1[0] = node_to_function2[0] = circ.get_constant( false );

  for ( auto i = 0u; i < info1.inputs.size(); ++i )
  {
    assert( info1.node_names.at( info1.inputs[i] ) == info2.node_names.at( info2.inputs[i] ) );
    const auto pi = circ.create_pi( info1.node_names.at( info1.inputs[i] ) );

    node_to_function1[info1.inputs[i]] = pi;
    node_to_function2[info2.inputs[i]] = pi;
  }

  /* perform cut enumeration */
  std::map<mig_node, structural_cut> cuts1, cuts2;
  std::thread t1( [&cuts1, &mig1]() { cuts1 = stack_based_structural_cut_enumeration( mig1, 4u ); } );
  std::thread t2( [&cuts2, &mig2]() { cuts2 = stack_based_structural_cut_enumeration( mig2, 4u ); } );
  t1.join();
  t2.join();

  /* find xors */
  find_xor_manager fxmgr;

  /* copy nodes */
  foreach_topological( mig1, [&]( const mig_node& n ) {
      if ( out_degree( n, mig1 ) == 0u ) { return true; }

      if ( fxmgr.realizes_xor( n, cuts1[n], mig1 ) )
      {
        assert( false );
      }

      const auto children = get_children( mig1, n );
      node_to_function1[n] = circ.create_maj( node_to_function1[children[0].node] ^ children[0].complemented,
                                              node_to_function1[children[1].node] ^ children[1].complemented,
                                              node_to_function1[children[2].node] ^ children[2].complemented );

      return true;
    } );

  foreach_topological( mig2, [&]( const mig_node& n ) {
      if ( out_degree( n, mig2 ) == 0u ) { return true; }

      if ( fxmgr.realizes_xor( n, cuts2[n], mig2 ) )
      {
        assert( false );
      }

      const auto children = get_children( mig2, n );
      node_to_function2[n] = circ.create_maj( node_to_function2[children[0].node] ^ children[0].complemented,
                                              node_to_function2[children[1].node] ^ children[1].complemented,
                                              node_to_function2[children[2].node] ^ children[2].complemented );

      return true;
    } );

  /* merge outputs */
  assert( info1.outputs.size() == info2.outputs.size() );
  std::vector<xmg_function> xors( info1.outputs.size() );
  for ( auto i = 0u; i < info1.outputs.size(); ++i )
  {
    assert( info1.outputs[i].second == info2.outputs[i].second );

    xors[i] = circ.create_xor( node_to_function1[info1.outputs[i].first.node] ^ info1.outputs[i].first.complemented,
                               node_to_function2[info2.outputs[i].first.node] ^ info2.outputs[i].first.complemented );
  }

  if ( xors.size() == 1u )
  {
    circ.create_po( xors.front(), "miter" );
  }
  else
  {
    circ.create_po(
        balanced_accumulate( xors.begin(), xors.end(),
                             std::bind( &xmg_graph::create_or,
                                        std::ref( circ ), std::placeholders::_1,
                                        std::placeholders::_2 ) ),
        "miter" );
  }

  return circ;
}

aig_graph create_miter( const mig_graph& mig1, const mig_graph& mig2,
                        const properties::ptr& settings,
                        const properties::ptr& statistics )
{
  /* verbose */
  const auto verbose             = get( settings, "verbose",             false );
  const auto propagate           = get( settings, "propagate",           true );
  const auto max_growth_factor   = get( settings, "max_growth_factor",   boost::optional<unsigned>() );
  const auto max_rounds          = get( settings, "max_rounds",          boost::optional<unsigned>() );
  const auto cut_point_detection = get( settings, "cut_point_detection", true );
  const auto functional_hashing  = get( settings, "functional_hashing",  false );
  const auto depth_rewriting     = get( settings, "depth_rewriting",     false );
#ifdef ADDON_EPFL
  const auto mighty_rewriting    = get( settings, "mighty_rewriting",    false );
#endif

  /* timing */
  properties_timer t( statistics );

  auto circ = create_miter_as_circuit( mig1, mig2 );

  std::vector<int> num_majs, num_xors;

  num_majs.push_back( circ.num_maj() );
  num_xors.push_back( circ.num_xor() );

  if ( propagate )
  {
    LN( boost::format( "[i] initial                    size: %8d  maj: %8d  xor: %8d" ) % circ.size() % circ.num_maj() % circ.num_xor() );
    const auto orig_size = circ.size();

    auto round = 0u;

    while ( true )
    {
      L( "[i] propagate                 " );
      xor_propagation_manager mgr( circ );
      circ = mgr.run();

      LN( boost::format( " size: %8d  maj: %8d  xor: %8d  run-time: %7.2f secs" ) % circ.size() % circ.num_maj() % circ.num_xor() % mgr.runtime() );

      num_majs.push_back( circ.num_maj() );
      num_xors.push_back( circ.num_xor() );

      if ( functional_hashing )
      {
        L( "[i] optimize (FH)" );
        circ = functional_hashing_optimization( circ, verbose );
        LN( boost::format( " size: %8d  maj: %8d  xor: %8d  run-time: %7.2f secs" ) % circ.size() % circ.num_maj() % circ.num_xor() % mgr.runtime() );

        num_majs.push_back( circ.num_maj() );
        num_xors.push_back( circ.num_xor() );
      }

      if ( depth_rewriting )
      {
        L( "[i] optimize (DR)" );
        circ = depth_rewriting_optimization( circ, verbose );
        LN( boost::format( " size: %8d  maj: %8d  xor: %8d  run-time: %7.2f secs" ) % circ.size() % circ.num_maj() % circ.num_xor() % mgr.runtime() );

        num_majs.push_back( circ.num_maj() );
        num_xors.push_back( circ.num_xor() );
      }

#ifdef ADDON_EPFL
      if ( mighty_rewriting )
      {
        L( "[i] optimize (MR)" );
        circ = mighty_rewriting_optimization( circ, verbose );
        LN( boost::format( " size: %8d  maj: %8d  xor: %8d  run-time: %7.2f secs" ) % circ.size() % circ.num_maj() % circ.num_xor() % mgr.runtime() );

        num_majs.push_back( circ.num_maj() );
        num_xors.push_back( circ.num_xor() );
      }
#endif

      /* terminate loop? */
      if ( !mgr.is_modified() ) { LN( "[i] no more modifications" ); break; }
      if ( (bool)max_growth_factor && circ.size() > orig_size * *max_growth_factor ) { LN( "[i] maximum growth reached" ); break; }
      if ( (bool)max_rounds && ++round == *max_rounds ) { LN( "[i] maximum number of rounds reached" ); break; }
    }
  }

  set( statistics, "num_majs", num_majs );
  set( statistics, "num_xors", num_xors );

  if ( cut_point_detection )
  {
    evaluate_cut_points( circ );
  }

  return create_aig_topological( circ );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
