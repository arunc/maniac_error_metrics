/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file yosys_api.hpp
 *
 * @brief Yosys interface
 *
 * Interface to Yosys
 * Ported from Yosys project.
 *
 * @author Arun 
 * @since  2.3
 */

#ifndef YOSYS_API_HPP
#define YOSYS_API_HPP

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
// Some precautions.
#ifndef _YOSYS_
# define _YOSYS_
#endif
#ifdef YOSYS_ENABLE_TCL
# undef YOSYS_ENABLE_TCL
# pragma message ("Warning - Yosys NOT built with TCL support!, disabling TCL")
#endif
#ifdef _WIN32
#error "No windows support for the timebeing!"
#endif

#include <kernel/yosys.h>

//#include <boost/regex.hpp>
#include <boost/algorithm/string/erase.hpp>

#include <sstream>
#include <fstream>
#include <istream>
#include <ostream>
#include <iostream>

namespace Yosys {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// for Cirkit
inline std::string proper_name (const std::string wire_name) {
  return boost::erase_all_copy (wire_name, "\\");
}

//------------------------------------------------------------------------------
// A wrapper with try-catch. TODO: enhance/correct this later.
inline void run_yosys_cmd (const std::string &command, bool print_cmd = true) {
  if (print_cmd) std::cout << "[i] Yosys :: " << command << std::endl;
  try {  Yosys::run_pass(command); }
  catch (const std::exception& e) {std::cout << e.what() << "\n";}
  catch (...) { throw; }
}

// TODO implement this one...
inline bool is_yosys_initialized() {
  // should be true even after design -reset.
  // only indiicate if its initialized or not, not if it has a valid design or not.
  return true;
}

inline bool has_module ( const std::string & module_name, RTLIL::Design *design) {
  RTLIL::Module* module;
  auto flag = false;
  for ( auto mod : design->modules() ) {
    if ( proper_name ( (mod->name).str() ) == module_name) {
      module = mod; flag = true;
      break;
    }
  }
  if (!flag) std::cout << "[e] Cannot find the module " << module_name
		       << " in the design" << std::endl;
  return flag;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
